library one_package;

import 'package:event_bus/event_bus.dart';
import 'package:timeago/timeago.dart' as time_ago;
import 'app_package.dart';
import 'config/config.dart' as _app_config;
import 'g.dart' as _app_g;
export 'helper/helper.dart';
export 'model/model.dart';
export 'view/widget/widget.dart';
export 'form/form.dart';
export 'package:collapsible/collapsible.dart';
export 'package:flutter/material.dart';
export 'config/constant/app_file.dart';
export 'translate.dart';
export 'package:dio/dio.dart';

_app_g.AppG appG = _app_g.AppG();
_app_config.AppConfig appConfig = _app_config.AppConfig();
EventBus appEventBus = EventBus(); ///

enum AppHttpType { post, get }

enum AppHttpStatus { error, tokenExpired, refreshTokenExpired, noUrl, success, fetchDataError, internalServerError, timeout }

class AppHttpError {
  AppHttpStatus status;
  dynamic message;
  dynamic errorCode;

  AppHttpError({required this.status, required this.message, required this.errorCode});
}

Future appInitialize({
  Map<String, String>? defaultHeader,
  String? baseUrl,
  String? baseUrlImg,
  String? defaultImageAsset,
  AppFilePath? oneFilePath,
  String? fontFamily,
  bool? isDebug,
  bool? isImageDebug,
  String? formatDate,
}) async {
  // timeago.setLocaleMessages('km', KmCustomMessages());
  if (defaultHeader != null) {
    appConfig.defaultHeader = defaultHeader;
  }
  if (baseUrl != null) {
    appConfig.baseUrl = baseUrl;
  }
  if (baseUrlImg != null) {
    appConfig.baseUrlImg = baseUrlImg;
  }
  if (defaultImageAsset != null) {
    appConfig.defaultImageAsset = defaultImageAsset;
  }
  if (oneFilePath != null) {
    appConfig.oneFilePath = oneFilePath;
  }
  if (fontFamily != null) {
    appG.fontFamily = fontFamily;
  }
  if (isDebug != null) {
    appG.isDebug = isDebug;
  }
  if (formatDate != null) {
    appG.formatDate = formatDate;
  }
  if (isImageDebug != null) {
    appG.isImageDebug = isImageDebug;
  }
  return null;
}

enum AppThemeMode { light, dark, system }
