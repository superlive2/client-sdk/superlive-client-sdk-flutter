
import 'package:app_packages/app_package.dart';

late LookupTranslate _lookupMessagesMap;

void setLocaleMessages(LookupTranslate lookupMessages) {
  _lookupMessagesMap = lookupMessages;
}

LookupTranslate get tr {
  return _lookupMessagesMap;
}
