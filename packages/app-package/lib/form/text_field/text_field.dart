import 'package:flutter/services.dart';
import 'package:app_packages/app_package.dart';
import '../../config/config.dart';
import 'dart:math' as math;

class AppFormTextField extends StatefulWidget {
  final String? text;
  final Color? textColor;
  final String? hintText;
  final String? helperText;
  final TextInputAction textInputAction;
  final bool readOnly;
  final bool disable;
  final bool numberOnly;
  final IconData? icon;
  final IconData? iconRight;
  final Color colorIcon;
  final Color colorIconRight;
  final bool textArea;
  final FormFieldValidator<String>? validator;
  final ValueChanged<String>? onChanged;
  final ValueChanged<String>? onFieldSubmitted;
  final GestureTapCallback? onTap;
  final Widget? suffixIcon;
  final Widget? prefix;
  final FocusNode? focusNode;
  Color? fillColor;
  final bool? disableBorderColor;
  final bool secureText;
  TextEditingController? controller = TextEditingController();
  final AppSize size;

  AppFormTextField({
    this.text,
    this.textColor,
    this.hintText,
    this.helperText,
    this.onChanged,
    this.onFieldSubmitted,
    this.onTap,
    this.controller,
    this.icon,
    this.iconRight,
    this.validator,
    this.suffixIcon,
    this.prefix,
    this.textInputAction = TextInputAction.done,
    this.textArea = false,
    this.disable = false,
    this.readOnly = false,
    this.numberOnly = false,
    this.focusNode,
    this.fillColor,
    this.secureText = false,
    this.disableBorderColor,
    this.size = AppSize.medium,
    this.colorIcon = Colors.black87,
    this.colorIconRight = Colors.black87,
  });

  @override
  _AppFormTextFieldState createState() => _AppFormTextFieldState();
}

class _AppFormTextFieldState extends State<AppFormTextField> {
  String? values;

  FocusNode _focusNode = FocusNode();

  var field = appConfig.fieldConfig;

  void setFocus() {
    FocusScope.of(context).requestFocus(_focusNode);
  }

  @override
  void initState() {
    _focusNode = widget.focusNode == null ? _focusNode : widget.focusNode!;
    super.initState();
    _focusNode.addListener(_focusNodeReRender);
  }

  _focusNodeReRender() {
    setState(() {
      // Re-renders
    });
  }

  @override
  Widget build(BuildContext context) {
    bool _disableBorderColor = true;
    Color? _fillColor = widget.fillColor ?? appInputDecorationTheme(context).fillColor;
    Color _labelTextColor = AppHelperColor.fieldTitle(context);
    return Container(
      constraints: appInputDecorationTheme(context).constraints,
      //height: widget.size == OneSize.big ? 48 : widget.size == OneSize.medium ? 48 : 38,
      height: widget.textArea ? null : 47,
      child: customFromWidget(disableBorderColor: _disableBorderColor, fillColor: _fillColor, labelTextColor: _labelTextColor),
    );
  }

  Widget customFromWidget({required bool disableBorderColor, required Color? fillColor, required Color labelTextColor}) {
    return FormField<String>(
      validator: widget.validator,
      builder: (FormFieldState<String> state) {
        return Theme(
          data: Theme.of(context).copyWith(splashColor: Colors.transparent),
          child: InputDecorator(
            decoration: InputDecoration(
              fillColor: fillColor,
              filled: fillColor == null ? widget.disable : true,
              prefixIcon: widget.icon != null
                  ? GestureDetector(
                      onTap: widget.onTap,
                      child: Icon(
                        widget.icon,
                        size: 20,
                        color: appInputDecorationTheme(context).prefixIconColor,
                      ),
                    )
                  : null,
              suffixIcon: widget.suffixIcon ??
                  (widget.iconRight != null
                      ? GestureDetector(
                          onTap: widget.onTap,
                          child: Icon(
                            widget.iconRight,
                            color: appInputDecorationTheme(context).suffixIconColor,
                            size: 20,
                          ),
                        )
                      : null),
              prefix: widget.prefix,
              contentPadding: EdgeInsets.zero,
              //contentPadding: widget.size == OneSize.big ? field.fieldContentPaddingBig : widget.size == OneSize.medium ? field.fieldContentPaddingMedium : field.fieldContentPaddingSmall,
              helperText: null,
              border: disableBorderColor == false ? field.fieldBorder : field.fieldBorder.copyWith(borderSide: const BorderSide(color: Colors.transparent)),
              enabledBorder: disableBorderColor == false ? field.fieldEnabledBorder : field.fieldEnabledBorder.copyWith(borderSide: const BorderSide(color: Colors.transparent)),
              focusedBorder: disableBorderColor == false ? field.fieldBorder : field.fieldBorder.copyWith(borderSide: const BorderSide(color: Colors.transparent)),
              labelText: null,
              hintText: null,
            ),
            isHovering: true,
            isFocused: _focusNode.hasFocus,
            child: TextField(
              obscureText: widget.secureText,
              textInputAction: widget.textArea ? TextInputAction.newline : widget.textInputAction,
              maxLines: widget.textArea == true ? 10 : 1,
              minLines: 1,
              focusNode: _focusNode,
              controller: widget.controller,
              readOnly: widget.disable ? widget.disable : widget.readOnly,
              style: appInputDecorationTheme(context).suffixStyle,
              onChanged: (value) {
                values = value;
                if (widget.onChanged != null) {
                  widget.onChanged!(value);
                }
              },
              onTap: () {
                if (widget.onTap != null) {
                  widget.onTap!();
                }
              },
              onSubmitted: widget.onFieldSubmitted,
              keyboardType: widget.numberOnly == true ? const TextInputType.numberWithOptions(decimal: true) : null,
              inputFormatters: widget.numberOnly == true ? [DecimalTextInputFormatter(decimalRange: 2), FilteringTextInputFormatter.allow(RegExp(r'\d+\.?\d*'))] : null,
              decoration: InputDecoration(
                border: InputBorder.none,
                fillColor: Colors.transparent,
                filled: true,
                labelText: widget.text == null ? null : ("${widget.text}"),
                hintText: widget.hintText == null ? null : ("${widget.hintText}"),
                contentPadding: checkFocus ? field.fieldContentPaddingFocus : field.fieldContentPadding,
                labelStyle: checkFocus
                    ? field.fieldLabelHasFocusStyle.copyWith(
                        color: _focusNode.hasFocus ? appInputDecorationTheme(context).focusColor : appInputLabelStyleTheme(context).color,
                        fontSize: appInputLabelStyleTheme(context).fontSize! + 2.0,
                      )
                    : field.fieldLabelStyle.copyWith(
                        color: _focusNode.hasFocus ? appInputDecorationTheme(context).focusColor : appInputLabelStyleTheme(context).color,
                        fontSize: appInputLabelStyleTheme(context).fontSize,
                      ),
                hintStyle: field.fieldLabelStyle.copyWith(
                  color: appInputLabelStyleTheme(context).color,
                  fontSize: appInputLabelStyleTheme(context).fontSize,
                ),
              ),
            ),
          ),
        );
       /* return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: 5.0),
            if (!state.hasError ? false : state.errorText != null)
              Padding(
                padding: const EdgeInsets.only(left: 15),
                child: Text(
                  state.errorText!,
                  style: TextStyle(color: Colors.redAccent.shade700, fontSize: 12.0),
                ),
              ),
          ],
        );*/
      },
    );
  }

  bool get checkFocus {
    return _focusNode.hasFocus || ((values != null && values != '') || (widget.controller == null ? false : widget.controller?.text != null && widget.controller?.text != ''));
  }
}

class DecimalTextInputFormatter extends TextInputFormatter {
  DecimalTextInputFormatter({required this.decimalRange}) : assert(decimalRange > 0);

  final int decimalRange;

  @override
  TextEditingValue formatEditUpdate(
    TextEditingValue oldValue, // unused.
    TextEditingValue newValue,
  ) {
    TextSelection newSelection = newValue.selection;
    String truncated = newValue.text;

    String value = newValue.text;

    if (value.contains(".") && value.substring(value.indexOf(".") + 1).length > decimalRange) {
      truncated = oldValue.text;
      newSelection = oldValue.selection;
    } else if (value == ".") {
      truncated = "0.";

      newSelection = newValue.selection.copyWith(
        baseOffset: math.min(truncated.length, truncated.length + 1),
        extentOffset: math.min(truncated.length, truncated.length + 1),
      );
    }

    return TextEditingValue(
      text: truncated,
      selection: newSelection,
      composing: TextRange.empty,
    );
  }
}

/*oneTheme(
        context,
        child: Theme(
          data: oneThemeData(context).copyWith(
              inputDecorationTheme: InputDecorationTheme(
                border: field.fieldBorder,
                contentPadding: widget.size == OneSize.big ? field.fieldContentPaddingBig : widget.size == OneSize.medium ? field.fieldContentPaddingMedium : field.fieldContentPaddingSmall,
              )),
          child: TextFormField(
            obscureText : widget.secureText,
            textInputAction :  widget.textInputAction,
            focusNode: widget.focusNode == null ? _focusNode : widget.focusNode,
            maxLines: widget.textArea == true ? 10 : 1,
            minLines: widget.textArea == true ? 3 : 1,
            validator: widget.validator,
            readOnly: widget.disable ? widget.disable : widget.readOnly,
            controller: widget.controller,
            style: isThemeDark(context) ?  field.fieldStyle.copyWith(color: Colors.white) : field.fieldStyle,
            onChanged: (value) {
              values = value;
              if (widget.onChanged != null) {
                widget.onChanged!(value);
              }
            },
            onFieldSubmitted: (val){
              if (widget.onFieldSubmitted != null) {
                widget.onFieldSubmitted!(val.toString());
              }
            },
            onTap: () {
              if (widget.onTap != null) {
                widget.onTap!();
              }
            },
            keyboardType: widget.numberOnly == true
                ? TextInputType.numberWithOptions(decimal: true)
                : null,
            inputFormatters: widget.numberOnly == true
                ? [
              DecimalTextInputFormatter(decimalRange: 2),
              FilteringTextInputFormatter.allow(RegExp(r'\d+\.?\d*'))
            ]
                : null,
            decoration: InputDecoration(
              fillColor:  _fillColor,
              filled: _fillColor == null ? widget.disable : true,
              prefixIcon: widget.icon != null ? Icon(
                widget.icon, size: 16,
                color: isThemeDark(context) ? Colors.grey[100] : Colors.black87,
              ) : null,
              suffixIcon: widget.suffixIcon == null ? widget.iconRight != null ? Icon(
                widget.iconRight,
                color: isThemeDark(context) ? Colors.grey[100] : Colors.black87,
                size: 16,
              ) : null : widget.suffixIcon,
              prefix: widget.prefix,
              contentPadding: widget.size == OneSize.big ? field.fieldContentPaddingBig : widget.size == OneSize.medium ? field.fieldContentPaddingMedium : field.fieldContentPaddingSmall,
              helperText: widget.helperText,
              border: _disableBorderColor == false ? field.fieldBorder : field.fieldBorder.copyWith(borderSide:
              const BorderSide(color: Colors.transparent)),
              enabledBorder: _disableBorderColor == false ? field.fieldEnabledBorder : field.fieldEnabledBorder.copyWith(borderSide:
              const BorderSide(color: Colors.transparent)),
              focusedBorder: _disableBorderColor == false ? field.fieldBorder : field.fieldBorder.copyWith(borderSide:
              const BorderSide(color: Colors.transparent)),
              labelText: widget.text == null ? null : ("${widget.text}"),
              hintText: widget.hintText == null ? null : ("${widget.hintText}"),
              labelStyle: _focusNode.hasFocus
                  ? field.fieldLabelHasFocusStyle
                  : (values != null && values != '') || (widget.controller == null ? false : widget.controller?.text != null && widget.controller?.text != '')
                  ? field.fieldLabelHasFocusStyle
                  .copyWith(color: Colors.black54)
                  : (widget.textColor != null ? field.fieldLabelStyle.copyWith(color: widget.textColor) : field.fieldLabelStyle),
              hintStyle: widget.textColor != null ? field.fieldLabelStyle.copyWith(color: widget.textColor) : field.fieldLabelStyle.copyWith(color: _labelTextColor)
              //labelStyle: TextStyle(color: Colors.red, fontSize: 20),
            ),
          ),
          ),
      ),*/
