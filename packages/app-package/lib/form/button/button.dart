import '../../app_package.dart';

class AppFormButton extends StatefulWidget {
  final String? text;
  final GestureTapCallback? onPressed;
  final double minWidth;
  final double? height;
  final Color? color;
  final double radius;
  final Icon? icon;
  final TextStyle? textStyle;
  final bool transparent;
  final TextDirection textDirection;

  const AppFormButton({
    this.text,
    this.onPressed,
    this.minWidth = -1,
    this.height,
    this.color,
    this.transparent = false,
    this.textStyle,
    this.radius = 5,
    this.icon,
    this.textDirection = TextDirection.ltr,
  });

  @override
  _coButtonState createState() => _coButtonState();
}

class _coButtonState extends State<AppFormButton> {
  @override
  Widget build(BuildContext context) {
    var _with = widget.minWidth;
    if (_with < 0) {
      _with = double.infinity;
    }
    Color _color = widget.color != null ? widget.color! : appButtonThemeData(context).colorScheme!.background;

    ButtonStyle _buttonStyle = ButtonStyle(
      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
        widget.transparent
            ? RoundedRectangleBorder(side: BorderSide(color: _color, width: 1, style: BorderStyle.solid), borderRadius: BorderRadius.circular(widget.radius))
            : RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(widget.radius),
              ),
      ),
      foregroundColor: MaterialStateProperty.all<Color>(
        widget.transparent == false ? Colors.white : Colors.black,
      ),
      backgroundColor: MaterialStateProperty.all<Color>(widget.transparent ? Colors.transparent : (_color)),
    );
    Text _text = Text(
      widget.text == null ? 'Submit' : '${widget.text}',
      style: widget.textStyle ?? appTextTheme(context).labelLarge,
    );
    return Container(
      constraints: BoxConstraints(
        minWidth: _with,
      ),
      child: ButtonTheme(
        minWidth: _with,
        height: appButtonThemeData(context).height,
        child: widget.icon == null
            ? TextButton(
                style: _buttonStyle,
                onPressed: widget.onPressed,
                child: _text,
              )
            : Directionality(
                textDirection: widget.textDirection,
                child: TextButton.icon(
                  icon: widget.icon!,
                  style: _buttonStyle,
                  label: _text,
                  onPressed: widget.onPressed,
                ),
              ),
      ),
    );
  }
}
