import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:app_packages/app_package.dart';

typedef CoDateWidgetBuilder = Widget Function(BuildContext context, String val);

class AppFormDate extends StatefulWidget {
  String? text;
  String? hintText;
  String? helperText;
  FormFieldValidator<String>? validator;
  ValueChanged<DateTime>? onChanged;
  DateTime? initialDateTime;
  TextEditingController? controller = TextEditingController();
  bool disable;
  CoDateWidgetBuilder? customBuilder;
  String? dateFormatDisplay;
  IconData? iconRight;
  final Widget? suffixIcon;
  final Widget? prefix;

  AppFormDate({
    this.text,
    this.hintText,
    this.helperText,
    this.onChanged,
    this.controller,
    this.validator,
    this.disable: false,
    this.initialDateTime,
    this.customBuilder,
    this.dateFormatDisplay,
    this.iconRight,
    this.suffixIcon,
    this.prefix,
  });

  @override
  _CoDateState createState() => _CoDateState();
}

class _CoDateState extends State<AppFormDate> {
  var now = DateTime.now();
  DateTime dateTimes = DateTime.now();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    widget.initialDateTime ??= DateTime(dateTimes.year, dateTimes.month, dateTimes.day);
  }

  Widget showDatePicker() {
    return Builder(
      builder: (context) {
        return CupertinoTheme(
          data: CupertinoThemeData(
            textTheme: CupertinoTextThemeData(
              dateTimePickerTextStyle: TextStyle(
                fontSize: 14,
                color: isThemeDark(context) ? Colors.white : Colors.black87,
              ),
            ),
          ),
          child: CupertinoDatePicker(
            mode: CupertinoDatePickerMode.date,
            initialDateTime: dateTimes,
            onDateTimeChanged: (newDateTime) {
              dateTimes = newDateTime;
            },
          ),
        );
      },
    );
  }

  void popUp() {
    if (widget.disable == false) {
      dateTimes = DateTime(widget.initialDateTime!.year, widget.initialDateTime!.month, widget.initialDateTime!.day);
      appModalBottomSheet(
        context,
        text: widget.text == null ? (widget.hintText == null ? tr.date.toFirstUppercase() : widget.hintText!) : widget.text!,
        scrollAble: false,
        withOK: true,
        styleBorderRadius: true,
        build: showDatePicker(),
        onOK: () {
          widget.initialDateTime = DateTime(dateTimes.year, dateTimes.month, dateTimes.day);
          setState(() {});
          Navigator.of(context, rootNavigator: true).pop();
          if (widget.onChanged != null) {
            widget.onChanged!(dateTimes);
          }
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    if (widget.initialDateTime == null) {
      widget.initialDateTime = DateTime(dateTimes.year, dateTimes.month, dateTimes.day);
    } else {
      dateTimes = DateTime(widget.initialDateTime!.year, widget.initialDateTime!.month, widget.initialDateTime!.day);
    }
    return widget.customBuilder == null
        ? AppFormTextField(
            disable: widget.disable,
            text: widget.text,
            hintText: widget.hintText,
            readOnly: true,
            validator: widget.validator,
            prefix: widget.prefix,
            suffixIcon: widget.suffixIcon,
            controller: TextEditingController(text: formatDateOfDay(widget.initialDateTime == null ? dateTimes : widget.initialDateTime!, format: widget.dateFormatDisplay)),
            iconRight: widget.iconRight ?? Icons.date_range_outlined,
            onTap: () {
              popUp();
            },
          )
        : GestureDetector(
            onTap: () {
              popUp();
            },
            child: widget.customBuilder!(context, formatDateOfDay(widget.initialDateTime == null ? dateTimes : widget.initialDateTime!, format: widget.dateFormatDisplay)),
          );
  }
}
