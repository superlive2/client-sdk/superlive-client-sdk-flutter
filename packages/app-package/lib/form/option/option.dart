import 'package:app_packages/app_package.dart';

class AppFormOption extends StatefulWidget {
  final String? text;
  final Color? textColor;
  final String? hintText;
  final String? value;
  final FormFieldValidator<String>? validator;
  final ValueChanged<String>? onChanged;
  final ValueChanged<String>? onFieldSubmitted;
  final GestureTapCallback? onTap;

  const AppFormOption({
    this.text,
    this.textColor,
    this.hintText,
    this.value,
    this.onChanged,
    this.onFieldSubmitted,
    this.onTap,
    this.validator,
  });

  @override
  _AppFormOptionState createState() => _AppFormOptionState();
}

class _AppFormOptionState extends State<AppFormOption> {
  @override
  Widget build(BuildContext context) {
    return AppFormTextField(
      controller: TextEditingController(text: widget.value),
      text: widget.text,
      hintText: widget.hintText,
      onChanged: widget.onChanged,
      validator: widget.validator,
      textColor: widget.textColor,
      onFieldSubmitted: widget.onFieldSubmitted,
      iconRight: Icons.keyboard_arrow_right_outlined,
      readOnly: true,
      onTap: widget.onTap,
    );
  }
}
