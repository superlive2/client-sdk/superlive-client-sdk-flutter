import 'package:flutter/cupertino.dart';
import 'package:app_packages/app_package.dart';

typedef CoTimeWidgetBuilder = Widget Function(BuildContext context, String val);

class AppFormTime extends StatefulWidget {
  String? text;
  String? hintText;
  String? helperText;
  FormFieldValidator<String>? validator;
  ValueChanged<TimeOfDay>? onChanged;
  TimeOfDay? initialTime;
  TextEditingController? controller = TextEditingController();
  bool disable;
  CoTimeWidgetBuilder? customBuilder;
  IconData? iconRight;
  final Widget? suffixIcon;
  final Widget? prefix;

  AppFormTime({
    this.text,
    this.hintText,
    this.helperText,
    this.onChanged,
    this.controller,
    this.validator,
    this.disable: false,
    this.initialTime,
    this.customBuilder,
    this.iconRight,
    this.suffixIcon,
    this.prefix,
  });

  @override
  _CoDateState createState() => _CoDateState();
}

class _CoDateState extends State<AppFormTime> {
  DateTime dateNow = DateTime.now();
  TimeOfDay times = TimeOfDay.now();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  Widget showDatePicker() {
    return CupertinoTheme(
      data: CupertinoThemeData(
        textTheme: CupertinoTextThemeData(
          dateTimePickerTextStyle: TextStyle(
            fontSize: 14,
            color: isThemeDark(context) ? Colors.white : Colors.black87,
          ),
        ),
      ),
      child: CupertinoDatePicker(
        mode: CupertinoDatePickerMode.time,
        initialDateTime: DateTime(
          dateNow.year,
          dateNow.month,
          dateNow.day,
          times.hour,
          times.minute,
        ),
        //initialDateTime: widget.initialTime == null ? dateNow : new DateTime(dateNow.year, dateNow.month, dateNow.day, widget.initialTime.hour, widget.initialTime.minute, ),
        onDateTimeChanged: (newDateTime) {
          times = TimeOfDay(hour: newDateTime.hour, minute: newDateTime.minute);
        },
      ),
    );
  }

  void popUp() {
    if (widget.disable == false) {
      times = TimeOfDay(hour: widget.initialTime!.hour, minute: widget.initialTime!.minute);
      appModalBottomSheet(
        context,
        text: widget.text == null ? (widget.hintText == null ? tr.time.toFirstUppercase() : widget.hintText!) : widget.text!,
        scrollAble: false,
        withOK: true,
        styleBorderRadius: true,
        build: showDatePicker(),
        onOK: () {
          widget.initialTime = TimeOfDay(hour: times.hour, minute: times.minute);
          setState(() {});
          Navigator.of(context, rootNavigator: true).pop();
          if (widget.onChanged != null) {
            widget.onChanged!(times);
          }
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    if (widget.initialTime == null) {
      widget.initialTime = TimeOfDay(hour: times.hour, minute: times.minute);
    } else {
      times = TimeOfDay(hour: widget.initialTime!.hour, minute: widget.initialTime!.minute);
    }
    return widget.customBuilder == null
        ? AppFormTextField(
            disable: widget.disable,
            hintText: widget.hintText,
            text: widget.text,
            readOnly: true,
            validator: widget.validator,
            prefix: widget.prefix,
            suffixIcon: widget.suffixIcon,
            controller: TextEditingController(text: '${formatTimeOfDay(widget.initialTime == null ? times : widget.initialTime!)}'),
            iconRight: widget.iconRight ?? Icons.timer,
            onTap: () {
              popUp();
            },
          )
        : GestureDetector(
            onTap: () {
              popUp();
            },
            child: widget.customBuilder!(context, '${formatTimeOfDay(widget.initialTime == null ? times : widget.initialTime!)}'),
          );
  }
}
