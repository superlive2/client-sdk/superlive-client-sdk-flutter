import 'package:flutter/material.dart';

class AppG {
  final GlobalKey<NavigatorState> navigatorKey = new GlobalKey<NavigatorState>();
  Map<String, dynamic>? localizedStrings;
  late BuildContext context = navigatorKey.currentState!.context;
  String fontFamily = 'DefaultFont';
  var localizations;
  late ThemeData darkTheme;
  late ThemeData lightTheme;
  bool isDebug = false;
  bool isImageDebug = false;
  String formatDate = "d/M/y";
}
