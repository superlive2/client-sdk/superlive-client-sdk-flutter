import '../../app_package.dart';

class AppHelperColor {
  static Color fieldTitle(BuildContext context) {
    return isThemeDark(context) ? Colors.grey.withOpacity(0.7) : const Color(0xFFB0BAC7);
  }
  static Color fieldTitleFocus(BuildContext context) {
    return isThemeDark(context) ? Colors.grey.withOpacity(0.7) : const Color(0xFF80A9EA);
  }
  static Color greyColor(BuildContext context){
    return isThemeDark(context) ? Colors.white.withOpacity(0.6) : Colors.grey[500]!;
  }
  static Color greyDividerColor(BuildContext context){
    return isThemeDark(context) ? Colors.grey.withOpacity(0.2) :Colors.grey.withOpacity(0.3);
  }
}
