import 'package:app_packages/time_ago.dart';

class KmCustomMessages implements LookupMessages {
  @override
  String prefixAgo() => '';
  @override
  String prefixFromNow() => '';
  @override
  String suffixAgo() => '';
  @override
  String suffixFromNow() => '';
  @override
  String lessThanOneMinute(int seconds) => 'មុននេះបន្តិច';
  @override
  String aboutAMinute(int minutes) => '$minutes នាទីមុន';
  @override
  String minutes(int minutes) => '$minutes នាទីមុន';
  @override
  String aboutAnHour(int minutes) => '$minutes នាទីមុន';
  @override
  String hours(int hours) => '$hours ម៉ោងមុន';
  @override
  String aDay(int hours) => '$hours ម៉ោងមុន';
  @override
  String days(int days) => '$days ថ្ងៃមុន';
  @override
  String aboutAMonth(int days) => '$days ថ្ងៃមុន';
  @override
  String months(int months) => '$months ខែមុន';
  @override
  String aboutAYear(int year) => '$year ឆ្នាំមុន';
  @override
  String years(int years) => '$years ឆ្នាំមុន';
  @override
  String wordSeparator() => ' ';
}
