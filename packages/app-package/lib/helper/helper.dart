import 'dart:io';
import 'dart:typed_data';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart' as p;
import 'package:app_packages/app_package.dart';
import 'package:timeago/timeago.dart' as timeago;
export 'http/http.dart';
export 'color/color_helper.dart';
export 'widget/widget_helper.dart';
export 'ago/km.dart';
export 'message/snack_bar.dart';
export 'translate/lookup_translate.dart';


extension StringExtension on dynamic {
  String toFirstUppercase() {
    try {
      return "${this![0].toUpperCase()}${this!.substring(1)}";
    } catch (_) {
      return this;
    }
  }

  String toFirstUppercaseWord() {
    try {
      String stringWord = '';
      List<String> stringList = this!.split(' ');
      if (stringList == null ? false : (stringList.isNotEmpty)) {
        for (var v in stringList) {
          stringWord = (stringWord == '' ? stringWord : stringWord + ' ') + "${v[0].toUpperCase()}${v.substring(1)}";
        }
        return stringWord;
      }
      return "${this![0].toUpperCase()}${this!.substring(1)}";
    } catch (_) {
      return this;
    }
  }

  String toAppRemoveFirstZero() {
    try {
      String _text = toString().toAppString()!;
      if (_text[0] == "0") {
        return _text.substring(1);
      } else {
        return this;
      }
    } catch (_) {
      return this;
    }
  }

  String? toAppString({String? defaultVal = ''}) {
    String? text = '$this';
    return text == 'null' ? defaultVal : text;
  }

  String toTrTest() {
    String tr = toString().toAppString()!;
    return tr;
  }

  double toAppDouble({double defaultVal = 0.0}) {
    double coNum = this == null || this == 'null' || this == '' ? defaultVal : double.parse('$this');
    return coNum;
  }

  int toAppInt({int defaultVal = 0}) {
    int coNum = this == null || this == 'null' || this == '' ? defaultVal : int.parse('$this');
    return coNum;
  }

  String toDate({bool time = true, String? formatDate}) {
    try {
      String sr = toString().toAppString()!;
      final dt = time ? stringDateTimeToDate(sr) : stringDateToDate(sr);
      return formatDateOfDay(dt, format: formatDate ?? appG.formatDate);
    } catch (_) {
      return toString().toAppString()!;
    }
  }

  String toDateTime({bool time = true, String? formatDate, bool format24 = false}) {
    try {
      String sr = toString().toAppString()!;
      final dt = time ? stringDateTimeToDate(sr) : stringDateToDate(sr);
      final formatTime = format24 ? DateFormat.Hm() : DateFormat.jm();
      return formatDateOfDay(dt, format: formatDate ?? appG.formatDate) + " " + (formatTime.format(dt));
    } catch (_) {
      return toString().toAppString()!;
    }
  }

  String toTime({bool format24 = false, bool isSecond = true}) {
    try {
      String sr = toString().toAppString()!;
      List<String> timeSplit = sr.split(":");
      int hour = int.parse(timeSplit[0]);
      int minute = int.parse(timeSplit[1]);
      int second = 0;
      if (isSecond) {
        second = int.parse(timeSplit[2]);
      }
      final now = DateTime.now();
      final formatTime = format24 ? DateFormat.Hm() : DateFormat.jm();
      final dt = DateTime(now.year, now.month, now.day, hour, minute, second);
      return formatTime.format(dt);
    } catch (_) {
      return toString().toAppString()!;
    }
  }

  String toAgo({bool time = true}) {
    String sr = toString().toAppString()!;
    try {
      final dt = time ? stringDateTimeToDate(sr) : stringDateToDate(sr);
      return timeago.format(dt);
    } catch (_) {
      return sr;
    }
  }
}

double convertStringToDouble(dynamic text, {double defaultVal = 0.0}) {
  double coNum = text == null || text == 'null' || text == '' ? defaultVal : double.parse(text.toString());
  return coNum;
}

int convertStringToInt(dynamic text, {int defaultVal = 0}) {
  int coNum = text == null || text == 'null' || text == '' ? defaultVal : int.parse(text.toString());
  return coNum;
}

bool checkKeyMap(Map? arr, dynamic key) {
  if (arr == null || key == null) {
    return false;
  }
  return arr.containsKey(key);
}

Future<double> oneGetFileSizeInMb(File file) async {
  int sizeInBytes = file.lengthSync();
  return sizeInBytes / (1024 * 1024);
}

DateTime stringDateTimeToDate(String dateTime) {
  return stringDateToDateObject(dateTime);
}

DateTime stringDateToDate(String dateTime) {
  return stringDateToDateObject(dateTime, format: "yyyy-MM-dd");
}

DateTime stringDateToDateObject(String dateTime, {String format = "yyyy-MM-dd HH:mm:ss"}) {
  DateTime dt = DateFormat(format).parse(dateTime);
  return dt;
}

String formatDateOfDay(DateTime tod, {String? format}) {
  final dt = DateTime(tod.year, tod.month, tod.day, tod.hour, tod.minute);
  final formats = DateFormat(format ?? appG.formatDate);
  return formats.format(dt);
}

formatTimeOfDay(TimeOfDay tod, {returnObject = false, return24FormatTime = false}) {
  final now = DateTime.now();
  final dt = DateTime(now.year, now.month, now.day, tod.hour, tod.minute);
  if (returnObject) {
    return dt;
  } else if (return24FormatTime) {
    final format = DateFormat.Hm();
    return format.format(dt);
  }
  final format = DateFormat.jm(); //"6:00 AM"
  return format.format(dt);
}

Theme appTheme(BuildContext context, {required Widget child}) {
  return Theme(
    data: Theme.of(context),
    child: child,
  );
}

TextTheme appTextTheme(BuildContext context) => Theme.of(context).textTheme;

ButtonThemeData appButtonThemeData(BuildContext context) => Theme.of(context).buttonTheme;

InputDecorationTheme appInputDecorationTheme(BuildContext context) => Theme.of(context).inputDecorationTheme;

TextStyle appInputLabelStyleTheme(BuildContext context) => Theme.of(context).inputDecorationTheme.labelStyle!;

bool isThemeDark(BuildContext context) {
  return Theme.of(context).brightness == Brightness.dark;
}

ThemeMode appGetMode(AppThemeMode mode) {
  if (mode == AppThemeMode.dark) {
    return ThemeMode.dark;
  } else if (mode == AppThemeMode.system) {
    final brightness = SchedulerBinding.instance.window.platformBrightness;
    if (brightness == Brightness.dark) {
      return ThemeMode.dark;
    } else {
      return ThemeMode.light;
    }
  } else {
    return ThemeMode.light;
  }
}

Color appIconColor(context) {
  return Colors.black; //Theme.of(context).appBarTheme.iconTheme!.color!;
}

Color appBackgroundColor(BuildContext context) {
  try {
    return Theme.of(context).colorScheme.background;
  } catch (e) {
    return Colors.white;
  }
}

Color appBackgroundDialogColor(BuildContext context) {
  try {
    return Theme.of(context).dialogBackgroundColor;
  } catch (e) {
    return Colors.white;
  }
}

String appExtension(String value) {
  String extension = p.extension(value);
  return extension.replaceFirst('.', '');
}

Uint8List? appFileToBytes({String? path, File? file}) {
  File? _file = file;
  if (file == null) {
    _file = File(path!);
  }
  if (_file == null) {
    return null;
  }
  return Uint8List.fromList(_file.readAsBytesSync());
}

String appNameWithExtension(String value) {
  return p.basename(value);
}

bool appIsEmail(String em) {
  String p = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
  RegExp regExp = RegExp(p);

  return regExp.hasMatch(em);
}


Future<File> getImageFileFromAssetString(String path) async {
  final byteData = await rootBundle.load('assets/$path');
  final file = File('${(await getTemporaryDirectory()).path}/$path');
  await file.writeAsBytes(byteData.buffer.asUint8List(byteData.offsetInBytes, byteData.lengthInBytes));
  return file;
}
