import 'dart:convert';
import 'dart:io';
import 'package:app_packages/app_package.dart';
import 'dart:async';
import 'package:path/path.dart' as path;

import '../telegram_report/telegram_report.dart';

class AppPKHttp {
  static const int _timeOutInSeconds = 10;
  static final dio = Dio();

  static Future<Map<String, dynamic>> requestMethod({
    String url = '',
    int? timeOut,
    AppHttpType type = AppHttpType.get,
    Map<String, dynamic>? fields,
    Map<String, List<dynamic>>? fieldsList,
    Map<String, File>? files,
    Map<String, List<File>>? filesList,
    Map<String, String>? headers,
    CancelToken? cancelToken,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    try {
      if (url == '') {
        return Future.error(AppHttpError(status: AppHttpStatus.error, message: tr.sthUn, errorCode: ""));
      }


      Map<String, dynamic>? _fields = {};
      if (fields != null) {
        if (fields.isNotEmpty) {
          fields.forEach((name, val) async {
            // _fields[name] = val is bool ? val: '$val';
            _fields[name] = val;
          });
        }
      }

      Map<String, MultipartFile>? _files = await _getFile(files);
      if (_files != null) {
        _fields.addAll(_files);
      }
      Map<String, MultipartFile>? _filesList = await _getFileList(filesList);
      if (_filesList != null) {
        _fields.addAll(_filesList);
      }
      Map<String, String>? _fieldList = _getFieldList(fieldsList);
      if (_fieldList != null) {
        _fields.addAll(_fieldList);
      }
      TelegramReport.send(ReportModel(
        message: "URL: $url \n[BaseRequest: GET]: $_fieldList",
      ));
      Response response;
      if (type == AppHttpType.get) {
        response = await dio.get(
          url,
          queryParameters: fields,
          options: Options(
            headers: headers,
            sendTimeout: Duration(seconds: timeOut ?? _timeOutInSeconds),
            receiveTimeout: Duration(seconds: timeOut ?? _timeOutInSeconds),
          ),
          cancelToken: cancelToken,
          onReceiveProgress: onReceiveProgress,
        );
      } else {
        Object _formData = _fields;
        if (_files == null ? false : _files.isEmpty || _filesList == null ? false : _filesList.isEmpty) {
          _formData = FormData.fromMap(_fields);
        }
        TelegramReport.send(ReportModel(
          message: "URL: $url \n[BaseRequest: POST]: $_formData",
        ));
        response = await dio.post(
          url,
          data: _formData,
          options: Options(
            headers: headers,
            sendTimeout: Duration(seconds: timeOut ?? _timeOutInSeconds),
            receiveTimeout: Duration(seconds: timeOut ?? _timeOutInSeconds),
          ),
          cancelToken: cancelToken,
          onReceiveProgress: onReceiveProgress,
          onSendProgress: onSendProgress,
        );
      }
      debugPrint("=========::::::URL::::: $url");
      if (response.statusCode == 200 || response.statusCode == 201 || response.statusCode == 202) {
        debugPrint("=========::::::DATA::::: ${response.data}");
        TelegramReport.send(ReportModel(
          message: "[BaseResponse]: [--]",
          statusCode: "${response.statusCode}"
        ));
        try {
          return _oneResponse(response.toString()).then((value) {
            return value;
          });
        } on SocketException {
          return Future.error(AppHttpError(status: AppHttpStatus.fetchDataError, message: tr.noInternet, errorCode: ""));
        } on TimeoutException {
          return Future.error(AppHttpError(status: AppHttpStatus.timeout, message: tr.timeout, errorCode: ""));
        } catch (e) {
          return Future.error(AppHttpError(status: AppHttpStatus.error, message: e, errorCode: ""));
        }
      } else {
        debugPrint("=========::::::URL:::::ERROR2 $url");
        TelegramReport.send(ReportModel(
          message: "URL: $url  \n[BaseResponse]: Response: ${response.statusMessage}",
          statusCode: "${response.statusCode}"
        ));
        final _result = response.toString();
        return Future.error(_errorMethod(response.statusCode!, _result)); ///
      }
    } on SocketException {
      return Future.error(AppHttpError(status: AppHttpStatus.fetchDataError, message: tr.noInternet, errorCode: ""));
    } on TimeoutException {
      return Future.error(AppHttpError(status: AppHttpStatus.timeout, message: tr.timeout, errorCode: ""));
    } catch (e) {
      if (e is DioException) {
        try {
          debugPrint("=========::::::URL:::::ERROR3 $url");
          TelegramReport.send(ReportModel(
            message: "URL: $url  \n Exception: $e",
          ));
          return Future.error(_errorMethod(e.response!.statusCode!, e.response.toString())); ///
        } catch (_) {}
      }
      return Future.error(AppHttpError(status: AppHttpStatus.error, message: e.toString(), errorCode: ""));
    }
  }

  static AppHttpError _errorMethod(int statusCode, String result) {
    debugPrint("=========::::::ERROR CODE::::: $statusCode");
    debugPrint("=========::::::ERROR MESSAGE::::: $result");
    AppHttpStatus _statusCode = _checkStatusCode(statusCode);
    String _message = "Something unexpected went wrong";
    String _errorCode = "";
    try {
      Map<String, dynamic> _result = json.decode(result);
      _message = checkKeyMap(_result, 'message') ? _result['message'] : tr.sthUn;
      try {
        _errorCode = checkKeyMap(_result, 'error') ? _result['error'] : "";
      } catch(e) {}
    } catch (_) {}
    return AppHttpError(status: _statusCode, message: _message, errorCode: _errorCode);
  }

  /// FieldList
  static Map<String, String>? _getFieldList(Map<String, List<dynamic>>? value) {
    if (value == null) return null;
    Map<String, String> _val = {};
    if (value.isNotEmpty) {
      for (final mapEntry in value.entries) {
        final name = mapEntry.key;
        final val = mapEntry.value;
        Map<String, String>? _filesBody = _getFieldListBody(val: val, name: name);
        if (_filesBody != null) {
          _val.addAll(_filesBody);
        }
      }
    }
    return _val.isEmpty ? null : _val;
  }

  static Map<String, String>? _getFieldListBody({required List<dynamic> val, required String name}) {
    Map<String, String>? _val = {};
    int _i = 0;
    for (final element in val) {
      if (name != '') {
        _val["$name[$_i]"] = '$element';
      }
      _i += 1;
    }
    return _val.isEmpty ? null : _val;
  }

  /// FileList
  static Future<Map<String, MultipartFile>?> _getFileList(Map<String, List<File>>? files) async {
    if (files == null) return null;
    Map<String, MultipartFile> _files = {};
    if (files.isNotEmpty) {
      for (final mapEntry in files.entries) {
        final name = mapEntry.key;
        final val = mapEntry.value;
        List<File> _val = val;
        Map<String, MultipartFile>? _filesBody = await _getFileListBody(val: _val, name: name);
        if (_filesBody != null) {
          _files.addAll(_filesBody);
        }
      }
    }
    return _files.isEmpty ? null : _files;
  }

  static Future<Map<String, MultipartFile>?> _getFileListBody({required List<File> val, required String name}) async {
    Map<String, MultipartFile> _files = {};
    int _i = 0;
    for (final element in val) {
      if (name != '') {
        Map<String, MultipartFile>? _filesBody = await _getFileListBodyChild(element: element, name: name, i: _i);
        if (_filesBody != null) {
          _files.addAll(_filesBody);
        }
      }
      _i += 1;
    }
    return _files.isEmpty ? null : _files;
  }

  static Future<Map<String, MultipartFile>?> _getFileListBodyChild({required File element, required String name, required int i}) async {
    Map<String, MultipartFile> _files = {};
    _files[name] = await MultipartFile.fromFile(element.path, filename: path.basename(element.path));
    return _files.isEmpty ? null : _files;
  }

  static Future<Map<String, MultipartFile>?> _getFile(Map<String, File>? files) async {
    Map<String, MultipartFile> _files = {};
    if (files != null) {
      if (files.isNotEmpty) {
        for (final mapEntry in files.entries) {
          final name = mapEntry.key;
          final val = mapEntry.value;
          if (name != '') {
            _files[name] = await MultipartFile.fromFile(val.path, filename: path.basename(val.path));
          }
        }
      }
    }
    return _files.isEmpty ? null : _files;
  }

  static Future<Map<String, dynamic>> _oneResponse(String response) async {
    try {
      Map<String, dynamic> result = json.decode(response);
      if (checkKeyMap(result, 'statusCode')) {
        final _statusCode = result['statusCode'].toString().toAppInt();
        if (_statusCode == 200 || _statusCode == 201 || _statusCode == 202) {
          return result;
        }
      }
      if (checkKeyMap(result, 'statusCode')) {
        AppHttpStatus statusCode = _checkStatusCode(result['statusCode'].toString().toAppInt());
        return Future.error(AppHttpError(status: statusCode, message: checkKeyMap(result, 'message') ? result['message'] : tr.sthUn, errorCode: ""));
      }
      //throw Exception(checkKeyMap(result, 'message') ? result['message'] : 'text.sth_un');
      return Future.error(AppHttpError(status: AppHttpStatus.error, message: checkKeyMap(result, 'message') ? result['message'] : tr.sthUn, errorCode: ""));
    } catch (e) {
      return Future.error(AppHttpError(status: AppHttpStatus.error, message: e, errorCode: ""));
    }
  }

  static AppHttpStatus _checkStatusCode(int statusCode) {
    if (statusCode == 401) {
      return AppHttpStatus.tokenExpired;
    } else if (statusCode == 500) {
      return AppHttpStatus.internalServerError;
    } else if (statusCode == 422) {
      return AppHttpStatus.refreshTokenExpired;
    } else if (statusCode == 400) {
      return AppHttpStatus.error;
    }
    return AppHttpStatus.success;
  }
}
