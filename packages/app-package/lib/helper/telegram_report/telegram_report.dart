// ignore_for_file: public_member_api_docs, sort_constructors_first, depend_on_referenced_packages
import 'dart:developer';
import 'package:http/http.dart' as http;

class TelegramReport {
  static Future send(ReportModel reportModel) async {
    var statusCode = reportModel.statusCode.isEmpty ? 'n/a' : reportModel.statusCode;
    var botToken = '6503517734:AAH5Sm0p_Qmk2xc2qiTAvuLrOdK3BUjZknQ';
    var groupLink = '@superlivecrashanalystics';
    var message = '''${reportModel.message}\nstatusCode: $statusCode''';
    try {
      final Uri uri = Uri.https(
        'api.telegram.org',
        '/bot$botToken/sendMessage',
        {
          'chat_id': groupLink,
          'text': message,
          'parse_mode': 'html',
        },
      );
      return http.get(uri);
    } catch (e) {
      log(e.toString(), name: 'Telegram Report');
    }
  }
}

class ReportModel {
  String message = '';
  String statusCode = '';
  ReportModel({
    this.message = '',
    this.statusCode = '',
  });
}
