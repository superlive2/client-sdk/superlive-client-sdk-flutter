/// [LookupTranslate] template for any language
abstract class LookupTranslate {
  String get date;

  String get gallery;

  String get camera;

  String get cancel;

  String get addImage;

  String get edit;

  String get time;

  String get close;

  String get permissionDenied;

  String get permission;

  String get pleaseAllowPermissionToAccess;

  String get yes;

  String get no;

  String get ok;

  String get off;

  String get action;

  String get warningMsg;

  String get sthUn;

  String get options;

  String get normal;

  String get subtitles;

  String get disconnect;

  String get connectToADevice;

  String get searchingForDevices;

  String get noDeviceFound;

  String get noInternet;

  String get timeout;

  /// word separator when words are concatenated
  String wordSeparator() => ' ';
}
