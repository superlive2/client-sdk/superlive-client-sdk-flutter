import 'package:app_packages/app_package.dart';
import 'package:flutter/material.dart';

class AppHelperMessage {
  static showSnackBar(
    BuildContext context, {
    required String message,
    VoidCallback? onClose,
    bool isShowAction: true,
    SnackBarAction? customAction,
    String? customTextClose,
  }) {
    final snackBar = SnackBar(
      content: Text(
        message,
        style: appTextTheme(context).bodySmall!.copyWith(color: isThemeDark(context) ? Colors.grey[850] : Colors.white),
      ),
      backgroundColor: isThemeDark(context) ? Colors.white : Colors.grey[950],
      action: customAction ??
          (isShowAction
              ? SnackBarAction(
                  textColor: isThemeDark(context) ? Colors.grey[850] : Colors.white,
                  label: customTextClose ?? tr.close.toFirstUppercase(),
                  onPressed: () {
                    if (onClose != null) {
                      onClose();
                    }
                  },
                )
              : null),
    );
    // Find the Scaffold in the Widget tree and use it to show a SnackBar!
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }
}
