
import 'form_config.dart';
import '../app_package.dart';

class AppConfig {
  String? baseUrl;
  Map<String, String>? defaultHeader;
  String? baseUrlImg;
  String? defaultImageAsset;
  AppFilePath oneFilePath = AppFilePath();

  FieldConfig fieldConfig = FieldConfig();

  TextStyle oneGetModalBottomSheetTextStyle(BuildContext context) {
    return appTextTheme(context).headlineMedium!.copyWith(fontSize: 18, fontWeight: FontWeight.w600);
  }
}
