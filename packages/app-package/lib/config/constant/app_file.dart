class AppFilePath {
  String? errorAnimate;
  String? warningAnimate;
  String? successAnimate;
  String? success2Animate;

  AppFilePath({
    this.errorAnimate,
    this.warningAnimate,
    this.successAnimate,
    this.success2Animate,
  });

  AppFilePath copyWith({
    String? errorAnimate,
    String? warningAnimate,
    String? successAnimate,
    String? success2Animate,
  }) {
    return AppFilePath(
      errorAnimate: errorAnimate ?? this.errorAnimate,
      warningAnimate: warningAnimate ?? this.warningAnimate,
      successAnimate: successAnimate ?? this.successAnimate,
      success2Animate: success2Animate ?? this.success2Animate,
    );
  }
}
