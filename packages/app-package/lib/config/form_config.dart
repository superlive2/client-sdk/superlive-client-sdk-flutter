import 'package:app_packages/app_package.dart';
import 'package:flutter/material.dart';

class FieldConfig {
  static const colors = Colors.black87;
  final fieldColor = colors;
  static const double circular = 5;
  static const colorFocus = Color(0xff4caf50);
  final fieldContentPaddingBig = const EdgeInsets.fromLTRB(15.0, 15.0, 0.0, 15.0);
  final fieldContentPaddingMedium = const EdgeInsets.fromLTRB(15.0, 15.0, 0.0, 15.0);
  final fieldContentPaddingFocus = const EdgeInsets.fromLTRB(10.0, 9.0, 10.0, 9.0);
  final fieldContentPadding = const EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0);
  // final fieldContentPadding = new EdgeInsets.only(left: 17.0, top: 39.0, bottom: 0);
  // final fieldContentPaddingSmall = new EdgeInsets.fromLTRB(15.0, 15.0, 0.0, 0.0);
  final fieldStyle = TextStyle(fontWeight: FontWeight.normal, fontSize: 15, color: Colors.black87, fontFamily: appG.fontFamily);
  final fieldBorder = OutlineInputBorder(
    borderSide: const BorderSide(color: Color(0xFF80A9EA), width: 1.5),
    borderRadius: BorderRadius.circular(circular),
    gapPadding: 0,
  );
  final fieldBorderError = OutlineInputBorder(
    borderSide: const BorderSide(color: Colors.redAccent, width: 1.5),
    borderRadius: BorderRadius.circular(circular),
    gapPadding: 0,
  );
  final fieldEnabledBorder = OutlineInputBorder(
    borderSide: const BorderSide(color: Color(0xFFB0BAC7), width: 1),
    borderRadius: BorderRadius.circular(circular),
    gapPadding: 0,
  );
  final fieldEnabledBorderError = OutlineInputBorder(
    borderSide: const BorderSide(color: Colors.redAccent, width: 1),
    borderRadius: BorderRadius.circular(circular),
    gapPadding: 0,
  );
  final fieldEnabledBorderWithValue = OutlineInputBorder(
    borderSide: const BorderSide(color: Color(0xFFB0BAC7), width: 1),
    borderRadius: BorderRadius.circular(circular),
    gapPadding: 0,
  );
  final TextStyle fieldLabelStyle = TextStyle(
      color: const Color(0xFFB0BAC7),
      //color: Color(0xFFCBD5E1),
      //fontWeight: FontWeight.bold,
      fontSize: 15,
      height: 1,
      fontFamily: appG.fontFamily);
  final TextStyle fieldLabelHasFocusStyle = TextStyle(
    color: const Color(0xFF80A9EA),
    //fontWeight: FontWeight.bold,
    fontSize: 17,
    fontFamily: appG.fontFamily,
    height: 1,
  );
}
