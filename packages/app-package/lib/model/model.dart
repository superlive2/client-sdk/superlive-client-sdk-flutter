export 'bottomSheetActionModel.dart';

enum AppAllowChooseGalleryType { gallery, camera, both }

//report_video
enum AppVideoOptionsType { quality, audio, subtitles, speed }

enum AppVideoSourceType { network, file, asset }

enum AppSize { small, medium, big }

enum AppSvgImageSourceType { asset, url }
