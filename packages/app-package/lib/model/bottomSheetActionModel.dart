import 'package:flutter/cupertino.dart';

class BottomSheetActionModel {
  final String? text;
  final bool textBold;
  final IconData? icon;
  final Color? iconColor;
  final Widget? image;
  final String? imagePath;
  final Color? color;
  final Widget? rightWidget;
  final String? rightText;
  final VoidCallback? callbackAction;

  const BottomSheetActionModel({
    this.text,
    this.textBold: false,
    this.icon,
    this.image,
    this.imagePath,
    this.rightWidget,
    this.rightText,
    this.callbackAction,
    this.color,
    this.iconColor,
  });
}
