import 'package:cached_network_image/cached_network_image.dart';
import 'package:app_packages/app_package.dart';
import 'dart:ui' as ui;
import 'dart:async';

class AppWidgetImageUrl extends StatefulWidget {
  final String? url;
  final BoxFit? fit;
  final String? errorImageAsset;
  final Widget? errorWidget;
  final Widget? loadingWidget;

  const AppWidgetImageUrl({
    this.url,
    this.fit = BoxFit.cover,
    this.errorImageAsset,
    this.errorWidget,
    this.loadingWidget,
  });

  @override
  State<AppWidgetImageUrl> createState() => _AppWidgetImageUrlState();
}

class _AppWidgetImageUrlState extends State<AppWidgetImageUrl> with AutomaticKeepAliveClientMixin<AppWidgetImageUrl> {
  TextStyle? get _textStyle => Theme.of(context).textTheme.labelMedium!.copyWith(color: Colors.red, fontSize: 9);

  @override
  Widget build(BuildContext context) {
    super.build(context);
    if (appG.isDebug || widget.url == null || widget.url == 'null' || widget.url == '' || widget.url == '${appConfig.baseUrlImg}null' || widget.url == '${appConfig.baseUrlImg}') {
      return !appG.isDebug || !appG.isImageDebug
          ? _imageAssetWidget
          : Stack(
              children: [
                Positioned.fill(
                  child: _imageAssetWidget,
                ),
                if (widget.url != null)
                  Positioned.fill(
                    child: Center(
                      child: FutureBuilder<ui.Image>(
                        future: _getImage(),
                        builder: (BuildContext context, AsyncSnapshot<ui.Image> snapshot) {
                          if (snapshot.hasData) {
                            if (snapshot.data == null) {
                              return Text(
                                'no image...',
                                style: _textStyle,
                              );
                            }
                            ui.Image image = snapshot.data!;
                            return FutureBuilder<String>(
                              future: _getImageSize(image),
                              builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
                                if (snapshot.hasData) {
                                  return Text(
                                    '${image.width}x${image.height} (${snapshot.data}M)',
                                    style: _textStyle,
                                  );
                                } else {
                                  return Text(
                                    '${image.width}x${image.height}',
                                    style: _textStyle,
                                  );
                                }
                              },
                            );
                          } else {
                            return Text(
                              'loading...',
                              style: _textStyle,
                            );
                          }
                        },
                      ),
                    ),
                  ),
              ],
            );
    }
    return CachedNetworkImage(
      placeholder: (context, url) {
        return widget.loadingWidget ?? appShimmerWidget(context);
      },
      imageUrl: '${widget.url}',
      fit: widget.fit,
      errorWidget: (context, url, error) => widget.errorWidget != null ? widget.errorWidget! : _imageAssetWidget,
    );
  }

  Widget get _imageAssetWidget {
    String? errorImageAssetString = widget.errorImageAsset;
    errorImageAssetString ??= appConfig.defaultImageAsset;
    return Image.asset(
      '$errorImageAssetString',
      fit: widget.fit,
      opacity: isThemeDark(context) ? const AlwaysStoppedAnimation<double>(0.5) : null,
    );
  }

  Future<String> _getImageSize(ui.Image data) async {
    try {
      final result = await data.toByteData();
      if (result == null) {
        return '';
      }
      final bytes = result.lengthInBytes;
      final kb = bytes / 1024;
      final mb = kb / 1024;
      // if (mb > 1) {
      //   print("Big image (${mb}M): ${widget.url}");
      // }
      return mb.toStringAsFixed(2);
    } catch (e) {
      return '';
    }
  }

  Future<ui.Image> _getImage() {
    Completer<ui.Image> completer = Completer<ui.Image>();
    NetworkImage(widget.url!).resolve(const ImageConfiguration()).addListener(
      ImageStreamListener((ImageInfo info, bool _) {
        completer.complete(info.image);
      }),
    );
    return completer.future;
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
