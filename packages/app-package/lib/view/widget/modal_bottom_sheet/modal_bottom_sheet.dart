import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:app_packages/app_package.dart';

void appModalBottomSheet(
  context, {
  String? text,
  Widget? build,
  bool withOK = false,
  bool close = true,
  bool autoHeight = false,
  bool fullScreen = false,
  GestureTapCallback? onOK,
  GestureTapCallback? onClose,
  bool scrollAble = true,
  Color? backgroundColor,
  EdgeInsetsGeometry? padding,
  EdgeInsetsGeometry? margin,
  double? height,
  double? heightFull,
  bool styleBorderRadius = true,
}) async {
  if (scrollAble && (heightFull == null || height == null)) {
    autoHeight = false;
  }
  Color _backgroundColor = backgroundColor ?? appBackgroundDialogColor(context);
  //30
  Decoration decoration = BoxDecoration(
    color: _backgroundColor,
    borderRadius: styleBorderRadius ? const BorderRadius.only(topLeft: Radius.circular(15.0), topRight: Radius.circular(15.0)) : null,
  );
  return showModalBottomSheet(
    backgroundColor: Colors.transparent,
    context: context,
    isScrollControlled: heightFull != null || height != null ? true : false,
    builder: (context) {
      return Builder(
        builder: (context) {
          final double _fullHeight = MediaQuery.of(context).size.height;
          final double _padding = MediaQuery.of(context).padding.top + MediaQuery.of(context).padding.bottom;
          final double _heightModal = _fullHeight - _padding - 25;
          final double _paddingBottom = MediaQuery.of(context).viewInsets.bottom;
          return appTheme(
            context,
            child: AppGestureDetector(
              onTap: () => AppHelperWidget.focusNew(context),
              child: Container(
                height: _paddingBottom > 0 && (heightFull == null ? true : _heightModal > heightFull) ? _heightModal : heightFull,
                decoration: decoration,
                child: SafeArea(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      if (styleBorderRadius)
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  width: 65,
                                  height: 4,
                                  margin: const EdgeInsets.only(top: 8),
                                  decoration: BoxDecoration(color: isThemeDark(context) ? Colors.white.withOpacity(0.2) : Colors.black12, borderRadius: BorderRadius.circular(5)),
                                ),
                              ],
                            ),
                          ],
                        ),
                      if (close == false && withOK == false && text == null)
                        const SizedBox(
                          width: 0,
                          height: 9,
                        )
                      else
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Expanded(
                              child: text != null
                                  ? Container(
                                      padding: const EdgeInsets.only(right: 15, top: 20, bottom: 5, left: 15),
                                      child: Text(
                                        text,
                                        style: appConfig.oneGetModalBottomSheetTextStyle(context),
                                      ),
                                    )
                                  : appZero(),
                            ),
                            /*Expanded(
                                    flex: 5,
                                    child: Container(
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Container(
                                            width: 65,
                                            height: 7,
                                            margin: EdgeInsets.only(top: 12),
                                            decoration: BoxDecoration(
                                                color: Colors.black12,
                                                borderRadius: new BorderRadius.circular(5)
                                            ),
                                          ),
                                        ],
                                      ),
                                    )
                                ),*/
                            Container(
                              child: close && !withOK
                                  ? GestureDetector(
                                      onTap: onClose ??
                                          () {
                                            Navigator.of(context, rootNavigator: true).pop();
                                          },
                                      child: Container(
                                        padding: const EdgeInsets.only(right: 15, top: 15, bottom: 5, left: 5),
                                        child: Icon(
                                          Icons.close,
                                          size: 27,
                                          color: isThemeDark(context) ? Colors.white : Colors.black87,
                                        ),
                                      ),
                                    )
                                  : Container(
                                      child: withOK == true
                                          ? InkWell(
                                              onTap: onOK ??
                                                  () {
                                                    Navigator.of(context, rootNavigator: true).pop();
                                                  },
                                              child: Container(
                                                padding: const EdgeInsets.only(right: 30, top: 20, bottom: 5, left: 5),
                                                child: Text(
                                                  'ok'.toUpperCase(),
                                                  textAlign: TextAlign.end,
                                                  style: appTextTheme(context).bodyMedium,
                                                ),
                                              ),
                                            )
                                          : Container(
                                              padding: EdgeInsets.only(right: 30, top: text != null ? 40 : 0, bottom: 5, left: 5),
                                              color: Colors.transparent,
                                              child: appZero(),
                                            ),
                                    ),
                            ),
                          ],
                        ),
                      // if(text != null)

                      autoHeight
                          ? build != null
                              ? Container(padding: padding, margin: margin, color: _backgroundColor, child: build)
                              : appZero()
                          : Expanded(
                              child: Container(
                                height: height,
                                margin: margin,
                                padding: padding,
                                color: _backgroundColor,
                                child: build != null
                                    ? (scrollAble ? AppWidgetScroll(child: build) : build)
                                    : const SizedBox(
                                        width: 0,
                                        height: 0,
                                      ),
                              ),
                            ),
                    ],
                  ),
                ),
              ),
            ),
          );
        },
      );
    },
  );
}
