import 'package:app_packages/app_package.dart';

class AppLoadingOverWidget extends StatefulWidget {
  final Widget child;
  final bool isShowLoading;

  const AppLoadingOverWidget({Key? key, required this.child, this.isShowLoading: false}) : super(key: key);

  @override
  State<AppLoadingOverWidget> createState() => _AppLoadingOverWidgetState();
}

class _AppLoadingOverWidgetState extends State<AppLoadingOverWidget> with AutomaticKeepAliveClientMixin<AppLoadingOverWidget> {
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Stack(
      children: [
        Positioned.fill(child: widget.child),
        if (widget.isShowLoading)
          Positioned(
            left: 0,
            right: 0,
            top: 5,
            child: Center(
              child: Container(
                  margin: const EdgeInsets.symmetric(vertical: 8),
                  padding: const EdgeInsets.symmetric(horizontal: 7, vertical: 7),
                  width: 35,
                  height: 35,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(1000),
                    color: appBackgroundColor(context),
                  ),
                  child: const CircularProgressIndicator()),
            ),
          ),
      ],
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
