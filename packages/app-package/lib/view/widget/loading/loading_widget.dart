import 'package:app_packages/app_package.dart';

class AppLoadingContainer extends StatelessWidget {
  final Color? color;
  final EdgeInsets margin;

  const AppLoadingContainer({
    Key? key,
    this.color,
    this.margin = const EdgeInsets.symmetric(vertical: 8),
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SizedBox(
        width: 15,
        height: 15,
        child: CircularProgressIndicator(
          color: color,
        ),
      ),
    );
  }
}
