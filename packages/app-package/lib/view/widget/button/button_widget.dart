import '../../../app_package.dart';

class AppButtonWidget extends StatelessWidget {
  final GestureTapCallback? onTap;
  final String text;
  final Color? color;
  final TextStyle? textStyle;

  const AppButtonWidget({
    Key? key,
    this.onTap,
    this.text = "ok",
    this.color,
    this.textStyle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      borderRadius: BorderRadius.circular(12),
      child: Container(
        alignment: Alignment.center,
        padding: const EdgeInsets.symmetric(vertical: 12),
        decoration: BoxDecoration(
          color: color ?? Colors.grey.withOpacity(0.3),
          borderRadius: BorderRadius.circular(12),
        ),
        child: Text(
          text,
          style: textStyle ?? appTextTheme(context).headline3!.copyWith(fontSize: 16),
        ),
      ),
    );
  }
}
