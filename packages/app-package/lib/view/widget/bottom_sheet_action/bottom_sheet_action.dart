import '../../../app_package.dart';

void appBottomSheetAction(
  context, {
  String? text,
  List<BottomSheetActionModel>? list,
  bool scrollAble = false,
  bool styleBorderRadius = true,
  BoxDecoration? itemBoxDecoration,
}) {
  Widget build = list != null
      ? Container(
          margin: const EdgeInsets.only(top: 10, left: 20, right: 20, bottom: 30),
          child: Column(
            children: List.generate(list.length, (index) {
              BottomSheetActionModel row = list[index];
              return InkWell(
                onTap: row.callbackAction,
                child: Container(
                  margin: const EdgeInsets.symmetric(
                    vertical: 8,
                  ),
                  child: Container(
                    padding: itemBoxDecoration == null
                        ? EdgeInsets.zero
                        : const EdgeInsets.symmetric(
                            vertical: 10,
                            horizontal: 8,
                          ),
                    decoration: itemBoxDecoration,
                    child: Row(
                      children: [
                        Expanded(
                          child: Row(
                            children: [
                              row.icon != null
                                  ? Icon(row.icon, size: 21, color: (row.iconColor ?? row.color) ?? appIconColor(context))
                                  : Container(
                                      width: 20,
                                      height: 20,
                                      constraints: const BoxConstraints(maxWidth: 20, maxHeight: 20),
                                      child: (row.image != null ? row.image! : (row.imagePath != null ? AppWidgetSvgImage(path: row.imagePath!, color: (row.iconColor ?? row.color) ?? appIconColor(context), width: 20, height: 20, fit: BoxFit.contain) : appZero())),
                                    ),
                              if (row.text != null)
                                Expanded(
                                  child: Container(
                                    margin: const EdgeInsets.only(left: 10),
                                    child: Text(
                                      row.text!,
                                      style: TextStyle(
                                        fontSize: 16,
                                        color: row.color,
                                      ),
                                    ),
                                  ),
                                )
                            ],
                          ),
                        ),
                        if (row.rightWidget != null)
                          Container(
                            margin: const EdgeInsets.only(right: 15),
                            width: 20,
                            height: 20,
                            constraints: const BoxConstraints(maxWidth: 20, maxHeight: 20),
                            child: row.rightWidget,
                          )
                      ],
                    ),
                  ),
                ),
              );
            }),
          ),
        )
      : appZero();
  appModalBottomSheet(context, text: text, build: build, autoHeight: true, close: false, styleBorderRadius: styleBorderRadius, scrollAble: scrollAble);
}
