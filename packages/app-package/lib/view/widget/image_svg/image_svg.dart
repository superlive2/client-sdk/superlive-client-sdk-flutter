import 'package:flutter_svg/flutter_svg.dart';
import 'package:app_packages/app_package.dart';

class AppWidgetSvgImage extends StatelessWidget {
  final String? path;
  final BoxFit fit;
  final String? errorImageAsset;
  final double? width;
  final double? height;
  final AppSvgImageSourceType sourceType;
  final Color? color;

  const AppWidgetSvgImage({this.path, this.fit: BoxFit.cover, this.errorImageAsset, this.width, this.height, this.sourceType: AppSvgImageSourceType.asset, this.color});

  @override
  Widget build(BuildContext context) {
    String? oneErrorImageAsset = errorImageAsset;
    if (errorImageAsset == null) {
      oneErrorImageAsset = appConfig.defaultImageAsset;
    }
    String? onePath = path ?? oneErrorImageAsset;
    if (path == null || sourceType == AppSvgImageSourceType.asset) {
      return appExtension('$onePath') == 'svg'
          ? SvgPicture.asset(
              '$onePath',
              fit: fit,
              width: width,
              height: height,
              color: color ?? appIconColor(context),
            )
          : Image.asset(
              '$onePath',
              fit: fit,
              width: width,
              height: height,
              color: color,
            );
    } else {
      return appExtension('$onePath') == 'svg'
          ? SvgPicture.network(
              '$onePath',
              fit: fit,
              width: width,
              height: height,
              color: color ?? appIconColor(context),
            )
          : SizedBox(
              width: width,
              height: height,
              child: AppWidgetImageUrl(url: path, fit: fit, errorImageAsset: errorImageAsset),
            );
    }
  }
}
