import 'package:app_packages/app_package.dart';

class AppListViewBuilder {
  final ScrollController? controller;
  final List<Widget> children;
  final Axis scrollDirection;
  final bool? shrinkWrap;
  final ScrollPhysics? physics;
  final Key? oneKey;

  const AppListViewBuilder({
    this.controller,
    required this.children,
    this.scrollDirection = Axis.vertical,
    this.shrinkWrap,
    this.physics,
    this.oneKey,
  });

  Widget builder() {
    return ListView.builder(
      padding: EdgeInsets.zero,
      cacheExtent: 1500,
      key: oneKey,
      physics: physics ?? AppHelperWidget.scrollPhysics,
      itemCount: children.length,
      controller: controller,
      scrollDirection: scrollDirection,
      addAutomaticKeepAlives: true,
      shrinkWrap: shrinkWrap ?? false,
      itemBuilder: (BuildContext context, int index) {
        return children[index];
      },
    );
  }

  Widget separated(IndexedWidgetBuilder separatorBuilder) {
    return ListView.separated(
      separatorBuilder: separatorBuilder,
      padding: EdgeInsets.zero,
      cacheExtent: 1500,
      key: oneKey,
      physics: physics ?? AppHelperWidget.scrollPhysics,
      itemCount: children.length,
      controller: controller,
      scrollDirection: scrollDirection,
      addAutomaticKeepAlives: true,
      shrinkWrap: shrinkWrap ?? false,
      itemBuilder: (BuildContext context, int index) {
        return children[index];
      },
    );
  }
}
