import 'package:app_packages/app_package.dart';
import 'package:shimmer/shimmer.dart';

Widget appShimmerWidget(BuildContext context, {Widget? child}) {
  return Shimmer.fromColors(
    period: const Duration(milliseconds: 700),
    baseColor: isThemeDark(context) ? Colors.grey[850]! : Colors.grey[300]!,
    highlightColor: isThemeDark(context) ? Colors.grey[800]! : Colors.grey[100]!,
    child: child ?? Container(
            color: Colors.grey,
          ),
  );
}
