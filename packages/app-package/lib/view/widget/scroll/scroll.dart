import 'package:app_packages/app_package.dart';
import 'package:flutter/material.dart';

class AppWidgetScroll extends StatelessWidget {
  final Widget? child;
  final ValueChanged<String>? onRefresh;
  final ScrollController? scrollController;
  final Axis? scrollDirection;
  final ScrollPhysics? physics;

  const AppWidgetScroll({
    this.child,
    this.onRefresh,
    this.scrollController,
    this.scrollDirection,
    this.physics,
  });

  @override
  Widget build(BuildContext context) {
    if (onRefresh != null) {
      return RefreshIndicator(
        onRefresh: () async {
          if (onRefresh != null) {
            return onRefresh!('Change');
          }
        },
        child: CustomScrollView(
          scrollDirection: scrollDirection ?? Axis.vertical,
          controller: scrollController,
          physics: physics ?? AppHelperWidget.scrollPhysics,
          slivers: <Widget>[
            SliverToBoxAdapter(
              child: child,
            ),
          ],
        ),
      );
    }
    return CustomScrollView(
      scrollDirection: scrollDirection ?? Axis.vertical,
      controller: scrollController,
      physics: physics ?? AppHelperWidget.scrollPhysics,
      slivers: <Widget>[
        SliverToBoxAdapter(
          child: child,
        ),
      ],
    );
  }
}
