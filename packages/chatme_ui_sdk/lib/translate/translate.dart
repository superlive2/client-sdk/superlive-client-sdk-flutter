import 'package:chatme_ui_sdk/translate/translate/en_translate.dart';
import 'package:chatme_ui_sdk/translate/translate/lookup_translate.dart';
import 'package:chatme_ui_sdk/translate/translate/zh_translate.dart';
import 'package:chatme_ui_sdk/ui/screen/chatme_sdk.dart';

Map<String, LookupTranslateChatMeSdk> _lookupMessagesMap = {
  'en': EnTranslate(),
  'zh': ZhTranslate(),
};

/// Sets the default [locale]. By default it is `en`.
///
/// Example
/// ```
/// setLocaleMessages('fr', FrMessages());
/// setDefaultLocale('fr');
/// ```
void setDefaultLocale(String locale) {
  assert(_lookupMessagesMap.containsKey(locale), '[locale] must be a registered locale');
  language = locale;
}

/// Sets a [locale] with the provided [lookupMessages] to be available when
/// using the [format] function.
///
/// Example:
/// ```dart
/// setLocaleMessages('fr', FrMessages());
/// ```
///
/// If you want to define locale message implement [LookupMessages] interface
/// with the desired messages
///
void setLocaleMessages(String locale, LookupTranslateChatMeSdk lookupMessages) {
  _lookupMessagesMap[locale] = lookupMessages;
}

LookupTranslateChatMeSdk get chatMeTr {
  if (_lookupMessagesMap[language] == null) {
    print("Locale [$language] has not been added, using [$language] as fallback. To add a locale use [setLocaleMessages]");
  }
  return _lookupMessagesMap[language] ?? EnTranslate();
}
