import 'lookup_translate.dart';

/// Chinese language
class ZhTranslate implements LookupTranslateChatMeSdk {
  @override
  String get date => '日期';
  @override
  String get somethingWentWrong => '出了些问题';
  @override
  String get leaderBoard => "排行榜";
  @override
  String get noInternetConnection => "无网络";
  @override
  String get playing => "播放...";
  @override
  String get pts => "分";
  @override
  String get sendGift => "送礼物";
  @override
  String get somethingUnexpectedWentWrong => "出了些问题";

  @override
  String get followerOnlyChat => "只有关注者才能发消息";
  @override
  String get noRecent => "无近期历史";
  @override
  String get repliedTo => "回复";
  @override
  String get reply => "回复";
  @override
  String get replyingTo => "正在回复";
  @override
  String get sendAMessage => "发送了一个消息";
  @override
  String get copy => "复制";
  @override
  String get removed => "已删除";
  @override
  String get userBanned => "你已被禁止。";
  @override
  String get setTimeoutSendMessageAlert => "You can send message again after";

  @override
  String get cancel => "取消";
  @override
  String get defaultMessage => '您的信息未发送。点击 "再试一次 "重新发送此信息';
  @override
  String get reChargePoint => "充值积分";
  @override
  String get tryAgain => "再试一次";
}
