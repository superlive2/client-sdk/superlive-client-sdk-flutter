
import 'lookup_translate.dart';

/// English language
class EnTranslate implements LookupTranslateChatMeSdk {
  @override
  String get date => 'Date';
  @override
  String get somethingWentWrong => 'Something went wrong';
  @override
  String get leaderBoard => "Leaderboard";
  @override
  String get noInternetConnection => "No internet connection";
  @override
  String get playing => "Playing...";
  @override
  String get pts => "Point";
  @override
  String get sendGift => "Send Gift";
  @override
  String get somethingUnexpectedWentWrong => "Something unexpected went wrong.";

  @override
  String get followerOnlyChat => "Followers-Only Chat";
  @override
  String get noRecent => "No recent";
  @override
  String get repliedTo => "Replied to";
  @override
  String get reply => "Reply";
  @override
  String get replyingTo => "Replying to";
  @override
  String get sendAMessage => "Send a message";
  @override
  String get copy => "Copy";
  @override
  String get removed => "Removed";
  @override
  String get userBanned => "You has been banned.";
  @override
  String get setTimeoutSendMessageAlert => "You can send message again after";

  @override
  String get cancel => "Cancel";
  @override
  String get defaultMessage => "Your message was not sent. Tap ”Try Again” to re-send this message";
  @override
  String get reChargePoint => "Recharge Point";
  @override
  String get tryAgain => "Try Again";
}
