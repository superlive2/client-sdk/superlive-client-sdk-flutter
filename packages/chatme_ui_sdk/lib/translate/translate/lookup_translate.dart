/// [LookupTranslateChatMeSdk] template for any language
abstract class LookupTranslateChatMeSdk {
  String get date;
  String get somethingWentWrong;
  String get leaderBoard;
  String get noInternetConnection;
  String get somethingUnexpectedWentWrong;
  String get playing;
  String get sendGift;
  String get pts;

  String get repliedTo;
  String get reply;
  String get noRecent;
  String get followerOnlyChat;
  String get sendAMessage;
  String get replyingTo;
  String get copy;
  String get removed;
  String get userBanned;
  String get setTimeoutSendMessageAlert;

  String get cancel;
  String get tryAgain;
  String get reChargePoint;
  String get defaultMessage;
}
