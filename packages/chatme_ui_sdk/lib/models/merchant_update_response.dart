import 'dart:convert';

class MerchantUpdateResponse {
  String? merchantTitleId;
  NewValue? newValue;

  MerchantUpdateResponse({
    this.merchantTitleId,
    this.newValue,
  });

  factory MerchantUpdateResponse.fromRawJson(String str) => MerchantUpdateResponse.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory MerchantUpdateResponse.fromJson(Map<String, dynamic> json) => MerchantUpdateResponse(
    merchantTitleId: json["merchantTitleId"],
    newValue: json["newValue"] == null ? null : NewValue.fromJson(json["newValue"]),
  );

  Map<String, dynamic> toJson() => {
    "merchantTitleId": merchantTitleId,
    "newValue": newValue?.toJson(),
  };
}

class NewValue {
  String? title;
  String? titleIcon;

  NewValue({
    this.title,
    this.titleIcon,
  });

  factory NewValue.fromRawJson(String str) => NewValue.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory NewValue.fromJson(Map<String, dynamic> json) => NewValue(
    title: json["title"],
    titleIcon: json["titleIcon"],
  );

  Map<String, dynamic> toJson() => {
    "title": title,
    "titleIcon": titleIcon,
  };
}
