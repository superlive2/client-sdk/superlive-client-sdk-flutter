import 'dart:convert';

class UserUpdateResponse {
  String? level;
  String? fullName;
  String? title;
  String? titleIcon;
  String? merchantUserId;
  String? merchantTitleId;

  UserUpdateResponse({
    this.level,
    this.fullName,
    this.title,
    this.titleIcon,
    this.merchantUserId,
    this.merchantTitleId,
  });

  factory UserUpdateResponse.fromRawJson(String str) => UserUpdateResponse.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory UserUpdateResponse.fromJson(Map<String, dynamic> json) => UserUpdateResponse(
    level: json["level"],
    fullName: json["fullName"],
    title: json["title"],
    titleIcon: json["titleIcon"],
    merchantUserId: json["merchantUserId"],
    merchantTitleId: json["merchantTitleId"],
  );

  Map<String, dynamic> toJson() => {
    "level": level,
    "fullName": fullName,
    "title": title,
    "titleIcon": titleIcon,
    "merchantUserId": merchantUserId,
    "merchantTitleId": merchantTitleId,
  };
}
