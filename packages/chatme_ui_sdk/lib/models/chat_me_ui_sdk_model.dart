import 'package:chatme_ui_sdk/util/app_color.dart';
import 'package:chatme_ui_sdk/util/app_constant.dart';
import 'package:chatme_ui_sdk/util/hex_color.dart';
import 'package:flutter/material.dart';

class ChatMeUiSdkModel {
  ChatMeUiSdkModel({
    required this.data,
    // required this.pagination,
  });

  List<Datum> data;
  // Pagination pagination;

  factory ChatMeUiSdkModel.fromJson(Map<String, dynamic> json) => ChatMeUiSdkModel(
    data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    // pagination: Pagination.fromJson(json["pagination"]),
  );

  Map<String, dynamic> toJson() => {
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
    // "pagination": pagination.toJson(),
  };
}

class Datum {
  Datum({
    required this.id,
    required this.attachments,
    required this.localize,
    required this.type,
    required this.status,
    this.refType,
    required this.ref,
    this.rejectCode,
    required this.isDeleted,
    required this.createdAt,
    required this.updatedAt,
    required this.sticker,
    required this.isPinned,
    required this.message,
    this.args,
    required this.merchant,
    required this.mentions,
    required this.sender,
    required this.isSeen,
    this.sentFailBody,
    required this.isCongrats,
    required this.style,
    required this.isBanned,
  });

  String id;
  List<dynamic> attachments;
  bool localize;
  String type;
  String status;
  dynamic refType;
  Datum? ref;
  dynamic rejectCode;
  bool isDeleted;
  bool isBanned;
  DateTime createdAt;
  DateTime updatedAt;
  dynamic sticker;
  bool isPinned;
  String message;
  TextStyle style;
  dynamic args;
  String merchant;
  List<dynamic> mentions;
  Sender sender;
  bool isSeen;
  dynamic sentFailBody;
  bool isCongrats;

  factory Datum.fromJson(Map<String, dynamic> json) {
    Datum? ref;
    return Datum(
      id: json["_id"],
      attachments: List<dynamic>.from(json["attachments"].map((x) => x)),
      localize: json["localize"],
      type: json["type"],
      status: json["status"],
      refType: json["refType"],
      ref: json["ref"] == null? ref: Datum.fromJson(json["ref"]),
      rejectCode: json["rejectCode"],
      isDeleted: json["isDeleted"]?? false,
      createdAt: DateTime.parse(json["createdAt"]),
      updatedAt: DateTime.parse(json["updatedAt"]),
      sticker: json["sticker"],
      isPinned: json["isPinned"],
      message: _returnTextMessage(json["message"]),
      args: json["args"],
      merchant: json["merchant"],
      mentions: List<dynamic>.from(json["mentions"].map((x) => x)),
      sender: Sender.fromJson(json["sender"]),
      isSeen: json["isSeen"],
      isBanned: json["isBanned"]?? false,
      sentFailBody: json['sentFailBody'],
      isCongrats: _checkIsCongratsMessage(json["message"]),
      style: json['style'] != null ? TextStyle(
        color: HexColor(json['style']['color']?? AppColor.blackColor),
        fontSize: double.tryParse("${json['style']['fontSize']?? 12}"),
        fontWeight: AppConstant.getCustomFontWeight("${json['style']['fontWeight']?? 400}"),
      ): TextStyle(
        color: HexColor(AppColor.blackColor),
        fontSize: 12,
        fontWeight: AppConstant.getCustomFontWeight("400"),
      ),
    );
  }

  Map<String, dynamic> toJson() => {
    "_id": id,
    "attachments": List<dynamic>.from(attachments.map((x) => x)),
    "localize": localize,
    "type": type,
    "status": status,
    "refType": refType,
    "ref": ref,
    "rejectCode": rejectCode,
    "isDeleted": isDeleted,
    "createdAt": createdAt.toIso8601String(),
    "updatedAt": updatedAt.toIso8601String(),
    "sticker": sticker.toJson(),
    "isPinned": isPinned,
    "message": message,
    "args": args,
    "merchant": merchant,
    "mentions": List<dynamic>.from(mentions.map((x) => x)),
    "sender": sender.toJson(),
    "isSeen": isSeen,
    "isBanned": isBanned,
    "style": style,
  };
}

class Sender {
  Sender({
    required this.id,
    required this.isBanned,
    required this.name,
    this.color,
    this.title,
    this.level,
    this.titleIcon,
    this.merchantUserId,
    this.merchantTitleId
  });

  String id;
  bool isBanned;
  String name;
  String? color;
  String? title;
  String? titleIcon;
  String? level;
  String? merchantUserId;
  String? merchantTitleId;

  factory Sender.fromJson(Map<String, dynamic> json) {
    return Sender(
      id: json["_id"],
      merchantUserId: json["merchantUserId"],
      merchantTitleId: json["merchantTitleId"],
      isBanned: json["isBanned"]?? false,
      name: json["name"],
      titleIcon: json["titleIcon"],
      title: json["title"],
      level: json["level"] != null && json["level"] != ""? "Lv${json["level"]}": null,
      color: "#000000",
    );
  }

  Map<String, dynamic> toJson() => {
    "_id": id,
    "isBanned": isBanned,
    "name": name,
    "color": color,
  };
}

_checkIsCongratsMessage(String text) {
  List list = text.toString().split(AppConstant.keySeparateGiftOrNormalText);
  if(list.length == 2) {
    return true;
  } else {
    return false;
  }
}

_returnTextMessage(String text) {
  List list = text.toString().split(AppConstant.keySeparateGiftOrNormalText);
  if(list.length == 2) {
    return list[1].toString();
  } else {
    return text;
  }
}
