import 'dart:async';
import 'package:chatme_ui_sdk/util/base_api_response.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:socket_io_client/socket_io_client.dart';
import '../util/base_socket.dart';
import '../util/enents.dart';
import '../util/name_space.dart';

class MessageService {
  late Socket _socket;
  late String _roomId;

  FutureOr<void> initSocketConnection(String roomId, String token, {required Function(dynamic) onError}) async {
    _roomId = roomId;
    _socket = await BaseSocket.initConnection(token, NameSpace.messageNameSpace);
    _socket.onError((data) => onError(data.toString()));
  }

  disposedSocket() {
    _socket.clearListeners();
    _socket.dispose();
  }

  FutureOr<void> getMessage({required String page, required String limit, required Function(dynamic) callBack, required Function(dynamic) onError}) async {
    try {
      var request = {
        "query": {
          "page": page,
          "limit": limit,
        },
        "body": {
          "roomId": _roomId,
        }
      };
      _socket.emitWithAck(SocketEvents.getMessages, request, ack: (data) {
        if(data.containsKey('data')) {
          callBack(data);
        } else {
          Fluttertoast.showToast(
            msg: "${data['message']}",
            toastLength: Toast.LENGTH_SHORT,
            timeInSecForIosWeb: 2,
            backgroundColor: Colors.black,
            textColor: Colors.white,
            fontSize: 16.0,
          );
          onError(data['message'].toString());
        }
        callBack(data);
        return;
      },);
      _socket.onError((data) {
        onError(data.toString());
        return;
      });
    } catch (e) {
      debugPrint(e.toString());
      return;
    }
  }
  FutureOr<void> sendMessage(String message, {
    String referenceMessage = "",
    String referenceType = "",
    Map<String, dynamic>? messageStyle,
    required Function(dynamic) callBack,
    required Function(String, dynamic) onError,
  }) async {
    try {
      Map<String, dynamic> body = {
        "message": message,
        "type": "text",
        "room": _roomId,
      };
      if(messageStyle != null) {
        var msgStyle = {
          "style": messageStyle,
        };
        body.addAll(msgStyle);
      }
      var bodyWithRef = {
        "message": message,
        "type": "text",
        "room": _roomId,
        "refType": referenceType,
        "ref": referenceMessage,
      };

      var request = BaseApiResponse.generateRequest(referenceType.isEmpty? body: bodyWithRef);
      _socket.emitWithAck(SocketEvents.sendMessage, request, ack: (data) {
        data as Map;
        if(data.containsKey('data')) {
          callBack(data['data']['message']);
        } else {
          onError(data['message'].toString(), referenceType.isEmpty? body: bodyWithRef);
        }
      });
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  FutureOr<void> reSendMessage({
    required dynamic body,
    required Function(dynamic) callBack,
  }) async {
    try {
      var request = BaseApiResponse.generateRequest(body);
      _socket.emitWithAck(SocketEvents.sendMessage, request, ack: (data) {
        if(data.containsKey('data')) {
          callBack(data['data']['message']);
        } else {
          debugPrint(data['message'].toString());
        }
      });
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  Future<dynamic> listenReceiveMessage({required Function(dynamic, String) callBack}) async {
    try {
      _socket.on(SocketEvents.receiveMessage, (data) {
        callBack(data['data']['message'], data['data']['room']?? "");
      });
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  FutureOr<void> listenIsBanned({required Function(dynamic) callBack}) async {
    try {
      _socket.on(SocketEvents.bannedMessage, (data) {
        data as Map;
        callBack(data['data']);
      });
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  FutureOr<void> listenIsUnBanned({required Function(dynamic) callBack}) async {
    try {
      _socket.on(SocketEvents.unBannedMessage, (data) {
        data as Map;
        callBack(data['data']);
      });
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  FutureOr<void> userUpdated({required Function(dynamic) callBack}) async {
    try {
      _socket.on(SocketEvents.userUpdated, (data) {
        data as Map;
        callBack(data['data']);
      });
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  FutureOr<void> merchantUpdate({required Function(dynamic) callBack}) async {
    try {
      _socket.on(SocketEvents.merchantUpdate, (data) {
        data as Map;
        callBack(data['data']);
      });
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  FutureOr<void> deleteEachMessage({required Function(Map<String, dynamic>) callBack}) async {
    try {
      _socket.on(SocketEvents.deleteMessage, (data) {
        data as Map;
        callBack(data['data']);
      });
    } catch (e) {
      debugPrint(e.toString());
    }
  }
}
