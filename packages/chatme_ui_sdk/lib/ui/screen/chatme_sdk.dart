import 'dart:async';
import 'package:after_layout/after_layout.dart';
import 'package:chatme_ui_sdk/helper/helper.dart';
import 'package:chatme_ui_sdk/helper/widget_helper.dart';
import 'package:chatme_ui_sdk/models/chat_me_ui_sdk_model.dart';
import 'package:chatme_ui_sdk/models/user_update_response_model.dart';
import 'package:chatme_ui_sdk/services/message_service.dart';
import 'package:chatme_ui_sdk/translate/translate.dart';
import 'package:chatme_ui_sdk/ui/widget/itm_chat.dart';
import 'package:chatme_ui_sdk/util/app_color.dart';
import 'package:chatme_ui_sdk/util/app_constant.dart';
import 'package:chatme_ui_sdk/util/app_secure_storage.dart';
import 'package:chatme_ui_sdk/util/hex_color.dart';
import 'package:chatme_ui_sdk/util/image_svg_path.dart';
import 'package:chatme_ui_sdk/util/init_chat_sdk.dart';
import 'package:chatme_ui_sdk/util/internet_connection_check.dart';
import 'package:chatme_ui_sdk/util/loading_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:show_up_animation/show_up_animation.dart';
import 'package:simple_shadow/simple_shadow.dart';
import 'package:timer_count_down/timer_count_down.dart';
import '../../models/merchant_update_response.dart';
import '../widget/custom_emoji.dart';

class ChatMeUiSdk extends StatefulWidget {
  final String accessToken;
  final String roomId;
  final Function(dynamic)? onError;
  final Function(dynamic)? onSuccess;
  final Function(dynamic) onSwitchInputType;
  final Function(bool) isRequiredLogin;
  final bool isImplementGift;
  final Widget? gifts;
  final Widget? congrats;
  final Function(MessageService)? callBackSocket;
  final MessageService messageService;
  final String language;
  final bool isDisplayFullScreen;
  final String? baseUrl;
  final String? maxLength;
  final bool isBanned;
  final bool isGuestUser;
  final int setTimeoutEachMessage;

  const ChatMeUiSdk({
    Key? key,
    required this.roomId,
    required this.accessToken,
    required this.isBanned,
    required this.onSwitchInputType,
    required this.messageService,
    required this.language,
    required this.isGuestUser,
    required this.isRequiredLogin,
    this.onError,
    this.onSuccess,
    this.isImplementGift = false,
    this.gifts,
    this.callBackSocket,
    this.congrats,
    this.isDisplayFullScreen = false,
    this.baseUrl,
    this.maxLength,
    this.setTimeoutEachMessage = 3,
  }) : super(key: key);

  @override
  State<ChatMeUiSdk> createState() => _ChatMeUiSdkState();
}
String? language;
final InitChatMeSdk initChatMeSdk = InitChatMeSdk();

class _ChatMeUiSdkState extends State<ChatMeUiSdk> with AfterLayoutMixin {
  final ScrollController listScrollController = ScrollController();
  late FocusNode chatTextBoxFocusNode;
  final TextEditingController controller = TextEditingController();
  bool _isBanned = false;
  int _maxLength = 150;
  final int _defaultMaxLength = 150;

  @override
  void initState() {
    _isBanned = widget.isBanned;
    if(widget.baseUrl!= null) {
      AppConstant.baseUrl = widget.baseUrl!;
    }
    if(widget.maxLength != null) {
      try{
        _maxLength = int.parse(widget.maxLength?? "$_defaultMaxLength");
      } catch(e) {
        _maxLength = _defaultMaxLength;
      }
    }
    chatTextBoxFocusNode = FocusNode();
    if(!widget.isDisplayFullScreen) {
      language = widget.language;
      initChatMeSdk.eventBusSentGift = chatEventBus.on<SentGiftEvent>().listen((event) {
        initChatMeSdk.showingType = OpenOptionType.closeAll;
      });
    }
    super.initState();
  }

  @override
  void dispose() {
    widget.messageService.disposedSocket();
    chatTextBoxFocusNode.dispose();
    listScrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final double keyBord = MediaQuery.of(context).viewInsets.bottom;
    return initChatMeSdk.isLoading ? const Center(
      child: LoadingStyle(),
    ): GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () => AppHelperWidget.focusNew(context),
      child: Container(
        padding: EdgeInsets.only(bottom: keyBord <= MediaQuery.of(context).viewPadding.bottom ? MediaQuery.of(context).viewPadding.bottom : keyBord),
        color: widget.isDisplayFullScreen? HexColor(AppColor.primaryColor).withOpacity(0): HexColor(AppColor.backgroundColor),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _chatList(),
            Container(
              height: 0.5,
              color: HexColor(widget.isDisplayFullScreen? AppColor.blackColor: AppColor.greyColor).withOpacity(0.5),
            ),
            SizedBox(
              child: Column(
                children: [
                  const SizedBox(
                    height: 16,
                  ),
                  if (initChatMeSdk.isReply) _replyToDisplayText(),
                  if (initChatMeSdk.isReply)
                    const SizedBox(
                      height: 12,
                    ),
                  _submitForm(),
                  const SizedBox(
                    height: 16,
                  ),
                  if (initChatMeSdk.showingType == OpenOptionType.gift || initChatMeSdk.showingType == OpenOptionType.emoji)
                    CustomEmoji(
                      isDisplayFullScreen: widget.isDisplayFullScreen,
                      showingType: initChatMeSdk.showingType,
                      controller: controller,
                      gifts: widget.gifts,
                      onTapEmoji: (isTap) {
                        AppHelperWidget.focusNew(context);
                        if (controller.text.trim().isNotEmpty) {
                          initChatMeSdk.isDisabledSendButton = false;
                        } else {
                          initChatMeSdk.isDisabledSendButton = true;
                        }
                        // setState(() {});
                      },
                    ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _getMessageWithPagination({required String page, required String limit}) {
    if (initChatMeSdk.isInternetConnected) {
      widget.messageService.getMessage(
        page: page == "0"? "1": page,
        limit: limit,
        onError: (errorMessage) {
          if (initChatMeSdk.isInternetConnected) {
            initChatMeSdk.noRecordReason = chatMeTr.somethingUnexpectedWentWrong;
          } else {
            initChatMeSdk.noRecordReason = chatMeTr.noInternetConnection;
          }
          SchedulerBinding.instance.addPostFrameCallback((_) {
            setState(() {
              initChatMeSdk.isLoading = false;
              initChatMeSdk.isLoadingPagination = false;
            });
          });
        },
        callBack: (data) async {
          try {
            var dataResponse = ChatMeUiSdkModel.fromJson(data);
            for (Datum da in dataResponse.data) {
              da.sender.color = await AppSecureStorage.readUserColor(da.sender.name);
              if (da.ref != null) {
                da.ref?.sender.color = await AppSecureStorage.readUserColor((da.ref?.sender.name)!);
              }
            }
            if (initChatMeSdk.currentPage == "1") {
              initChatMeSdk.listMessage = dataResponse.data;
              initChatMeSdk.listMessage = List.from(initChatMeSdk.listMessage?.reversed as Iterable);
              initChatMeSdk.isLoading = false;
            } else {
              initChatMeSdk.listMessage?.addAll(List.from(dataResponse.data.reversed));
              initChatMeSdk.isLoadingPagination = false;
            }
            if("${dataResponse.data.length}" == initChatMeSdk.recordsPerPage) {
              initChatMeSdk.currentPage = "${int.parse(initChatMeSdk.currentPage)+1}";
              initChatMeSdk.hasNextPage = true;
            } else {
              initChatMeSdk.hasNextPage = false;
            }
            SchedulerBinding.instance.addPostFrameCallback((_) {
              setState(() { });
            });
          } catch(e) {
            debugPrint("Error:::::::::::::::${e.toString()}");
          }
        },
      );
    } else {
      initChatMeSdk.isLoadingPagination = false;
      SchedulerBinding.instance.addPostFrameCallback((_) {
        setState(() {
          initChatMeSdk.isLoading = false;
          initChatMeSdk.isLoadingPagination = false;
        });
      });
    }
  }

  void _sendMessage() {
    /// If guest user
    if(widget.isGuestUser) {
      widget.isRequiredLogin(true);
      return;
    }
    /// SetTimeout send message
    if(!initChatMeSdk.isAllowSendMessage) {
      return;
    }
    chatTextBoxFocusNode.unfocus();
    /// Send message
    if(_isBanned == true) {
      Fluttertoast.showToast(
        msg: chatMeTr.userBanned,
        toastLength: Toast.LENGTH_SHORT,
        timeInSecForIosWeb: 2,
        backgroundColor: Colors.black,
        textColor: Colors.white,
        fontSize: 16.0,
      );
      return;
    }
    if (initChatMeSdk.isInternetConnected) {
      String message = controller.text.trim().toString();
      Datum? datum;
      controller.clear();
      initChatMeSdk.isAllowSendMessage = false;
      initChatMeSdk.isDisabledSendButton = true;
      if (message.isNotEmpty) {
        widget.messageService.sendMessage(
          message,
          referenceMessage: initChatMeSdk.isReply ? "${initChatMeSdk.replyTo?.id}" : "",
          referenceType: initChatMeSdk.isReply ? ReferenceType.reply : "",
          callBack: (itemChat) async {
            initChatMeSdk.isReply = false;
            initChatMeSdk.replyTo = datum;
            initChatMeSdk.tapOnIndex = -1;
            SchedulerBinding.instance.addPostFrameCallback((_) {
              setState(() { });
            });
          },
          onError: (errorMessage, messageBody) async {
            messageBody as Map;
            Map<String, dynamic> sentFailItemChat = {
              "sentFailBody": messageBody,
            };
            sentFailItemChat.addAll(_itemChatMockupData);
            Datum sentFailMessageAsModel = Datum.fromJson(sentFailItemChat);
            if (initChatMeSdk.isReply) {
              sentFailMessageAsModel.ref = initChatMeSdk.replyTo;
            }
            sentFailMessageAsModel.sender.color = AppColor.errorColor;
            sentFailMessageAsModel.message = messageBody['message'].toString();
            initChatMeSdk.listMessage!.insert(0, sentFailMessageAsModel);
            initChatMeSdk.isReply = false;
            initChatMeSdk.replyTo = datum;
            initChatMeSdk.tapOnIndex = -1;
            SchedulerBinding.instance.addPostFrameCallback((_) {
              setState(() { });
            });
            if (listScrollController.hasClients) {
              final position = listScrollController.position.minScrollExtent;
              listScrollController.jumpTo(position);
            }
          },
        );
      }
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(chatMeTr.noInternetConnection),
        ),
      );
    }
  }

  Widget _submitForm() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Stack(
        children: [
          Row(
            children: [
              Expanded(
                child: Container(
                  decoration: BoxDecoration(
                    color: HexColor(AppColor.greyColor).withOpacity(0.05),
                    borderRadius: BorderRadius.circular(10),
                    border: initChatMeSdk.isOverLength ? Border.all(
                      width: 0.5,
                      color: HexColor(AppColor.errorColor),
                    ) : chatTextBoxFocusNode.hasFocus == true && initChatMeSdk.showingType == OpenOptionType.keyboard ? Border.all(
                      width: 0.5,
                      color: HexColor(
                        AppColor.primaryColor,
                      ).withOpacity(0.5),
                    ) : null,
                  ),
                  padding: const EdgeInsets.symmetric(
                    horizontal: 8,
                  ),
                  child: Stack(
                    children: [
                      Row(
                        children: [
                          Expanded(
                            child: TextFormField(
                              inputFormatters: <TextInputFormatter>[
                                LengthLimitingTextInputFormatter(_maxLength),
                                FilteringTextInputFormatter.deny(RegExp(r"\n")),
                              ],
                              focusNode: chatTextBoxFocusNode,
                              onChanged: (e) {
                                if (e.length > _maxLength) {
                                  initChatMeSdk.isOverLength = true;
                                } else {
                                  initChatMeSdk.isOverLength = false;
                                }
                                if (e.trim().isNotEmpty) {
                                  initChatMeSdk.isDisabledSendButton = false;
                                } else {
                                  initChatMeSdk.isDisabledSendButton = true;
                                }
                                SchedulerBinding.instance.addPostFrameCallback((_) {
                                  setState(() { });
                                });
                              },
                              onTap: () {
                                initChatMeSdk.showingType = OpenOptionType.keyboard;
                                SchedulerBinding.instance.addPostFrameCallback((_) {
                                  setState(() { });
                                });
                                widget.onSwitchInputType(true);
                              },
                              controller: controller,
                              maxLines: 3,
                              minLines: 1,
                              style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: 12,
                                height: 1.2,
                                color: HexColor(AppColor.blackColor),
                              ),
                              cursorColor: HexColor(AppColor.primaryColor),
                              decoration: InputDecoration(
                                hintText: chatMeTr.sendAMessage,
                                border: InputBorder.none,
                                hintStyle: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  fontSize: 12,
                                  height: 1.5,
                                  color: HexColor(AppColor.disabledColor),
                                ),
                                focusColor: HexColor(AppColor.primaryColor),
                                isCollapsed: true,
                                contentPadding: const EdgeInsets.symmetric(vertical: 14),
                              ),
                            ),
                          ),
                          if (widget.isImplementGift && controller.text.isEmpty)
                            const SizedBox(
                              width: 37,
                              height: 22,
                            ),
                          const SizedBox(
                            width: 37,
                            height: 22,
                          ),
                        ],
                      ),
                      if(controller.text.trim().isNotEmpty == true && chatTextBoxFocusNode.hasFocus == true)
                        Positioned(
                          bottom: 28,
                          right: 0,
                          child: Builder(
                            builder: (context) {
                              return Text("${controller.text.length}/$_maxLength", style: TextStyle(fontSize: 10, fontWeight: FontWeight.w500, color: HexColor(AppColor.disabledColor)));
                            },
                          ),
                        ),
                      AnimatedPositioned(
                        bottom: (controller.text.trim().isNotEmpty == true && chatTextBoxFocusNode.hasFocus == true)? 6 : 12,
                        right: 0,
                        duration: const Duration(milliseconds: 300),
                        curve: Curves.linear,
                        child: Row(
                          children: [
                            if(widget.isImplementGift)
                              Center(
                                child: GestureDetector(
                                  onTap: () {
                                    AppHelperWidget.focusNew(context);
                                    if (initChatMeSdk.showingType == OpenOptionType.gift) {
                                      initChatMeSdk.showingType = OpenOptionType.closeAll;
                                    } else {
                                      initChatMeSdk.showingType = OpenOptionType.gift;
                                    }
                                    widget.onSwitchInputType(true);
                                    setState(() { });
                                  },
                                  child: Padding(
                                    padding: const EdgeInsets.only(right: 5),
                                    child: Icon(
                                      Icons.diamond_outlined,
                                      size: 20,
                                      color: initChatMeSdk.showingType == OpenOptionType.gift ? HexColor(AppColor.primaryColor).withOpacity(.7) : HexColor(AppColor.disabledColor).withOpacity(.7),
                                    ),
                                  ),
                                ),
                              ),

                            Center(
                              child: InkWell(
                                onTap: () {
                                  AppHelperWidget.focusNew(context);
                                  if (initChatMeSdk.showingType == OpenOptionType.emoji) {
                                    initChatMeSdk.showingType = OpenOptionType.closeAll;
                                  } else {
                                    initChatMeSdk.showingType = OpenOptionType.emoji;
                                  }
                                  setState(() { });
                                  widget.onSwitchInputType(true);
                                },
                                child: SvgPicture.asset(
                                  AppSvgPath.smileIconSvg,
                                  package: AppConstant.appPackageName,
                                  color: initChatMeSdk.showingType == OpenOptionType.emoji ? HexColor(AppColor.primaryColor) : HexColor(AppColor.disabledColor),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              const SizedBox(
                width: 55,
                height: 47,
              ),
            ],
          ),
          Positioned(
            bottom: 0,
            right: 0,
            child: InkWell(
              onTap: () => _sendMessage(),
              child: Container(
                width: 55,
                height: 47,
                decoration: BoxDecoration(
                  color: HexColor(initChatMeSdk.isDisabledSendButton || initChatMeSdk.isAllowSendMessage == false? AppColor.disabledColor : AppColor.primaryColor),
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Center(
                  child: initChatMeSdk.isAllowSendMessage == false? Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Countdown(
                        seconds: widget.setTimeoutEachMessage,
                        build: (BuildContext context, double time) => Text("${time.toInt()}s", style: TextStyle(color: HexColor(AppColor.whiteColor), fontSize: 12, fontWeight: FontWeight.w500),),
                        interval: const Duration(seconds: 1),
                        onFinished: () {
                          setState(() {
                            initChatMeSdk.isAllowSendMessage = true;
                          });
                        },
                      ),
                      const SizedBox(width: 2,),
                      Icon(Icons.timer, size: 15, color: HexColor(AppColor.whiteColor),),
                    ],
                  ): AnimatedSize(
                    duration: const Duration(milliseconds: 300),
                    curve: Curves.bounceOut,
                    child: SvgPicture.asset(
                      AppSvgPath.sendIconSvg,
                      package: AppConstant.appPackageName,
                    ),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _replyToDisplayText() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        children: [
          Row(
            children: [
              SvgPicture.asset(
                AppSvgPath.shareIconSvg,
                package: AppConstant.appPackageName,
              ),
              const SizedBox(width: 7),
              Expanded(
                child: Text(
                  "${chatMeTr.replyingTo} ${initChatMeSdk.replyTo?.sender.name}:",
                  style: TextStyle(color: HexColor(AppColor.greyColor)),
                ),
              ),
              InkWell(
                onTap: () {
                  initChatMeSdk.isReply = false;
                  setState(() { });
                },
                child: SvgPicture.asset(
                  AppSvgPath.closeIconSvg,
                  package: AppConstant.appPackageName,
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 6,
          ),
          Row(
            children: [
              Expanded(
                child: RichText(
                  maxLines: 20,
                  overflow: TextOverflow.fade,
                  text: TextSpan(
                    text: "${initChatMeSdk.replyTo?.sender.name}: ",
                    style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.w400,
                      color: HexColor(AppColor.primaryColor),
                    ),
                    children: [
                      TextSpan(
                        text: "${initChatMeSdk.replyTo?.isBanned == true? chatMeTr.removed :initChatMeSdk.replyTo?.message}",
                        style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w400,
                          color: HexColor(initChatMeSdk.replyTo?.isBanned == true? AppColor.disabledColor: AppColor.blackColor),
                          fontStyle: initChatMeSdk.replyTo?.isBanned == true? FontStyle.italic: null,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              const SizedBox(
                width: 30,
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _chatList() {
    return Expanded(
      child: (initChatMeSdk.listMessage!.isEmpty)? Center(
        child: Text(
          initChatMeSdk.noRecordReason,
          style: TextStyle(
            fontSize: 12,
            color: HexColor(widget.isDisplayFullScreen? AppColor.blackColor: AppColor.greyColor),
          ),
        ),
      ) : Stack(
        children: [
          Positioned.fill(
            child: ListView.builder(
              shrinkWrap: true,
              reverse: true,
              keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
              padding: EdgeInsets.zero,
              physics: const BouncingScrollPhysics(),
              controller: listScrollController,
              itemCount: (initChatMeSdk.listMessage?.length)! + 1,
              itemBuilder: (context, ind) {
                return Stack(
                  clipBehavior: Clip.none,
                  children: [
                    SizedBox(
                      child: initChatMeSdk.listMessage?.length == ind ? const SizedBox(
                        height: 30,
                      ): ind == 0 ? ShowUpAnimation(
                        delayStart: const Duration(milliseconds: 100),
                        animationDuration: const Duration(milliseconds: 300),
                        curve: Curves.fastOutSlowIn,
                        direction: Direction.vertical,
                        offset: 0.5,
                        child: ItemChat(
                          isDisplayFullScreen: widget.isDisplayFullScreen,
                          messageService: widget.messageService,
                          itemData: initChatMeSdk.listMessage?[ind],
                          removeFromIndex: (index) {
                            AppHelperWidget.focusNew(context);
                            initChatMeSdk.listMessage?.removeAt(index);
                            setState(() { });
                          },
                          tapIndex: initChatMeSdk.tapOnIndex,
                          currentChatIndex: ind,
                          onTap: (tapIndex, isCopy) {
                            AppHelperWidget.focusNew(context);
                            if(_isBanned == true || initChatMeSdk.listMessage?[ind].isBanned == true) {
                              initChatMeSdk.isShowCopy = false;
                            } else {
                              initChatMeSdk.isShowCopy = isCopy;
                            }
                            initChatMeSdk.tapOnIndex = tapIndex;
                            setState(() { });
                          },
                          tapToReply: (isReply, replyTo) {
                            AppHelperWidget.focusNew(context);
                            initChatMeSdk.isReply = isReply;
                            initChatMeSdk.replyTo = replyTo;
                            setState(() { });
                          },
                        ),
                      ) : ItemChat(
                        isDisplayFullScreen: widget.isDisplayFullScreen,
                        messageService: widget.messageService,
                        itemData: initChatMeSdk.listMessage?[ind],
                        removeFromIndex: (index) {
                          AppHelperWidget.focusNew(context);
                          initChatMeSdk.listMessage?.removeAt(index);
                          setState(() { });
                        },
                        tapIndex: initChatMeSdk.tapOnIndex,
                        currentChatIndex: ind,
                        onTap: (tapIndex, isCopy) {
                          AppHelperWidget.focusNew(context);
                          if(_isBanned == true || initChatMeSdk.listMessage?[ind].isBanned == true) {
                            initChatMeSdk.isShowCopy = false;
                          } else {
                            initChatMeSdk.isShowCopy = isCopy;
                          }
                          initChatMeSdk.tapOnIndex = tapIndex;
                          setState(() { });
                        },
                        tapToReply: (isReply, replyTo) {
                          AppHelperWidget.focusNew(context);
                          initChatMeSdk.isReply = isReply;
                          initChatMeSdk.replyTo = replyTo;
                          setState(() { });
                        },
                      ),
                    ),
                    if (initChatMeSdk.tapOnIndex == ind - 1 && initChatMeSdk.isShowCopy)
                      Positioned(
                        bottom: 0,
                        left: 0,
                        right: 0,
                        child: Center(
                          child: InkWell(
                            onTap: () async {
                              await Clipboard.setData(ClipboardData(text: "${initChatMeSdk.listMessage?[initChatMeSdk.tapOnIndex].message}"));
                              initChatMeSdk.isShowCopy = false;
                              setState(() { });
                            },
                            child: SimpleShadow(
                              opacity: 0.6,
                              color: HexColor(widget.isDisplayFullScreen? AppColor.blackColor: AppColor.greyColor),
                              offset: const Offset(2, 2),
                              sigma: 5,
                              child: Stack(
                                children: [
                                  SvgPicture.asset(
                                    AppSvgPath.copyIconSvg,
                                    package: AppConstant.appPackageName,
                                  ),
                                  Positioned(
                                    top: 6,
                                    right: 6,
                                    bottom: 18,
                                    left: 26,
                                    child: Container(
                                      alignment: Alignment.center,
                                      color: HexColor(AppColor.whiteColor),
                                      child: Text(
                                        chatMeTr.copy,
                                        style: TextStyle(
                                          color: HexColor(AppColor.blackColor),
                                          fontWeight: FontWeight.w400,
                                          fontSize: 13,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                  ],
                );
              },
            ),
          ),
          if (initChatMeSdk.isLoadingPagination)
            const Positioned(
              top: 10,
              left: 0,
              right: 0,
              child: SizedBox(
                height: 35,
                child: Center(
                  child: LoadingStyle(),
                ),
              ),
            ),
          if (initChatMeSdk.isShowCongrats) widget.congrats!,
        ],
      ),
    );
  }

  @override
  FutureOr<void> afterFirstLayout(BuildContext context) async {
    try {
      AppInternetConnectionCheck.checkIsInternetConnected(
        listener: (isConnected) {
          initChatMeSdk.isInternetConnected = isConnected;
          setState(() { });
        },
      );
      await widget.messageService.initSocketConnection(
        widget.roomId,
        widget.accessToken,
        onError: (data) {
          widget.onError!(data);
        },
      );
      widget.callBackSocket!(widget.messageService);
      widget.onSuccess!("Connected succeed");
      _getMessageWithPagination(page: initChatMeSdk.firstPage, limit: initChatMeSdk.recordsPerPage);
      widget.messageService.listenReceiveMessage(
        callBack: (itemChat, roomId) async {
          if(!widget.isDisplayFullScreen) {
            Datum newRecord = Datum.fromJson(itemChat);
            if(widget.roomId == roomId) {
              newRecord.sender.color = await AppSecureStorage.readUserColor(newRecord.sender.name);
              if (newRecord.ref != null) {
                newRecord.ref?.sender.color = await AppSecureStorage.readUserColor((newRecord.ref?.sender.name)!);
              }
              initChatMeSdk.listMessage!.insert(0, newRecord);
              if (listScrollController.hasClients) {
                final position = listScrollController.position.minScrollExtent;
                listScrollController.jumpTo(position);
              }
            }
          }
          setState(() { });
        },
      );
      //
      widget.messageService.listenIsBanned(callBack: (data) {
        data as Map;
        if(data['deleteOldMessage']) {
          for(Datum itm in initChatMeSdk.listMessage!) {
            if(itm.sender.merchantUserId == data['merchantUserId']) {
              itm.isBanned = true;
            }
          }
        }
        _isBanned = true;
        setState(() { });
      },);

      widget.messageService.listenIsUnBanned(callBack: (data) {
        data as Map;
        if(data['restoreOldMessage']) {
          for(Datum itm in initChatMeSdk.listMessage!) {
            if(itm.sender.merchantUserId == data['merchantUserId']) {
              itm.isBanned = false;
            }
          }
        }
        _isBanned = false;
        setState(() { });
      });

      widget.messageService.userUpdated(callBack: (data) {
        UserUpdateResponse _updateOn = UserUpdateResponse.fromJson(data);
        int _totalUpdate = ((initChatMeSdk.listMessage??[]).length > int.parse(initChatMeSdk.recordsPerPage)? int.parse(initChatMeSdk.recordsPerPage): (initChatMeSdk.listMessage??[]).length);
        for(int i = 0; i < _totalUpdate; i++) {
          if(initChatMeSdk.listMessage?[i].sender.merchantUserId == _updateOn.merchantUserId) {
            if(_updateOn.title != null) initChatMeSdk.listMessage?[i].sender.title = _updateOn.title ;
            if(_updateOn.titleIcon != null) initChatMeSdk.listMessage?[i].sender.titleIcon = _updateOn.titleIcon == ""? null: _updateOn.titleIcon;
            if(_updateOn.level != null) initChatMeSdk.listMessage?[i].sender.level = _updateOn.level;
            if(_updateOn.fullName != null) initChatMeSdk.listMessage?[i].sender.name = "${_updateOn.fullName}";
            if(_updateOn.merchantTitleId != null) initChatMeSdk.listMessage?[i].sender.merchantTitleId = "${_updateOn.merchantTitleId}";
          }
        }
        setState(() { });
      });

      widget.messageService.merchantUpdate(callBack: (data) {
        MerchantUpdateResponse _updatedOn = MerchantUpdateResponse.fromJson(data);
        int _totalUpdate = ((initChatMeSdk.listMessage??[]).length > int.parse(initChatMeSdk.recordsPerPage)? int.parse(initChatMeSdk.recordsPerPage): (initChatMeSdk.listMessage??[]).length);
        for(int i = 0; i < _totalUpdate; i++) {
          if(initChatMeSdk.listMessage?[i].sender.merchantTitleId == _updatedOn.merchantTitleId) {
            initChatMeSdk.listMessage?[i].sender.title = _updatedOn.newValue?.title;
            initChatMeSdk.listMessage?[i].sender.titleIcon = _updatedOn.newValue?.titleIcon;
          }
        }
        setState(() { });
      });

      widget.messageService.deleteEachMessage(callBack: (data) {
        int _totalUpdate = ((initChatMeSdk.listMessage??[]).length > int.parse(initChatMeSdk.recordsPerPage)? int.parse(initChatMeSdk.recordsPerPage): (initChatMeSdk.listMessage??[]).length);
        List _list = data['messageIds'];
        for(int i = 0; i < _totalUpdate; i++) {
          if(_list.contains(initChatMeSdk.listMessage?[i].id)) {
            initChatMeSdk.listMessage?[i].isBanned = true;
          }
        }
        setState(() {  });
      });

      // Add listener to get pagination.
      listScrollController.addListener(() {
        if (listScrollController.position.maxScrollExtent == listScrollController.position.pixels) {
          if (!initChatMeSdk.isLoading && !initChatMeSdk.isLoadingPagination && initChatMeSdk.hasNextPage == true) {
            initChatMeSdk.isLoadingPagination = true;
            SchedulerBinding.instance.addPostFrameCallback((_) {
              setState(() { });
            });
            _getMessageWithPagination(
              page: initChatMeSdk.currentPage,
              limit: initChatMeSdk.recordsPerPage,
            );
          }
        }
      });
    } catch (e) {
      widget.onError!(e.toString());
    }
  }

  final Map<String, dynamic> _itemChatMockupData = {
    "_id": "",
    "attachments": [],
    "localize": false,
    "type": "text",
    "status": "sent",
    "refType": "reply",
    "ref": null,
    "rejectCode": null,
    "createdAt": "2023-04-26T04:50:47.074Z",
    "updatedAt": "2023-04-26T04:50:47.074Z",
    "sticker": {},
    "isPinned": false,
    "message": "see ya ☺️",
    "args": null,
    "merchant": "64464c11f1e5badf4cac1823",
    "mentions": [],
    "sender": {"_id": "", "isBanned": false, "name": "Send Fail"},
    "isSeen": false
  };
}

class ReferenceType {
  static const String reply = "reply";
}

class OpenOptionType {
  static const String closeAll = "-1";
  static const String keyboard = "0";
  static const String emoji = "1";
  static const String gift = "2";
}
