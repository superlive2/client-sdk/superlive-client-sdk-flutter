import 'package:flutter/material.dart';
import '../../util/app_color.dart';
import '../../util/hex_color.dart';

class FilterUserList extends StatelessWidget {
  const FilterUserList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50 * 3.5,
      decoration: BoxDecoration(
        color: HexColor(AppColor.backgroundColor),
        borderRadius: const BorderRadius.only(
          topLeft: Radius.circular(10),
          topRight: Radius.circular(10),
        ),
        boxShadow: [
          BoxShadow(
            color: HexColor(AppColor.primaryColor).withOpacity(0.3),
            spreadRadius: 2,
            blurRadius: 7,
            offset: const Offset(10, -3),
          ),
        ],
      ),
      child: ListView.builder(
        itemCount: 3,
        shrinkWrap: true,
        itemBuilder: (context, index) {
          return Container(
            height: 50,
            margin: const EdgeInsets.symmetric(horizontal: 16),
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(
                  width: 0.5,
                  color: HexColor(AppColor.disabledColor),
                ),
              ),
            ),
            alignment: Alignment.centerLeft,
            child: const Text("FRANK Monster FT"),
          );
        },
      ),
    );
  }
}
