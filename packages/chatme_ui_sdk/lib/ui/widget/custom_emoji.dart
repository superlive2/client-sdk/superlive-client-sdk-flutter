import 'package:chatme_ui_sdk/translate/translate.dart';
import 'package:chatme_ui_sdk/util/app_color.dart';
import 'package:chatme_ui_sdk/util/hex_color.dart';
import 'package:emoji_picker_flutter/emoji_picker_flutter.dart';
import 'package:flutter/material.dart';
import '../screen/chatme_sdk.dart';

class CustomEmoji extends StatelessWidget {
  final String showingType;
  final Widget? gifts;
  final Function(bool) onTapEmoji;
  final bool isDisplayFullScreen;
  final TextEditingController controller;
  const CustomEmoji({Key? key, required this.controller, required this.showingType, this.gifts, required this.onTapEmoji, required this.isDisplayFullScreen}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedSize(
      duration: const Duration(milliseconds: 300),
      child: SizedBox(
        height: 200,
        child: showingType == OpenOptionType.gift? AnimatedSize(
          duration: const Duration(milliseconds: 300),
          child: gifts,
        ): AnimatedSize(
          duration: const Duration(milliseconds: 300),
          child: EmojiPicker(
            textEditingController: controller,
            onEmojiSelected: (cat, e) {
              onTapEmoji(true);
            },
            config: Config(
              columns: 8,
              emojiSizeMax: 32 * 1.0,
              verticalSpacing: 0,
              horizontalSpacing: 0,
              gridPadding: EdgeInsets.zero,
              initCategory: Category.RECENT,
              bgColor: HexColor(AppColor.whiteColor).withOpacity(isDisplayFullScreen? 0: 1),
              skinToneIndicatorColor: HexColor(AppColor.blackColor),
              enableSkinTones: true,
              recentsLimit: 28,
              replaceEmojiOnLimitExceed: false,
              noRecents: Text(
                chatMeTr.noRecent,
                style: const TextStyle(fontSize: 20, color: Colors.black87),
                textAlign: TextAlign.center,
              ),
              checkPlatformCompatibility: true,
            ),
          ),
        ),
      ),
    );
  }
}
