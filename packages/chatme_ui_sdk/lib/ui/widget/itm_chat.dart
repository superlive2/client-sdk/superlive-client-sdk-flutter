import 'package:chatme_ui_sdk/models/chat_me_ui_sdk_model.dart';
import 'package:chatme_ui_sdk/services/message_service.dart';
import 'package:chatme_ui_sdk/translate/translate.dart';
import 'package:chatme_ui_sdk/util/custom_bottom_sheet.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../util/app_color.dart';
import '../../util/app_constant.dart';
import '../../util/hex_color.dart';
import '../../util/image_svg_path.dart';
import 'package:linkify/linkify.dart';

class ItemChat extends StatelessWidget {
  final int currentChatIndex;
  final int tapIndex;
  final Function(int, bool) onTap;
  final Function(bool, Datum) tapToReply;
  final Function(int) removeFromIndex;
  final Datum? itemData;
  final MessageService messageService;
  final bool isDisplayFullScreen;

  const ItemChat({
    Key? key,
    required this.onTap,
    required this.currentChatIndex,
    required this.tapIndex,
    required this.tapToReply,
    required this.itemData,
    required this.removeFromIndex,
    required this.messageService,
    required this.isDisplayFullScreen,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final List<LinkifyElement> entities = linkify("${itemData?.message}", options: const LinkifyOptions(humanize: false));
    if(itemData?.isBanned == true) return const SizedBox();
    return GestureDetector(
      onLongPress: () {
        if (itemData?.sentFailBody == null) {
          HapticFeedback.heavyImpact();
          onTap(currentChatIndex, true);
        }
      },
      onTap: () {
        if (itemData?.sentFailBody == null) {
          if (currentChatIndex == tapIndex) {
            onTap(-1, false);
            tapToReply(false, itemData!);
          } else {
            onTap(currentChatIndex, false);
          }
        } else {
          AppCustomBottomSheet.showAppBottomSheet(context, onTap: () {
            messageService.reSendMessage(
              body: itemData?.sentFailBody, // {"message": "hello 1233", "type": "text", "room": "6454a36c248e7145617d29fa"}, //
              callBack: (itemChat) {
                removeFromIndex(currentChatIndex);
              },
            );
          },);
        }
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            color: currentChatIndex == tapIndex ? HexColor(isDisplayFullScreen? AppColor.blackColor: AppColor.greyColor).withOpacity(0.1) : null,
            padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 16),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                if (itemData?.ref != null)
                  Row(
                    children: [
                      SvgPicture.asset(
                        AppSvgPath.shareIconSvg,
                        package: AppConstant.appPackageName,
                      ),
                      const SizedBox(
                        width: 7,
                      ),
                      RichText(
                        text: TextSpan(
                          text: "${itemData?.sender.name}",
                          style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.bold,
                            color: HexColor("${itemData?.sender.color!}"),
                          ),
                          children: [
                            ..._listSenderName(
                              context,
                              title: "${itemData?.sender.title}",
                              color: "${itemData?.sender.color}",
                              level: itemData?.sender.level,
                              titleIcon: itemData?.sender.titleIcon,
                              opacity: 1,
                            ),
                            TextSpan(
                              text: " ${chatMeTr.repliedTo} ",
                              style: TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.w400,
                                color: HexColor(isDisplayFullScreen? AppColor.blackColor: AppColor.greyColor),
                              ),
                              children: [
                                TextSpan(
                                  text: "${itemData?.ref?.sender.name}",
                                  style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.bold,
                                    color: HexColor("${itemData?.ref?.sender.color!}"),
                                  ),
                                ),
                                ..._listSenderName(
                                  context,
                                  title: "${itemData?.ref?.sender.title}",
                                  color: "${itemData?.ref?.sender.color}",
                                  level: itemData?.ref?.sender.level,
                                  titleIcon: itemData?.ref?.sender.titleIcon,
                                  opacity: 1,
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                if (itemData?.ref != null)
                  const SizedBox(
                    height: 7,
                  ),
                if (itemData?.ref != null)
                  Row(
                    children: [
                      Expanded(
                        child: _itemMessage(
                          context,
                          message: "${itemData?.ref?.message}",
                          senderName: "${itemData?.ref?.sender.name}",
                          color: "${itemData?.ref?.sender.color!}",
                          opacity: .5,
                          title: itemData?.ref?.sender.title,
                          titleIcon: itemData?.ref?.sender.titleIcon,
                          level: itemData?.ref?.sender.level,
                          style: itemData!.ref!.style,
                          isRemoved: itemData!.ref!.isBanned,
                          entities: entities,
                        ),
                      ),
                      const SizedBox(
                        width: 30,
                      ),
                    ],
                  ),
                Row(
                  children: [
                    Expanded(
                      child: _itemMessage(
                        context,
                        senderName: "${itemData?.sender.name}",
                        color: "${itemData?.sender.color!}",
                        title: itemData?.sender.title,
                        titleIcon: itemData?.sender.titleIcon,
                        level: itemData?.sender.level,
                        style: itemData!.style,
                        isRemoved: itemData?.isBanned?? false,
                        entities: entities,
                      ),
                    ),
                    const SizedBox(
                      width: 18,
                    ),
                    if (itemData?.sentFailBody == null)
                      Text(DateFormat("hh:mm a").format((itemData?.createdAt.toLocal())!).toLowerCase(),
                        style: TextStyle(
                          fontSize: 10,
                          fontWeight: FontWeight.w500,
                          color: HexColor(isDisplayFullScreen? AppColor.blackColor: AppColor.disabledColor),
                        ),
                      ),
                    if (itemData?.sentFailBody != null)
                      Icon(
                        Icons.info,
                        color: HexColor(AppColor.errorColor),
                        size: 15,
                      ),
                  ],
                ),
              ],
            ),
          ),
          Visibility(
            visible: currentChatIndex == tapIndex,
            maintainAnimation: true,
            maintainState: true,
            child: AnimatedOpacity(
              duration: const Duration(milliseconds: 300),
              curve: Curves.fastOutSlowIn,
              opacity: currentChatIndex == tapIndex ? 1 : 0,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(
                    height: 7,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    child: InkWell(
                      onTap: () {
                        tapToReply(true, itemData!);
                        onTap(currentChatIndex, false);
                      },
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          SvgPicture.asset(
                            AppSvgPath.shareIconSvg,
                            package: AppConstant.appPackageName,
                          ),
                          const SizedBox(
                            width: 7,
                          ),
                          Text(chatMeTr.reply,
                            style: TextStyle(
                              color: HexColor(AppColor.primaryColor),
                              fontSize: 12,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _itemMessage(BuildContext context, {required List<LinkifyElement> entities, String? message, required String senderName, String? title, String? level, String? titleIcon, required String color, double opacity = 1, required TextStyle style, bool isRemoved = false}) {
    /// is removed
    if(isRemoved) {
      return const SizedBox();
    }
    /// Normal message with condition show removed message, but is not work when the condition above is true.
    return RichText(
      maxLines: 20,
      overflow: TextOverflow.fade,
      text: TextSpan(
        text: "$senderName ",
        style: TextStyle(
          fontSize: 12,
          fontWeight: FontWeight.bold,
          color: HexColor(color).withOpacity(opacity),
        ),
        children: [
          ..._listSenderName(
            context,
            color: color,
            level: level,
            titleIcon: titleIcon,
            title: "$title",
            opacity: opacity,
          ),
          TextSpan(
            text: ": ",
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.bold,
              color: HexColor(color).withOpacity(opacity),
            ),
          ),
          if(isRemoved == false)
            for(int i=0; i < entities.length; i++)
              TextSpan(
                text: message?? entities[i].text,
                style: TextStyle(
                  fontSize: style.fontSize,
                  fontWeight: style.fontWeight,
                  color: AppConstant.regExpCheckLink.hasMatch(entities[i].text) ? HexColor(AppColor.infoColor): style.color!.withOpacity(opacity),
                ),
                recognizer: AppConstant.regExpCheckLink.hasMatch(entities[i].text) != true? null: TapGestureRecognizer()?..onTap = () async {
                  if(AppConstant.regExpCheckLink.hasMatch(entities[i].text)) {
                    await launchUrl(
                      Uri.parse(entities[i].text),
                      mode: LaunchMode.externalApplication,
                    );
                  }
                },
              ),
          if(isRemoved == true)
            TextSpan(
              text: chatMeTr.removed,
              style: TextStyle(
                fontStyle: FontStyle.italic,
                fontSize: 12,
                fontWeight: FontWeight.normal,
                color: HexColor(AppColor.disabledColor),
              ),
            ),
        ],
      ),
    );
  }

  List<InlineSpan> _listSenderName(BuildContext context, {required String title, String? titleIcon, String? level, required String color, required double opacity}) {
    return <InlineSpan>[
      if(titleIcon != null && titleIcon != "")
        WidgetSpan(
          child: GestureDetector(
            onTap: () {
              Fluttertoast.showToast(
                msg: title,
                toastLength: Toast.LENGTH_SHORT,
                timeInSecForIosWeb: 1,
                backgroundColor: Colors.black87,
                textColor: Colors.white,
                fontSize: 12.0,
              );
            },
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Image.network(
                titleIcon,
                height: 14,
                errorBuilder: (_, __, ___) {
                  return const Icon(Icons.info_outline, color: Colors.grey, size: 15,);
                },
              ),
            ),
          ),
        ),
      if(level != null)
        WidgetSpan(
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 2, vertical: 1),
            decoration: BoxDecoration(
              color: HexColor(AppColor.primaryColor).withOpacity(opacity),
              borderRadius: BorderRadius.circular(4),
            ),
            child: Text(level, style: TextStyle(
              color: HexColor(AppColor.whiteColor),
              fontSize: 8,
              fontWeight: FontWeight.w400,
            ),),
          ),
        ),
      if(level != null)
        const WidgetSpan(
          child: SizedBox(width: 2,),
        ),
    ];
  }
}
