import 'package:flutter/material.dart';

class AppHelperWidget {
  static ScrollPhysics get scrollPhysics => const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics());

  static void focusNew(BuildContext context) {
    FocusScope.of(context).requestFocus(FocusNode());
  }
}
