class CustomFontWeight {
  static const String w100 = "100";
  static const String w200 = "200";
  static const String w300 = "300";
  static const String w400 = "400";
  static const String w500 = "500";
  static const String w600 = "600";
  static const String w700 = "700";
  static const String w800 = "800";
  static const String w900 = "900";
}
