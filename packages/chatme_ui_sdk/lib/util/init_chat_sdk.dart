import 'dart:async';
import 'package:flutter/material.dart';
import '../helper/helper.dart';
import '../models/chat_me_ui_sdk_model.dart';
import '../translate/translate.dart';
import '../ui/screen/chatme_sdk.dart';

class InitChatMeSdk {
  List<Datum>? listMessage = [];
  bool isLoading = true;
  String showingType = OpenOptionType.closeAll;
  int tapOnIndex = -1;
  bool isReply = false;
  bool isOverLength = false;
  bool isLoadingPagination = false;
  Datum? replyTo;
  final String firstPage = "1";
  final String recordsPerPage = "20";
  var isDeviceConnected = false;
  bool isInternetConnected = true;
  String noRecordReason = chatMeTr.noRecent;
  bool isShowCopy = false;
  bool isShowCongrats = false;
  bool isDisabledSendButton = true;
  late StreamSubscription<SentGiftEvent> eventBusSentGift;
  String currentPage = "0";
  bool hasNextPage = false;
  bool isAllowSendMessage = true;

  static clearInitializeData() {
    initChatMeSdk.listMessage = [];
    initChatMeSdk.isLoading = true;
    initChatMeSdk.showingType = OpenOptionType.closeAll;
    initChatMeSdk.tapOnIndex = -1;
    initChatMeSdk.isReply = false;
    initChatMeSdk.isOverLength = false;
    initChatMeSdk.isLoadingPagination = false;
    initChatMeSdk.isDeviceConnected = false;
    initChatMeSdk.isInternetConnected = true;
    initChatMeSdk.noRecordReason = chatMeTr.noRecent;
    initChatMeSdk.isShowCopy = false;
    initChatMeSdk.isShowCongrats = false;
    initChatMeSdk.isDisabledSendButton = true;
    initChatMeSdk.currentPage = "0";
    initChatMeSdk.hasNextPage = false;
    initChatMeSdk.isAllowSendMessage = true;
  }
}