import 'dart:math';
import 'package:chatme_ui_sdk/util/app_color.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class AppSecureStorage {
  static const _storage = FlutterSecureStorage();
  static writeUserColor(String codeColor, String userKey) async {
    await _storage.write(key: userKey, value: codeColor);
  }
  static readUserColor(String userKey) async {
    String? codeColor = await _storage.read(key: userKey);
    if(codeColor == null) {
      final random = Random();
      String indexOfRandom = AppColor.userColors[random.nextInt(AppColor.userColors.length)];
      writeUserColor(indexOfRandom, userKey);
      return indexOfRandom;
    }
    return codeColor;
  }
}
