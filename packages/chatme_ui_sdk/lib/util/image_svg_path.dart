class AppSvgPath {
  static const String infoIconSvg = "lib/assets/svg/info.svg";
  static const String sendIconSvg = "lib/assets/svg/send.svg";
  static const String shareIconSvg = "lib/assets/svg/share.svg";
  static const String smileIconSvg = "lib/assets/svg/smile.svg";
  static const String closeIconSvg = "lib/assets/svg/close.svg";
  static const String pinIconSvg = "lib/assets/svg/pin.svg";
  static const String keyboardIconSvg = "lib/assets/svg/keyboard.svg";
  static const String logoBottomSheetIconSvg = "lib/assets/svg/icon_bottom_sheet.svg";
  static const String copyIconSvg = "lib/assets/svg/copy.svg";
}

class AppImagePath {
  static const String sendIconImage = "lib/assets/images/send.png";
  static const String liveBackgroundIconImage = "lib/assets/images/live-img.png";
}
