class SocketEvents {
  static const String receiveMessage = "message:receiveMessage";
  static const String sendMessage = "message:sendMessage";
  static const String getMessages = "message:getMessages";
  static const String bannedMessage = "message:bannedUser";
  static const String unBannedMessage = "message:unbannedUser";
  static const String userUpdated = "message:userUpdated";
  static const String merchantUpdate = "message:titleUpdated";
  static const String deleteMessage = "message:deleteMessage";
}
