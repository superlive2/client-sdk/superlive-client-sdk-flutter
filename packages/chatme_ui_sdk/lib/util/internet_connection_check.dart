import 'package:flutter/cupertino.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';

class AppInternetConnectionCheck {
  static checkIsInternetConnected({required Function(bool) listener}) {
    InternetConnectionChecker().onStatusChange.listen((status) {
      switch (status) {
        case InternetConnectionStatus.connected:
          debugPrint('Data connection is available.');
          listener(true);
          break;
        case InternetConnectionStatus.disconnected:
          debugPrint('You are disconnected from the internet.');
          listener(false);
          break;
      }
    });
  }
}
