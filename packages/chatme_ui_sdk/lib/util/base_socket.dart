import 'dart:async';
import 'package:chatme_ui_sdk/util/app_constant.dart';
import 'package:socket_io_client/socket_io_client.dart';

class BaseSocket {
  static Future<Socket> initConnection(String token, String nameSpace, {String path = "/socket.io", String? baseUrl}) async {
    baseUrl = baseUrl?? AppConstant.baseUrl;
    final completer = Completer<Socket>();
    Socket socket = io(
      baseUrl+nameSpace,
      OptionBuilder().setTransports(['websocket']).disableAutoConnect().setExtraHeaders({'token': token}).setPath(path).build(),
    );
    try {
      socket.connect();
      completer.complete(socket);
    } catch (e) {
      completer.completeError(socket);
    }
    return completer.future;
  }
}
