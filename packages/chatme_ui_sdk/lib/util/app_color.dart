class AppColor {
  static const String primaryColor = "#EE7326";
  static const String backgroundColor = "#FFFFFF";
  static const String blackColor = "#231F20";
  static const String infoColor = "#2F8BEB";
  static const String warningColor = "#FFAB00";
  static const String errorColor = "#F35625";
  static const String successColor = "#00CD2D";
  static const String greyColor = "#505050";
  static const String disabledColor = "#AEAEAE";
  static const String whiteColor = "#FFFFFF";

  static const List<String> userColors= [
    "#2F8BEB",
    "#EE7326",
    "#FFAB00",
    "#00CD2D",
    "#231F20",
    "#800000",
    "#808000",
    "#00FF00",
    "#008000",
    "#00FFFF",
    "#008080",
    "#0000FF",
    "#000080",
    "#FF00FF",
    "#800080",
    "#FFBF00",
    "#DFFF00",
    "#FF7F50",
    "#DE3163",
    "#9FE2BF",
    "#40E0D0",
    "#6495ED",
  ];
}
