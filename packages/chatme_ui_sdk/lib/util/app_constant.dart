import 'package:event_bus/event_bus.dart';
import 'package:flutter/material.dart';
import 'custom_font_weight.dart';

EventBus chatEventBus = EventBus();

class AppConstant {
  static String baseUrl = "wss://chatme.rgbatom.com"; //"wss://open-core.chat-me.chat";
  static const String keySeparateGiftOrNormalText = "@#NOHACKER#@!@#*&%@NoFAKE@!#";
  static const String appPackageName = "chatme_ui_sdk";
  static const String userColorKey = "@AbcUserCodeColor";
  static const _pattern = r'^(.*?)((?:https?:\/\/|www\.)[^\s/$.?#].[^\s]*)';
  static RegExp regExpCheckLink = RegExp(_pattern);
  static getCustomFontWeight(String fontWeight) {
    switch(fontWeight) {
      case CustomFontWeight.w100:
        return FontWeight.w100;
      case CustomFontWeight.w200:
        return FontWeight.w200;
      case CustomFontWeight.w300:
        return FontWeight.w300;
      case CustomFontWeight.w400:
        return FontWeight.w400;
      case CustomFontWeight.w500:
        return FontWeight.w500;
      case CustomFontWeight.w600:
        return FontWeight.w600;
      case CustomFontWeight.w700:
        return FontWeight.w700;
      case CustomFontWeight.w800:
        return FontWeight.w800;
      case CustomFontWeight.w900:
        return FontWeight.w900;
    }
    // return FontWeight.w500;
  }
}
