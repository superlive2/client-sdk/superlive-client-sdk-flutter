import 'package:chatme_ui_sdk/translate/translate.dart';
import 'package:chatme_ui_sdk/util/app_color.dart';
import 'package:chatme_ui_sdk/util/app_constant.dart';
import 'package:chatme_ui_sdk/util/hex_color.dart';
import 'package:chatme_ui_sdk/util/image_svg_path.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class AppCustomBottomSheet {
  static showAppBottomSheet(BuildContext context, {
    required VoidCallback onTap,
    String? textCancelButton,
    String? textOkButton,
    String? textMessage,
    String? iconPath,
  }) {
    showModalBottomSheet(
      context: context,
      // useSafeArea: false,
      shape:  const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(10.0),
          topRight: Radius.circular(10.0),
        ),
      ),
      isScrollControlled: true,
      builder: (ctx) {
        return ResendMessageBottomSheet(
          onTap: onTap,
          iconPath: iconPath?? AppSvgPath.logoBottomSheetIconSvg,
          textCancelButton: textCancelButton?? chatMeTr.cancel,
          textOkayButton: textOkButton?? chatMeTr.tryAgain,
          textMessage: textMessage?? chatMeTr.defaultMessage,
        );
      },
    );
  }
}

class ResendMessageBottomSheet extends StatelessWidget {
  final VoidCallback onTap;
  final String textMessage;
  final String textOkayButton;
  final String textCancelButton;
  final String iconPath;
  const ResendMessageBottomSheet({
    Key? key,
    required this.onTap,
    required this.iconPath,
    required this.textCancelButton,
    required this.textMessage,
    required this.textOkayButton,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Center(
                  child: Container(
                    width: 41,
                    height: 4,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: HexColor(AppColor.greyColor).withOpacity(0.2),
                    ),
                  ),
                ),
                const SizedBox(height: 16,),
                SvgPicture.asset(
                  iconPath,
                  package: AppConstant.appPackageName,
                ),
                const SizedBox(height: 16,),
                Text(
                  textMessage,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: HexColor(AppColor.blackColor),
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    height: 1.2,
                  ),
                ),
                const SizedBox(height: 32,),
              ],
            ),
          ),
          Container(height: 0.5, width: MediaQuery.of(context).size.width, color: HexColor(AppColor.greyColor).withOpacity(0.5),),
          const SizedBox(height: 24,),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: Row(
              children: [
                Expanded(
                  child: InkWell(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Container(
                      height: 48,
                      decoration: BoxDecoration(
                        color: HexColor(AppColor.whiteColor),
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(
                          width: 1,
                          color: HexColor(AppColor.primaryColor),
                        ),
                      ),
                      child: Center(
                        child: Text(
                          textCancelButton,
                          style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                            color: HexColor(AppColor.primaryColor),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                const SizedBox(width: 14,),
                Expanded(
                  child: InkWell(
                    onTap: () {
                      onTap();
                      Navigator.of(context).pop();
                    },
                    child: Container(
                      height: 48,
                      decoration: BoxDecoration(
                        color: HexColor(AppColor.primaryColor),
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(
                          width: 1,
                          color: HexColor(AppColor.primaryColor),
                        )
                      ),
                      child: Center(
                        child: Text(
                          textOkayButton,
                          style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                            color: HexColor(AppColor.whiteColor),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(height: 16,),
        ],
      ),
    );
  }
}
