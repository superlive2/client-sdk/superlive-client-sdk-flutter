import 'dart:developer';
import 'package:flutter/material.dart';

@protected
class BaseApiResponse<T> {
  final bool success;
  final List<dynamic>? error;
  final int? errorCode;
  final String? message;
  final Map<String, dynamic>? otherInfo;
  final dynamic result;
  const BaseApiResponse({
    this.otherInfo,
    this.success = true,
    this.errorCode,
    this.message,
    this.result,
    this.error,
  });
  static Map<String, dynamic> generateRequest(dynamic request) {
    return {'body': request};
  }
  static generateResponse({
    required Map<String?, dynamic> response,
    required Function(BaseApiResponse) callBack,
  }) {
    BaseApiResponse? resp;
    if (response.containsKey('errorCode')) {
      int errorCode = response['errorCode'] as int;
      List<dynamic>? error = response['error'] as List<dynamic>?;
      String message = response['message'] ?? 'no message';
      Map<String, dynamic> otherInfo = {};
      for (var i in response.keys) {
        if (i != 'errorCode' || i != 'error' || i != 'message') {
          otherInfo[i ?? '-'] = response[i];
        }
      }
      resp = BaseApiResponse(
        success: false,
        message: message,
        error: error,
        errorCode: errorCode,
        otherInfo: otherInfo,
      );
    } else if (response.containsKey('data')) {
      resp = BaseApiResponse(
        success: true,
        result: response,
      );
    } else {
      Map<String, dynamic> otherInfo = {};
      for (var i in response.keys) {
        if (i != 'errorCode' || i != 'error' || i != 'message') {
          otherInfo[i ?? '-'] = response[i];
        }
      }
      log('Base Response Error:  $response');
      resp = BaseApiResponse(otherInfo: otherInfo);
    }
    callBack(resp);
  }

  @override
  String toString() {
    return 'ApiBase(success: $success, error: $error, errorCode: $errorCode, message: $message, otherInfo: $otherInfo, result: $result)';
  }
}
