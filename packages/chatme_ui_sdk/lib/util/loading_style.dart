import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:io' show Platform;

class LoadingStyle extends StatelessWidget {
  const LoadingStyle({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Platform.isAndroid? const CircularProgressIndicator(): const CupertinoActivityIndicator(),
    );
  }
}
