//MIT License
//
//Copyright (c) [2019] [Befovy]
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.

part of fijkplayer;

/// Default builder generate default [FijkPanel] UI
Widget defaultFijkPanelBuilder(FijkPlayer player, FijkData data, BuildContext context, Size viewSize, Rect texturePos, tapChat) {
  return _DefaultFijkPanel(
    player: player,
    buildContext: context,
    viewSize: viewSize,
    texturePos: texturePos,
    tapChat: tapChat,
  );
}

String _duration2String(Duration duration) {
  if (duration.inMilliseconds < 0) return "-: negtive";

  String twoDigits(int n) {
    if (n >= 10) return "$n";
    return "0$n";
  }

  String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
  String twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60));
  int inHours = duration.inHours;
  return inHours > 0 ? "$inHours:$twoDigitMinutes:$twoDigitSeconds" : "$twoDigitMinutes:$twoDigitSeconds";
}

bool _internetConnectionIsConnected = true;
double fijkPlayerVolume = 1.0;
bool isfijkPlayerVolume = true;

/// Default Panel Widget
class _DefaultFijkPanel extends StatefulWidget {
  final FijkPlayer player;
  final BuildContext buildContext;
  final Size viewSize;
  final Rect texturePos;
  final tapChat;

  const _DefaultFijkPanel({
    required this.player,
    required this.buildContext,
    required this.viewSize,
    required this.texturePos,
    required this.tapChat,
  });

  @override
  _DefaultFijkPanelState createState() => _DefaultFijkPanelState();
}

class _DefaultFijkPanelState extends State<_DefaultFijkPanel> with SingleTickerProviderStateMixin {
  FijkPlayer get player => widget.player;
  Duration _duration = const Duration();
  Duration _currentPos = const Duration();
  Duration _bufferPos = const Duration();
  bool _playing = false;
  bool _prepared = false;
  String? _exception;
  double _seekPos = -1.0;
  StreamSubscription? _currentPosSubs;
  StreamSubscription? _bufferPosSubs;
  Timer? _hideTimer;
  bool _hideStuff = true;
  final barHeight = 40.0;
  StreamSubscription<InternetConnectionStatus>? _internetConnectionListener;
  StreamSubscription? _videoPlayEventBus;
  int _numberOfReLoad = 0;
  int _maxNumberOfReLead = 3;

  int indexOfUrl = 1;
  String _url = playerUrl.firstWhere((url) => url.contains('medium'), orElse: () => playerUrl[0]);
  String selectQuality = 'auto';

  @override
  void initState() {
    super.initState();
    /// _setUpPlayer();
    _resetPlayVideo();
    /// Check video internet speed
    InternetSpeedHelper.videoQualityByInternetSpeed(
      playerUrl,
      callBack: (url) {
        if (selectQuality == "auto") {
          if (_url != url && url.isNotEmpty) {
            setState(() {
              _url = url;
            });
            _resetPlayVideo();
          }
        }
      },
    );

    _numberOfReLoad = 0;
    _checkInternetConnection();
    _setUpPlayer();
    _videoPlayEventBus = fijkPlayerEventBus.on<PlayVideoEventBus>().listen((event) {
      if (event.state == PlayVideoEventBusState.startLive) {
        _resetPlayVideo();
        _numberOfReLoad = 0;
      } else {
        player.stop();
      }
    });
  }

  void _resetPlayVideo() async {
    await player.reset();
    await playVideo(_url);
    // await _setUpPlayer();
    await player.prepareAsync();
    player.start();
  }

  void _setUpPlayer() {
    _duration = player.value.duration;
    _currentPos = player.currentPos;
    _bufferPos = player.bufferPos;
    _prepared = player.state.index >= FijkState.prepared.index;
    _playing = player.state == FijkState.started;
    _exception = "${player.value.exception.message?? ''}";
    player.addListener(_playerValueChanged);
    _currentPosSubs = player.onCurrentPosUpdate.listen((v) {
      setState(() {
        _currentPos = v;
      });
    });
    _bufferPosSubs = player.onBufferPosUpdate.listen((v) {
      setState(() {
        _bufferPos = v;
      });
    });
  }

  void _checkInternetConnection() {
    _internetConnectionListener = InternetConnectionChecker().onStatusChange.listen((status) async {
      switch (status) {
        case InternetConnectionStatus.connected:
          WidgetsBinding.instance.addPostFrameCallback((_) {
            if (_internetConnectionIsConnected != true) {
              setState(() {
                _internetConnectionIsConnected = true;
              });
            }
          });
          _resetPlayVideo();
          _numberOfReLoad = 0;
          break;
        case InternetConnectionStatus.disconnected:
          if (_playing) {
            player.pause();
          }
          WidgetsBinding.instance.addPostFrameCallback((_) {
            if (_internetConnectionIsConnected != false) {
              setState(() {
                _internetConnectionIsConnected = false;
              });
            }
          });
          break;
      }
    });
  }

  void _playerValueChanged() {
    FijkValue value = player.value;
    if (value.duration != _duration) {
      setState(() {
        _duration = value.duration;
      });
    }
    bool playing = (value.state == FijkState.started);
    bool prepared = value.prepared;
    String? exception = value.exception.message;
    if (playing != _playing || prepared != _prepared || exception != _exception) {
      setState(() {
        _playing = playing;
        _prepared = prepared;
        _exception = exception;
      });
    }
  }

  void _playOrPause() async {
    if (_playing == true) {
      player.pause();
    } else {
      if (_internetConnectionIsConnected) {
        _resetPlayVideo();
      }
    }
  }

  @override
  void dispose() {
    _internetConnectionListener?.cancel();
    _hideTimer?.cancel();
    player.removeListener(_playerValueChanged);
    _currentPosSubs?.cancel();
    _bufferPosSubs?.cancel();
    _videoPlayEventBus?.cancel();
    super.dispose();
  }

  void _startHideTimer() {
    _hideTimer?.cancel();
    _hideTimer = Timer(const Duration(seconds: 3), () {
      setState(() {
        _hideStuff = true;
      });
    });
  }

  void _cancelAndRestartTimer() {
    if (_hideStuff == true) {
      _startHideTimer();
    }
    setState(() {
      _hideStuff = !_hideStuff;
    });
  }

  Widget _buildVolumeButton() {
    IconData iconData;
    if (!isfijkPlayerVolume || fijkPlayerVolume == 0) {
      iconData = Icons.volume_off;
    } else {
      iconData = Icons.volume_up;
    }
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        IconButton(
          icon: Icon(iconData),
          padding: const EdgeInsets.only(left: 10.0),
          onPressed: () {
            if (isfijkPlayerVolume) {
              isfijkPlayerVolume = false;
              player.setVolume(0);
            } else {
              isfijkPlayerVolume = true;
              player.setVolume(fijkPlayerVolume);
            }
            setState(() {});
          },
        ),
        SizedBox(
          height: 30,
          child: FittedBox(
            fit: BoxFit.cover,
            child: Builder(
              builder: (context) {
                return Slider(
                  autofocus: true,
                  value: fijkPlayerVolume * 100,
                  max: 100,
                  min: 0,
                  divisions: 100,
                  activeColor: Colors.grey[700],
                  inactiveColor: Colors.grey[200],
                  onChangeEnd: (e) {
                    if (fijkPlayerVolume > 0) isfijkPlayerVolume = true;
                    Timer(const Duration(seconds: 3), () {
                      setState(() {
                        _hideStuff = true;
                      });
                    });
                  },
                  onChanged: (double value) {
                    _hideStuff = false;
                    fijkPlayerVolume = value > 0 ? value / 100 : 0;
                    player.setVolume(fijkPlayerVolume);
                  },
                );
              },
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildVideoQualityButton(BuildContext ctx, String selectedValue) {
    bool? _auto = selectedValue == 'auto';
    bool? _1080p = selectedValue == '1080p';
    bool? _720p = selectedValue == '720p';
    bool? _480p = selectedValue == '480p';

    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        IconButton(
          icon: const Icon(Icons.settings),
          padding: const EdgeInsets.only(left: 10.0),
          onPressed: () {
            showModalBottomSheet<void>(
              context: ctx,
              backgroundColor: Colors.white,
              isScrollControlled: true,
              shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.vertical(
                  top: Radius.circular(16),
                ),
              ),
              builder: (BuildContext context) {
                return DefaultTextStyle(
                  style: const TextStyle(fontSize: 22, color: Colors.black),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      const SizedBox(
                        height: 4,
                      ),
                      Container(
                        alignment: Alignment.center,
                        padding: const EdgeInsets.all(10),
                        child: const Text(
                          "质量",
                          style: TextStyle(
                            fontWeight: FontWeight.w700,
                            fontSize: 15,
                          ),
                        ),
                      ),
                      const Divider(),
                      CheckboxListTile.adaptive(
                        value: _auto,
                        onChanged: (v) {
                          setQuality(1, context);
                          selectQuality = 'auto';
                        },
                        title: const Text('自动（推荐）'),
                      ),
                      CheckboxListTile.adaptive(
                        value: _1080p,
                        onChanged: (v) {
                          setQuality(2, context);
                          selectQuality = '1080p';
                        },
                        title: const Text('1080p'),
                      ),
                      CheckboxListTile.adaptive(
                        value: _720p,
                        onChanged: (v) {
                          setQuality(1, context);
                          selectQuality = '720p';
                        },
                        title: const Text('720p'),
                      ),
                      CheckboxListTile.adaptive(
                        value: _480p,
                        onChanged: (v) {
                          setQuality(0, context);
                          selectQuality = '480p';
                        },
                        title: const Text('480p'),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                );
              },
            );
          },
        ),
      ],
    );
  }

  void setQuality(int index, BuildContext context) {
    if (index == 0) {
      _url = playerUrl.firstWhere((url) => url.contains('low'), orElse: () => playerUrl[0]);
    } else if (index == 1) {
      _url = playerUrl.firstWhere((url) => url.contains('medium'), orElse: () => playerUrl[0]);
    } else if (index == 2) {
      _url = playerUrl.firstWhere((url) => url.contains('high'), orElse: () => playerUrl[0]);
    }
    setState(() {});
    _resetPlayVideo();
    Navigator.of(context, rootNavigator: true).pop();
  }

  AnimatedOpacity _buildBottomBar(BuildContext context) {
    double duration = _duration.inMilliseconds.toDouble();
    double currentValue = _seekPos > 0 ? _seekPos : _currentPos.inMilliseconds.toDouble();
    currentValue = min(currentValue, duration);
    currentValue = max(currentValue, 0);
    return AnimatedOpacity(
      opacity: _hideStuff ? 0.0 : 0.8,
      duration: const Duration(milliseconds: 400),
      child: Container(
        height: barHeight,
        color: Theme.of(context).dialogBackgroundColor,
        child: Row(
          children: <Widget>[
            const SizedBox(
              width: 10,
            ),
            _buildVolumeButton(),
            _duration.inMilliseconds == 0
                ? const Expanded(child: Center())
                : Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(right: 0, left: 0),
                      child: FijkSlider(
                        value: currentValue,
                        cacheValue: _bufferPos.inMilliseconds.toDouble(),
                        min: 0.0,
                        max: duration,
                        onChanged: (v) {
                          _startHideTimer();
                          setState(() {
                            _seekPos = v;
                          });
                        },
                        onChangeEnd: (v) {
                          setState(() {
                            player.seekTo(v.toInt());
                            _currentPos = Duration(milliseconds: _seekPos.toInt());
                            _seekPos = -1;
                          });
                        },
                      ),
                    ),
                  ),
            const SizedBox(
              width: 10,
            ),

            /// Setting
            _buildVideoQualityButton(context, selectQuality),

            if (widget.player.value.fullScreen) // && !isShowLiveChat)
              GestureDetector(
                onTap: widget.tapChat,
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: SvgPicture.asset(
                    "assets/svg/comment.svg",
                    package: FijkConstant.packageName,
                    color: Colors.black,
                  ),
                ),
              ),
            // const SizedBox(width: 10,),
            // _duration.inMilliseconds == 0
            //     ? Container(child: const Text("LIVE"))
            //     : Padding(
            //         padding: EdgeInsets.only(right: 5.0, left: 5),
            //         child: Text(
            //           '${_duration2String(_duration)}',
            //           style: TextStyle(
            //             fontSize: 14.0,
            //           ),
            //         ),
            //       ),

            IconButton(
              icon: Icon(widget.player.value.fullScreen ? Icons.fullscreen_exit : Icons.fullscreen),
              padding: const EdgeInsets.only(left: 10.0, right: 10.0),
              onPressed: () {
                widget.player.value.fullScreen ? player.exitFullScreen() : player.enterFullScreen();
              },
            ),
            const SizedBox(
              width: 10,
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Rect rect = widget.player.value.fullScreen //player.value.fullScreen
        ? Rect.fromLTWH(0, 0, widget.viewSize.width, widget.viewSize.height)
        : Rect.fromLTRB(
            max(0.0, widget.texturePos.left),
            max(0.0, widget.texturePos.top),
            min(widget.viewSize.width, widget.texturePos.right),
            min(widget.viewSize.height, widget.texturePos.bottom),
          );
    if ((player.state == FijkState.error || player.state == FijkState.end) && _numberOfReLoad < _maxNumberOfReLead) {
      _resetPlayVideo();
      _numberOfReLoad++;
    } else if (_playing == true && _numberOfReLoad != 0) {
      _numberOfReLoad = 0;
    }
    return Positioned.fromRect(
      rect: rect,
      child: Row(
        children: [
          Expanded(
            child: Stack(
              children: [
                Positioned.fill(
                  child: GestureDetector(
                    onTap: _cancelAndRestartTimer,
                    child: AbsorbPointer(
                      absorbing: _hideStuff,
                      child: Column(
                        children: <Widget>[
                          Expanded(
                            child: GestureDetector(
                              onTap: () {
                                _cancelAndRestartTimer();
                              },
                              child: Stack(
                                children: [
                                  Positioned.fill(
                                    child: Container(
                                      color: Colors.transparent,
                                      height: double.infinity,
                                      width: double.infinity,
                                      child: _internetConnectionIsConnected
                                          ? Center(
                                              child: _exception != null
                                                  ? Text(
                                                      "${player.value.exception.code}" == "-875574520" || "${player.value.exception.code}" == "-1094995529" ? broadcastSoonText ?? "" : "",
                                                      style: TextStyle(
                                                        color: Colors.grey[100],
                                                        fontSize: 17,
                                                        fontWeight: FontWeight.w700,
                                                        shadows: _shadowStyle,
                                                      ),
                                                    )
                                                  : (_prepared || player.state == FijkState.initialized)
                                                      ? AnimatedOpacity(
                                                          opacity: _hideStuff ? 0.0 : 0.7,
                                                          duration: const Duration(milliseconds: 400),
                                                          child: IconButton(
                                                            iconSize: barHeight * 2,
                                                            icon: Icon(
                                                              _playing ? Icons.pause : Icons.play_arrow,
                                                              color: Colors.white,
                                                              shadows: _shadowStyle,
                                                            ),
                                                            padding: const EdgeInsets.only(
                                                              left: 10.0,
                                                              right: 10.0,
                                                            ),
                                                            onPressed: _playOrPause,
                                                          ),
                                                        )
                                                      : const SizedBox(),
                                              //     : SizedBox(
                                              //   width: barHeight,
                                              //   height: barHeight,
                                              //   child: const CircularProgressIndicator(
                                              //     valueColor: AlwaysStoppedAnimation(Colors.white),
                                              //   ),
                                              // ),
                                            )
                                          : Column(
                                              mainAxisSize: MainAxisSize.min,
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children: [
                                                Text("${errorMessage?.errorTitle}",
                                                    style: TextStyle(
                                                      color: Colors.grey[100],
                                                      fontSize: 17,
                                                      fontWeight: FontWeight.w700,
                                                      shadows: _shadowStyle,
                                                    )),
                                                Text("${errorMessage?.errorMessage}",
                                                    style: TextStyle(
                                                      color: Colors.grey[100],
                                                      fontSize: 14,
                                                      fontWeight: FontWeight.w400,
                                                      shadows: _shadowStyle,
                                                    )),
                                                const SizedBox(
                                                  height: 10,
                                                ),
                                                GestureDetector(
                                                  onTap: () async {
                                                    if (_internetConnectionIsConnected) {
                                                      if (!_playing && player.state != FijkState.error) {
                                                        player.start();
                                                      } else if (player.state == FijkState.error) {
                                                        _resetPlayVideo();
                                                      }
                                                    }
                                                  },
                                                  child: Container(
                                                    padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                                                    decoration: BoxDecoration(
                                                      color: Colors.white,
                                                      borderRadius: BorderRadius.circular(8),
                                                    ),
                                                    child: Text("${errorMessage?.retryTextButton}",
                                                        style: const TextStyle(
                                                          color: Colors.black,
                                                          fontSize: 14,
                                                          fontWeight: FontWeight.w700,
                                                        )),
                                                  ),
                                                ),
                                              ],
                                            ),
                                    ),
                                  ),
                                  if (player.state == FijkState.idle)
                                    IgnorePointer(
                                      ignoring: true,
                                      child: Center(
                                        child: SizedBox(
                                          width: barHeight,
                                          height: barHeight,
                                          child: const CircularProgressIndicator(
                                            valueColor: AlwaysStoppedAnimation(Colors.white),
                                          ),
                                        ),
                                      ),
                                    ),
                                ],
                              ),
                            ),
                          ),
                          _buildBottomBar(context),
                        ],
                      ),
                    ),
                  ),
                ),
                if (player.state == FijkState.completed || player.state == FijkState.stopped)
                  Container(
                    color: Colors.black,
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "$broadcastSoonText",
                          style: TextStyle(
                            color: Colors.grey[100],
                            fontSize: 17,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ],
                    ),
                  ),
                if (countLiveView != null && _isShowLive)
                  Positioned(
                    top: 6,
                    left: 45,
                    child: Row(
                      children: [
                        SvgPicture.asset(
                          "assets/svg/icon-live.svg",
                          package: "fijkplayer",
                        ),
                        const SizedBox(
                          width: 8,
                        ),
                        Container(
                          height: 23,
                          padding: const EdgeInsets.symmetric(horizontal: 8),
                          decoration: BoxDecoration(
                            color: Colors.grey.withOpacity(0.2),
                            borderRadius: BorderRadius.circular(4),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SvgPicture.asset(
                                "assets/svg/viwer.svg",
                                package: "fijkplayer",
                              ),
                              const SizedBox(
                                width: 8,
                              ),
                              Text(
                                "$countLiveView",
                                style: const TextStyle(
                                  color: Colors.white,
                                  fontSize: 12,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(
                          width: 8,
                        ),
                      ],
                    ),
                  ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  final _shadowStyle = const <Shadow>[
    Shadow(
      offset: Offset(0.0, 0.0),
      blurRadius: 9.0,
      color: Color.fromARGB(255, 0, 0, 0),
    ),
  ];
  Future<void> playVideo(String url) async {
     // player.setOption(FijkOption.playerCategory, "mediacodec", 0);
     // player.setOption(FijkOption.playerCategory, "opensles", 0);
     // player.setOption(FijkOption.playerCategory, "mediacodec-hevc", 0);
     // player.setOption(FijkOption.formatCategory, "buffer_size", 13160);
     // player.setOption(FijkOption.formatCategory, "max-buffer-size", 0);
     // player.setOption(FijkOption.playerCategory, "min-frames", 2);
     // player.setOption(FijkOption.playerCategory, "max_cached_duration", 1000);
     // player.setOption(FijkOption.formatCategory, "max-fps", 15);
     // player.setOption(FijkOption.formatCategory, "infbuf", 1);
     // player.setOption(FijkOption.formatCategory, "analyzemaxduration", 80);
     // player.setOption(FijkOption.formatCategory, "probesize", 10240);
     // player.setOption(FijkOption.formatCategory, "flush_packets", 1);
     // player.setOption(FijkOption.playerCategory, "fast", 1);
     // player.setOption(FijkOption.playerCategory, "mediacodec-handle-resolution-change", 0);
     // player.setOption(FijkOption.playerCategory, "packet-buffering", 0);
     // player.setOption(FijkOption.codecCategory, "skip_frame", 0);
     // player.setOption(FijkOption.playerCategory, "framedrop", 1);
     // player.setOption(FijkOption.codecCategory, "skip_loop_filter", 48);
     // player.setOption(FijkOption.formatCategory, "dns_cache_clear", 1);
     // player.setOption(FijkOption.formatCategory, "timeout", 10);
     // player.setOption(FijkOption.formatCategory, "acodec", "pcm_alaw"); // avoid probe
     // player.setOption(FijkOption.formatCategory, "ar", "8000"); // avoid probe
     // player.setOption(FijkOption.formatCategory, "ac", "1"); // avoid probe
     // player.setOption(FijkOption.formatCategory, "sample_fmt", "s16"); // avoid probe
     // player.setOption(FijkOption.formatCategory, "vcodec", "h264"); // avoid probe
     // player.setOption(FijkOption.formatCategory, "pixel_format", "yuv420p"); // avoid probe
     // player.setOption(FijkOption.formatCategory, "video_size", "1080x720"); // avoid probe
     // player.setOption(FijkOption.formatCategory, "max_delay", "0");
     // player.setOption(FijkOption.formatCategory, "err_detect", "8");
     // player.setOption(FijkOption.formatCategory, "fflags", "nobuffer");
     // player.setOption(FijkOption.formatCategory, "reorder_queue_size", 0);
    // await player.setOption(FijkOption.hostCategory, "enable-snapshot", 1);

     player.setOption(FijkOption.playerCategory, "mediacodec-all-videos", 1);
     player.setDataSource(url, autoPlay: true);
  }
}
