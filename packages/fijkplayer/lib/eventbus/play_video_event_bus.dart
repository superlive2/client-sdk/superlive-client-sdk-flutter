enum PlayVideoEventBusState {startLive, stopLive, refreshLive}

class PlayVideoEventBus {
  final String url;
  final PlayVideoEventBusState? state;

  const PlayVideoEventBus({
    required this.url,
    this.state,
  });
}
