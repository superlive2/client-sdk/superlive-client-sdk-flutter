class FijkPlayerErrorMessage {
  final String errorTitle;
  final String errorMessage;
  final String retryTextButton;

  FijkPlayerErrorMessage({
    this.errorMessage = "",
    this.errorTitle = "",
    this.retryTextButton = "",
  });
}
