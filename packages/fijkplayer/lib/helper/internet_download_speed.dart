import 'dart:async';
import 'dart:io';
import 'package:event_bus/event_bus.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';

class InternetSpeedHelper {
  static int speed = 0;
  static Timer? timer;
  static EventBus dynamicVideoUrlEvent = EventBus();
  static int tempTime = 0;

  static FutureOr<void> getDownloadSpeed() async {
    try {
      var url = 'http://ipv4.download.thinkbroadband.com/1MB.zip'; // URL of a file to download
      var stopwatch = Stopwatch()..start();

      var response = await http.get(Uri.parse(url));

      if (response.statusCode == 200) {
        var dir = await getTemporaryDirectory();
        var file = File('${dir.path}/temp.zip');
        await file.writeAsBytes(response.bodyBytes);

        var downloadTime = stopwatch.elapsedMilliseconds;
        var fileSizeInMB = response.bodyBytes.length / 1024 / 1024;
        var downloadSpeed = fileSizeInMB / (downloadTime / 1000);
        speed = (downloadSpeed * 8).round();
        stopwatch.stop();
      } else {
        stopwatch.stop();
        // throw Exception('Error downloading file');
      }
    } catch (e) {
      speed = 1; // default internet speed
    }
  }

  static videoQualityByInternetSpeed(List<String> urls, {required Function(String) callBack}) {
    var low = urls.firstWhere((url) => url.contains('low'), orElse: () => '');
    var medium = urls.firstWhere((url) => url.contains('medium'), orElse: () => '');
    var high = urls.firstWhere((url) => url.contains('high'), orElse: () => '');
    timer = Timer.periodic(const Duration(seconds: 60), (Timer t) async {
      getDownloadSpeed();
      if (tempTime != speed) {
        tempTime = speed;
        if (speed <= 2) {
          callBack(low);
        } else if (speed <= 3 && speed > 2) {
          callBack(medium);
        } else if (speed <= 20 && speed > 3) {
          callBack(high);
        } else {
          callBack(high);
        }
      }
    });
  }

  static void stopCheckingInternetSpeed() {
    if (timer != null) {
      timer!.cancel();
    }
  }
}
