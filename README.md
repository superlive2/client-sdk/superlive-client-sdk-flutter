## Getting started
Use Flutter Fair require few steps. Add dependency inside pubspec.yaml.

```yaml
dependencies:
  chat_video_sdk:
    path: packages/chat-video-sdk
```

## Usage

```dart
import 'package:chat_video_sdk/chat_video_sdk.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  ChatVideoPlayerConfig.init(
    apiKey: 'JnQCn0hBs33GEZf_FSMFmwl1',
    isProduction: false,
  );
}


import 'package:flutter/material.dart';
import 'package:chat_video_sdk/chat_video_sdk.dart';

class VideoPlayPage extends StatefulWidget {
  static const String routeName = '/video_play_page';

  const VideoPlayPage({Key? key}) : super(key: key);

  @override
  State<VideoPlayPage> createState() => _VideoPlayPageState();
}

class _VideoPlayPageState extends State<VideoPlayPage> {
  final _chatVideoPlayerHelper = ChatVideoPlayerHelper();

  @override
  void initState() {
    _chatVideoPlayerHelper.play(
      hostId: '648acfe655c136d605fe698a',
      participantsId: '648ad34a55c136d605fe6ae9',
      title: 'Play Title',
      description: 'Player Description',
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return _chatVideoPlayerHelper.builder(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Video Play'),
        ),
        body: Container(),
      ),
    );
  }
}
```