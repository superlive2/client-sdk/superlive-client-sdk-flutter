import 'package:chat_video_sdk/chat_video_sdk.dart';
import 'package:chat_video_sdk/data/translate/translate.dart';
import 'package:chatme_ui_sdk/util/app_color.dart';
import 'package:chatme_ui_sdk/util/hex_color.dart';
import 'package:flutter_svg/flutter_svg.dart';

class RequiredSigInPage extends StatelessWidget {
  const RequiredSigInPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: HexColor(AppColor.whiteColor),
        leading: GestureDetector(
          onTap: () => Navigator.of(context).pop(),
          child: SizedBox(
            width: 30,
            height: 30,
            child: Center(child: SvgPicture.asset("lib/assets/svg/close.svg", package: sdkConfig.appPackageName)),
          ),
        ),
      ),
     body: Container(
       color: HexColor(AppColor.whiteColor),
       padding: const EdgeInsets.symmetric(horizontal: 16.0),
       child: SizedBox(
         width: MediaQuery.of(context).size.width,
         child: Column(
           mainAxisAlignment: MainAxisAlignment.center,
           crossAxisAlignment: CrossAxisAlignment.start,
           children: [
             Center(child: SvgPicture.asset("lib/assets/svg/required_sigin_background.svg", package: sdkConfig.appPackageName,)),
             const SizedBox(height: 45,),
             Text(
               trans.signInToContinue,
               style: TextStyle(
                 fontWeight: FontWeight.w600,
                 fontSize: 18,
                 color: HexColor(AppColor.blackColor),
               ),
             ),
             Text(
               trans.getAccessToMemberOnly,
               style: TextStyle(
                 fontWeight: FontWeight.w300,
                 fontSize: 12,
                 color: HexColor(AppColor.blackColor).withOpacity(.7),
               ),
             ),
             const SizedBox(height: 45,),
             GestureDetector(
               onTap: () => Navigator.of(context).pop("goToLogIn"),
               child: Container(
                 height: 45,
                 decoration: BoxDecoration(
                   color: HexColor(AppColor.primaryColor),
                   borderRadius: BorderRadius.circular(10),
                 ),
                 child: Center(
                   child: Text(
                     trans.signIn,
                     style: TextStyle(
                       fontWeight: FontWeight.w600,
                       fontSize: 16,
                       color: HexColor(AppColor.whiteColor),
                     ),
                   ),
                 ),
               ),
             ),
             const SizedBox(height: 45,),
           ],
         ),
       ),
     ),
    );
  }
}
