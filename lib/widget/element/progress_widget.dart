import 'package:app_packages/app_package.dart';
import 'package:chatme_ui_sdk/util/app_color.dart';
import 'package:chatme_ui_sdk/util/hex_color.dart';

class ProgressWidget extends StatelessWidget {
  final Color? color;
  final int process;

  const ProgressWidget({super.key, this.color, required this.process});

  @override
  Widget build(BuildContext context) {
    final Color _color = color ?? HexColor(AppColor.primaryColor);
    return SizedBox(
      width: 25,
      child: AspectRatio(
        aspectRatio: 1,
        child: Stack(
          children: [
            CircularProgressIndicator(
              value: process / 100,
              backgroundColor: _color.withOpacity(0.2),
              valueColor: AlwaysStoppedAnimation<Color>(_color),
            ),
            Positioned.fill(
              child: Center(
                child: Text(
                  process.toString() + "%",
                  style: appTextTheme(context).labelSmall?.copyWith(
                    color: _color,
                    fontWeight: FontWeight.w700,
                    letterSpacing: 0.1,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
