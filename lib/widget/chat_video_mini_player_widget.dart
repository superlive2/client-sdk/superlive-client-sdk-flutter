import 'dart:async';

import 'package:chat_video_sdk/chat_video_sdk.dart';
import 'package:chat_video_sdk/logic/host/request_join_host_cubit.dart';
import 'package:chat_video_sdk/logic/host/request_left_host_cubit.dart';
import 'package:chat_video_sdk/logic/host/view_count_cubit.dart';
import 'package:chatme_ui_sdk/util/app_color.dart';
import 'package:chatme_ui_sdk/util/hex_color.dart';
import 'package:chatme_ui_sdk/util/init_chat_sdk.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:miniplayer/miniplayer.dart';

import '../data/socket_service/socket_service.dart';
import '../logic/chat/get_chat_host_cubit.dart';

bool requestJoinChat = true;
late RequestLeftHostCubit requestLeftHostCubit;
late RequestJoinHostCubit requestJoinHostCubit;
late ViewCountCubit viewCountCubit;
final SocketService socketService = SocketService();

class ChatVideoMiniPlayerWidget extends StatefulWidget {
  final Widget child;
  final Widget? chatHeaderWidget;
  final ChatVideoPlayerHelper helper;

  const ChatVideoMiniPlayerWidget({
    Key? key,
    required this.child,
    required this.helper,
    required this.chatHeaderWidget,
  }) : super(key: key);

  @override
  State<ChatVideoMiniPlayerWidget> createState() => _ChatVideoMiniPlayerWidgetState();
}

class _ChatVideoMiniPlayerWidgetState extends State<ChatVideoMiniPlayerWidget> with WidgetsBindingObserver {
  late HostCubit _cubit;
  late StreamSubscription<ChatVideoPlayerHelperEventBus> _eventBus;
  ChatVideoPlayerHelperEventBus? _selectHost;
  bool _isFocus = false;
  List<String> _urls = [];
  // List<String> _urls = [
  //   "http://192.168.160.231:8000/stream/64c8c9b5134de737ac69e38b_low.flv",
  //   "http://192.168.160.231:8000/stream/64c8c9b5134de737ac69e38b_medium.flv",
  //   "http://192.168.160.231:8000/stream/64c8c9b5134de737ac69e38b_high.flv",
  // ];

  @override
  void initState() {
    _cubit = context.read<HostCubit>();
    requestJoinHostCubit = context.read<RequestJoinHostCubit>();
    requestLeftHostCubit = context.read<RequestLeftHostCubit>();
    viewCountCubit = context.read<ViewCountCubit>();
    _eventBus = widget.helper.videoMiniHelper.eventBusMiniHelper.on<ChatVideoPlayerHelperEventBus>().listen((event) {
      _selectHost = event;
      Future.delayed(const Duration(milliseconds: 100), () {
        widget.helper.videoMiniHelper.miniPlayerController.animateToHeight(state: PanelState.MIN);
      });
      _cubit.request(event.hostId);
      viewCountCubit.request(event.hostId);
      context.read<GetChatHostCubit>().request();
      socketService.listenReceiveParticipantJoin(
        callBack: (_) {},
      );
    });
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    socketService.listenReceiveParticipantLeave(
      callBack: (_) {},
    );

    socketService.disConnect();
    _eventBus.cancel();
    super.dispose();
    WidgetsBinding.instance.removeObserver(this);
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      /// join host
      socketService.reConnect();
      socketService.listenReceiveParticipantJoin(
        callBack: (_) {},
      );
      if (widget.helper.hostId != null) requestJoinHostCubit.request(widget.helper.hostId);
    }
    if (state == AppLifecycleState.detached) {
      /// left host
      socketService.listenReceiveParticipantLeave(
        callBack: (_) {},
      );
      _isFocus = false;
      socketService.disConnect();
      if (widget.helper.hostId != null) requestLeftHostCubit.request(widget.helper.hostId);
    }
    if (state == AppLifecycleState.inactive) {
      /// left host
      socketService.listenReceiveParticipantLeave(
        callBack: (_) {},
      );
      _isFocus = false;
      socketService.disConnect();
      if (widget.helper.hostId != null) requestLeftHostCubit.request(widget.helper.hostId);
    }
    if (state == AppLifecycleState.paused) {
      /// left host
      socketService.listenReceiveParticipantLeave(
        callBack: (_) {},
      );
      _isFocus = false;
      socketService.disConnect();
      if (widget.helper.hostId != null) requestLeftHostCubit.request(widget.helper.hostId);
    }
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: Focus(
        onFocusChange: (isFocus) {
          setState(() {
            _isFocus = isFocus;
          });
        },
        child: Stack(
          children: [
            Positioned.fill(
              child: BlocConsumer<HostCubit, HostState>(
                listener: (context, state) {
                  if (_selectHost != null && state.stateStatus == SuperLiveStateStatus.success && state.data != null) {
                    // var url = state.data!.playOptions!.ws![0].urls[0];

                    /// Test url on prod
                    // _urls = [
                    //   "https://flv.art8848.cn/play/stream/64b0ef31296762b73321e303_low.flv",
                    //   "https://flv.art8848.cn/play/stream/64b0ef31296762b73321e303_medium.flv",
                    //   "https://flv.art8848.cn/play/stream/64b0ef31296762b73321e303_high.flv",
                    // ];

                    /// Test on local
                    // _urls = [
                    //   "http://192.168.160.192:8000/stream/64c8c9b5134de737ac69e38b_low.flv",
                    //   "http://192.168.160.192:8000/stream/64c8c9b5134de737ac69e38b_medium.flv",
                    //   "http://192.168.160.192:8000/stream/64c8c9b5134de737ac69e38b_high.flv",
                    // ];

                    /// Used 3 urls
                    _urls = [];
                    for(int i =0; i < (state.data!.playOptions?.ws?? []).length; i++) {
                      if("${state.data!.playOptions?.ws?[i].name}" != "standard") {
                        _urls.add("${state.data!.playOptions?.ws?[i].urls[0]}");
                      }
                    }

                    /// Used 1 url
                    // for (int i = 0; i < (state.data!.playOptions?.ws ?? []).length; i++) {
                    //   if ("${state.data!.playOptions?.ws?[i].name}" == "standard") {
                    //     _urls.add("${state.data!.playOptions?.ws?[i].urls[0]}");
                    //   }
                    // }

                    widget.helper.videoMiniHelper.pay(urls: _urls, title: _selectHost!.title, description: _selectHost!.description);
                  }
                },
                builder: (context, state) {
                  return widget.helper.videoMiniHelper.builder(
                    child: widget.child,
                    state: state,
                    chatHelper: widget.helper,
                    relatedChild: Theme(
                      data: ThemeData.light(),
                      // child: const Text('data '),
                      child: ChatWidget(
                        state: state,
                        helper: widget.helper,
                        chatHeaderWidget: widget.chatHeaderWidget,
                      ),
                    ),
                    onClose: () {
                      InitChatMeSdk.clearInitializeData();
                      socketService.disposed();
                      leaderBoardData = null;
                      requestJoinChat = true;
                      if (widget.helper.hostId != null) requestLeftHostCubit.request(widget.helper.hostId);
                      setState(() {});
                    },
                  );
                },
              ),
            ),
            if (_isFocus)
              Positioned(
                bottom: MediaQuery.of(context).viewInsets.bottom + 75,
                top: 0,
                left: 0,
                right: 0,
                child: GestureDetector(
                  onTap: () {
                    AppHelperWidget.focusNew(context);
                  },
                  onTapDown: (_) {
                    AppHelperWidget.focusNew(context);
                  },
                  child: Container(
                    color: HexColor(AppColor.blackColor).withOpacity(0),
                  ),
                ),
              ),
          ],
        ),
      ),
    );
  }
}
