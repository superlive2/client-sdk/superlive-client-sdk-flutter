import 'package:chat_video_sdk/chat_video_sdk.dart';

class VideoMiniWidget extends StatelessWidget {
  final String title;
  final String? description;

  const VideoMiniWidget({
    Key? key,
    required this.title,
    this.description,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 5),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(title, style: appTextTheme(context).headlineSmall, maxLines: 1, overflow: TextOverflow.ellipsis),
          if(description != null)...[
            const SizedBox(height: 5),
            Text(description!, style: appTextTheme(context).bodySmall, maxLines: 1, overflow: TextOverflow.ellipsis),
          ],
          const SizedBox(height: 10),
        ],
      ),
    );
  }
}
