import 'dart:async';
import 'package:after_layout/after_layout.dart';
import 'package:chat_video_sdk/chat_video_sdk.dart';
import 'package:chat_video_sdk/data/translate/translate.dart';
import 'package:chat_video_sdk/helper/eventbus/reset_lucky_draw_event_bus.dart';
import 'package:chat_video_sdk/helper/eventbus/vote_event_bus.dart';
import 'package:chat_video_sdk/logic/chat/get_chat_host_cubit.dart';
import 'package:chat_video_sdk/logic/chat/get_vote_cubit.dart';
import 'package:chat_video_sdk/logic/chat/top_gift_cubit.dart';
import 'package:chat_video_sdk/logic/host/request_join_host_cubit.dart';
import 'package:chat_video_sdk/widget/chat/gift.dart';
import 'package:chatme_ui_sdk/helper/helper.dart';
import 'package:chatme_ui_sdk/services/message_service.dart';
import 'package:chatme_ui_sdk/translate/translate.dart';
import 'package:chatme_ui_sdk/ui/screen/chatme_sdk.dart';
import 'package:chatme_ui_sdk/util/app_color.dart';
import 'package:chatme_ui_sdk/util/app_constant.dart';
import 'package:chatme_ui_sdk/util/custom_bottom_sheet.dart';
import 'package:chatme_ui_sdk/util/custom_font_weight.dart';
import 'package:chatme_ui_sdk/util/hex_color.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:lottie/lottie.dart';
import '../data/model/chat/gift_recieve_on_listen_model.dart';
import '../data/model/home/leaderboard_model.dart';
import '../helper/eventbus/recharge_pint.dart';
import 'animations/animations_style_widget.dart';
import 'chat/draggable_vote_winner_floating_widget.dart';

class ChatWidget extends StatefulWidget {
  final HostState state;
  final ChatVideoPlayerHelper helper;
  final Widget? chatHeaderWidget;

  const ChatWidget({
    Key? key,
    required this.state,
    required this.helper,
    required this.chatHeaderWidget,
  }) : super(key: key);

  @override
  State<ChatWidget> createState() => _ChatWidgetState();
}

LeaderBoardModel? leaderBoardData;
late SendGiftCubit cubitSendGift;
late GetVoteCubit getVoteCubit;
late WinnerCubit getWinnerCubit;
int setTimeoutPerMessage = 0;
bool displayGiftedLeaderboard = false;
GiftResponse? giftCongrats;

class _ChatWidgetState extends State<ChatWidget> with AfterLayoutMixin {
  late GiftsPacksCubit _cubitPacks;
  late TopGiftsCubit _topGiftsCubit;
  late ChatHeaderCubit _cubitHeaderChat;
  bool _isShowCongratsScreen = false;
  GiftResponse? _selectedGift;
  List<PackResponse>? _listPackages;
  final MessageService _messageService = MessageService();
  int? _selectedIndexVote;
  bool _isFinishedVote = false;
  final GlobalKey _parentKey = GlobalKey();
  String countParts = "0";
  String numberOfGift = "1";

  @override
  void initState() {
    getWinnerCubit = context.read<WinnerCubit>();
    getWinnerCubit.hostId = "${widget.helper.hostId}";
    getWinnerCubit.request();
    getVoteCubit = context.read<GetVoteCubit>();
    _cubitHeaderChat = context.read<ChatHeaderCubit>();
    requestJoinHostCubit.participantId = widget.helper.participantsId!;
    _topGiftsCubit = context.read<TopGiftsCubit>();
    if (requestJoinHostCubit.state.data == null || requestJoinChat) {
      requestJoinChat = false;
      requestJoinHostCubit.request(widget.helper.hostId);
    }
    if (leaderBoardData == null) {
      _topGiftsCubit.request(widget.helper.hostId);
    }
    _cubitPacks = context.read<GiftsPacksCubit>();
    cubitSendGift = context.read<SendGiftCubit>();
    cubitSendGift.participantId = widget.helper.participantsId!;
    cubitSendGift.hostId = widget.helper.hostId!;
    requestLeftHostCubit.participantId = widget.helper.participantsId!;
    getVoteCubit.participantId = widget.helper.participantsId!;
    getVoteCubit.request(widget.helper.hostId);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      color: HexColor(AppColor.whiteColor),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          BlocConsumer<TopGiftsCubit, TopGiftsState>(
            listener: (context, state) {
              if (state.stateStatus == SuperLiveStateStatus.success || state.stateStatus == SuperLiveStateStatus.empty) {
                leaderBoardData = state.data!;
                SchedulerBinding.instance.addPostFrameCallback((_) {
                  setState(() {});
                });
              }
            },
            builder: (context, state) {
              if (state.stateStatus == SuperLiveStateStatus.success) {
                return SizedBox(
                  child: PhysicalModel(
                    elevation: 0.7,
                    color: HexColor(AppColor.primaryColor),
                    child: Container(
                      // height: 50,
                      padding: const EdgeInsets.only(left: 16, right: 16),
                      width: MediaQuery.of(context).size.width,
                      color: HexColor(AppColor.whiteColor).withOpacity(0.95),
                      child: BlocBuilder<ChatHeaderCubit, ChatHeaderState>(
                        builder: (context, state) {
                          if (state.stateStatus == ChatHeaderStateStatus.vote) {
                            return state.step == 1 ? Row(
                              children: [
                                Expanded(
                                  child: ChatHeaderVoteWidget(
                                    onVoteTap: () {
                                      _cubitHeaderChat.goToStep(2);
                                    },
                                  ),
                                ),
                              ],
                            ) : (state.step == 2 ? ChatHeaderVoteStep2Widget(
                              onClose: () {
                                _cubitHeaderChat.goTo(ChatHeaderStateStatus.initial);
                              },
                            )
                          : Row(
                              children: [
                                Expanded(
                                  child: AppGestureDetector(
                                    onTap: () {
                                      _cubitHeaderChat.goToStep(3);
                                    },
                                    child: const ChatHeaderVoteStep3Widget(),
                                  ),
                                ),
                              ],
                            ));
                          } else if (state.stateStatus == ChatHeaderStateStatus.winner) {
                            return state.step == 1 ? Row(
                              children: [
                                Expanded(
                                  child: AppGestureDetector(
                                    onTap: () {
                                      _cubitHeaderChat.goToStep(2);
                                    },
                                    child: const SizedBox(),
                                  ),
                                ),
                              ],
                            ) : ChatHeaderWinnerStep2Widget(
                              requestClose: (isForceClose) {
                                if (isForceClose) _cubitHeaderChat.goTo(ChatHeaderStateStatus.initial);
                              },
                              onClose: () {
                                _cubitHeaderChat.goTo(ChatHeaderStateStatus.initial);
                              },
                            );
                          }
                          return GestureDetector(
                            onTap: () {
                              if (leaderBoardData?.data?.isNotEmpty == true) {
                                if (leaderBoardData != null) {
                                  setState(() {
                                    leaderBoardData?.isExpandLeaderBoard = !(leaderBoardData?.isExpandLeaderBoard)!;
                                  });
                                }
                              }
                            },
                            child: leaderBoardData?.data?.isEmpty == true
                                ? Center(
                                    child: Text(
                                      trans.noRecord,
                                      style: TextStyle(color: HexColor(AppColor.blackColor), fontWeight: FontWeight.w400, fontSize: 12),
                                    ),
                                  )
                                : (leaderBoardData != null && displayGiftedLeaderboard)
                                    ? SizedBox(
                                        height: 50,
                                        child: leaderBoardData?.isExpandLeaderBoard != true
                                            ? ListView.builder(
                                                shrinkWrap: true,
                                                physics: const NeverScrollableScrollPhysics(),
                                                scrollDirection: Axis.horizontal,
                                                itemCount: (leaderBoardData?.data?.length ?? 0) > 3 ? 3 : leaderBoardData?.data?.length,
                                                itemBuilder: (context, index) {
                                                  DatumLeaderboard? _itm = leaderBoardData?.data?[index];
                                                  return AnimatedSize(
                                                    duration: const Duration(milliseconds: 500),
                                                    child: SizedBox(
                                                      height: 50,
                                                      width: (MediaQuery.of(context).size.width - 32) / 3,
                                                      child: Row(
                                                        children: [
                                                          SizedBox(
                                                            width: 20,
                                                            height: 45,
                                                            child: Image.asset(
                                                              "lib/assets/images/medals/$index.png",
                                                              package: sdkConfig.appPackageName,
                                                            ),
                                                          ),
                                                          const SizedBox(
                                                            width: 12,
                                                          ),
                                                          Expanded(
                                                            child: Column(
                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                                              mainAxisAlignment: MainAxisAlignment.center,
                                                              children: [
                                                                Text(
                                                                  "${_itm?.name}",
                                                                  overflow: TextOverflow.ellipsis,
                                                                  maxLines: 1,
                                                                  style: TextStyle(
                                                                    color: HexColor(AppColor.blackColor),
                                                                    fontSize: 14,
                                                                    fontWeight: FontWeight.w500,
                                                                  ),
                                                                ),
                                                                Text(
                                                                  "${_itm?.totalGiftPoint}",
                                                                  style: TextStyle(
                                                                    color: HexColor(AppColor.blackColor),
                                                                    fontSize: 10,
                                                                    fontWeight: FontWeight.w500,
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  );
                                                },
                                              )
                                            : AnimatedSize(
                                                duration: const Duration(milliseconds: 500),
                                                child: Row(
                                                  children: [
                                                    Text(
                                                      trans.leaderBoard,
                                                      style: TextStyle(
                                                        fontSize: 15,
                                                        color: HexColor(AppColor.blackColor),
                                                        fontWeight: FontWeight.w500,
                                                      ),
                                                    ),
                                                    const Expanded(child: SizedBox()),
                                                    Icon(
                                                      Icons.close,
                                                      color: HexColor(AppColor.blackColor),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                      )
                                    : const SizedBox(),
                          );
                        },
                      ),
                    ),
                  ),
                );
              }
              return const SizedBox();
            },
          ),
          Expanded(
            child: BlocBuilder<ChatHeaderCubit, ChatHeaderState>(
              builder: (context, stateHead) {
                return Stack(
                  children: [
                    SizedBox(
                      width: MediaQuery.of(context).size.width,
                      child: Column(
                        children: [
                          if (widget.chatHeaderWidget != null) widget.chatHeaderWidget!,
                          Expanded(
                            child: BlocConsumer<RequestJoinHostCubit, RequestJoinHostState>(
                              listener: (context, stateRequestChat) async {
                                if (stateRequestChat.stateStatus == SuperLiveStateStatus.success) {
                                  _cubitPacks.request(widget.helper.hostId);
                                }
                              },
                              builder: (context, stateRequestChat) {
                                if (stateRequestChat.stateStatus == SuperLiveStateStatus.success) {
                                  return BlocListener<SendGiftCubit, SendGiftState>(
                                    listener: (context, state) {
                                      if (state.stateStatus == SuperLiveStateStatus.success && _selectedGift != null) {
                                        SchedulerBinding.instance.addPostFrameCallback((_) {
                                          setState(() {
                                            _isShowCongratsScreen = true;
                                          });
                                        });
                                        Future.delayed(const Duration(seconds: 4), () {
                                          SchedulerBinding.instance.addPostFrameCallback((_) {
                                            setState(() {
                                              _isShowCongratsScreen = false;
                                            });
                                          });
                                        });
                                        _messageService.sendMessage(
                                          "🎁${_selectedGift!.name} - ${_selectedGift!.point}" + trans.pts+ (numberOfGift == "1"? "": " x$numberOfGift"),
                                          messageStyle: {
                                            "color": AppColor.primaryColor,
                                            "fontSize": "12",
                                            "fontWeight": CustomFontWeight.w900,
                                          },
                                          callBack: (e) {
                                            _selectedGift = null;
                                            SchedulerBinding.instance.addPostFrameCallback((_) {
                                              setState(() {});
                                            });
                                            chatEventBus.fire(const SentGiftEvent());
                                          },
                                          onError: (e, v) {
                                            //
                                          },
                                        );
                                      } else if (state.stateStatus == SuperLiveStateStatus.failure) {
                                        if(state.error == "PARTICIPANT_IS_BANNED") {
                                          Fluttertoast.showToast(
                                            msg: chatMeTr.userBanned,
                                            toastLength: Toast.LENGTH_SHORT,
                                            timeInSecForIosWeb: 2,
                                            backgroundColor: Colors.black,
                                            textColor: Colors.white,
                                            fontSize: 16.0,
                                          );
                                          return;
                                        } else if(state.error == "NOT_ENOUGH_POINTS") {
                                          AppCustomBottomSheet.showAppBottomSheet(
                                            context,
                                            textOkButton: chatMeTr.reChargePoint,
                                            textMessage: state.message,
                                            onTap: () {
                                              appEventBus.fire(RechargePintEventBus(isTrue: true));
                                            },
                                          );
                                          return;
                                        }
                                        Fluttertoast.showToast(
                                          msg: state.message.toString(),
                                          toastLength: Toast.LENGTH_SHORT,
                                          timeInSecForIosWeb: 2,
                                          backgroundColor: Colors.black,
                                          textColor: Colors.white,
                                          fontSize: 16.0,
                                        );
                                        return;
                                      }
                                    },
                                    child: widget.state.stateStatus == SuperLiveStateStatus.loading
                                        ? const AppLoadingContainer()
                                        : BlocBuilder<GiftsPacksCubit, GiftsPacksState>(
                                            builder: (context, state) {
                                              if (state.stateStatus == SuperLiveStateStatus.success ||
                                                  state.stateStatus == SuperLiveStateStatus.empty ||
                                                  state.data != null) {
                                                _listPackages = state.data ?? [];
                                                return Stack(
                                                  children: [
                                                    BlocBuilder<GetChatHostCubit, GetChatHostState>(
                                                      builder: (context, settingState) {
                                                        String? _basedUrl;
                                                        String? _maxLength;
                                                        int _setTimeout = 3;
                                                        if (settingState.stateStatus == SuperLiveStateStatus.success || settingState.stateStatus == SuperLiveStateStatus.empty) {
                                                          for (int i = 0; i < (settingState.data?.length ?? 0); i++) {
                                                            if (settingState.data?[i].name == "ChatMe") {
                                                              for (int j = 0; j < (settingState.data?[i].properties?.length ?? 0); j++) {
                                                                if (settingState.data?[i].properties?[j].key == "wsDomain") {
                                                                  _basedUrl = settingState.data?[i].properties?[j].value;
                                                                } else if (settingState.data?[i].properties?[j].key == "maxMsgText") {
                                                                  _maxLength = settingState.data?[i].properties?[j].value ?? "150";
                                                                } else if (settingState.data?[i].properties?[j].key == "msgThreshold") {
                                                                  _setTimeout = int.parse("${settingState.data?[i].properties?[j].value ?? 3}");
                                                                  setTimeoutPerMessage = int.parse("${settingState.data?[i].properties?[j].value ?? 0}");
                                                                } else if (settingState.data?[i].properties?[j].key == "displayGiftedLeaderboard") {
                                                                  displayGiftedLeaderboard = "${settingState.data?[i].properties?[j].value ?? false}" == "true" ? true : false;
                                                                }
                                                              }
                                                            }
                                                          }
                                                          return Scaffold(
                                                            floatingActionButton: SafeArea(
                                                              child: BlocBuilder<WinnerCubit, WinnerState>(
                                                                builder: (context, winnerState) {
                                                                  return BlocBuilder<GetVoteCubit, GetVoteState>(
                                                                    builder: (context, getVoteState) {
                                                                      return DraggableVoteWinnerFloatingWidget(
                                                                        settingState: settingState,
                                                                        isFinishedVote: _isFinishedVote,
                                                                        winnerState: winnerState,
                                                                        getVoteState: getVoteState,
                                                                        callBack: (isFinished) {
                                                                          _isFinishedVote = isFinished;
                                                                          SchedulerBinding.instance.addPostFrameCallback((_) {
                                                                            setState(() {});
                                                                          });
                                                                        },
                                                                        tapToGoToVote: () async {
                                                                          AppHelperWidget.focusNew(context);
                                                                          _cubitHeaderChat.goTo(ChatHeaderStateStatus.vote, step: 2);
                                                                        },
                                                                        tapToGoToWinner: () async {
                                                                          AppHelperWidget.focusNew(context);
                                                                          _cubitHeaderChat.goTo(ChatHeaderStateStatus.winner, step: 2);
                                                                        },
                                                                      );
                                                                    },
                                                                  );
                                                                },
                                                              ),
                                                            ),
                                                            body: Stack(
                                                              key: _parentKey,
                                                              children: [
                                                                Positioned.fill(
                                                                  child: widget.state.data?.isChatEnabled == true
                                                                      ? ChatMeUiSdk(
                                                                          setTimeoutEachMessage: _setTimeout,
                                                                          onSwitchInputType: (isChanged) {
                                                                            setState(() {});
                                                                          },
                                                                          isGuestUser: widget.helper.isGuestUser ?? false,
                                                                          isRequiredLogin: (isRequiredLogin) {
                                                                            if (isRequiredLogin) {
                                                                              checkRequiredLogin(
                                                                                context: context,
                                                                                isGuestUser: widget.helper.isGuestUser ?? false,
                                                                                eventContinue: () {},
                                                                              );
                                                                            }
                                                                          },
                                                                          isBanned: stateRequestChat.data?.isBanned ?? false,
                                                                          baseUrl: _basedUrl,
                                                                          maxLength: _maxLength,
                                                                          roomId: widget.state.data!.chatRoomId,
                                                                          isImplementGift: _listPackages!.isNotEmpty ? true : false,
                                                                          accessToken: stateRequestChat.data!.chatToken,
                                                                          gifts: GiftWidget(
                                                                            data: _listPackages!,
                                                                            onClear: () {
                                                                              _selectedGift = null;
                                                                              setState(() {});
                                                                            },
                                                                            onChoose: (v) {
                                                                              setState(() {
                                                                                _selectedGift = v;
                                                                              });
                                                                            },
                                                                            selectedGift: _selectedGift,
                                                                            onSentGift: (giftAmount) {
                                                                              giftCongrats = _selectedGift;
                                                                              checkRequiredLogin(
                                                                                context: context,
                                                                                isGuestUser: widget.helper.isGuestUser ?? false,
                                                                                eventContinue: () {
                                                                                  numberOfGift = giftAmount;
                                                                                  cubitSendGift.giftAmount = giftAmount;
                                                                                  cubitSendGift.request(_selectedGift?.id);
                                                                                },
                                                                              );
                                                                            },
                                                                          ),
                                                                          onSuccess: (succeedMessage) {
                                                                            debugPrint("On success");
                                                                            debugPrint(succeedMessage.toString());
                                                                          },
                                                                          onError: (errorMessage) {
                                                                            debugPrint("On error");
                                                                            debugPrint(errorMessage.toString());
                                                                          },
                                                                          callBackSocket: (messageService) {
                                                                            //
                                                                          },
                                                                          congrats: const SizedBox(),
                                                                          messageService: _messageService,
                                                                          language: sdkConfig.language,
                                                                        ) : Center(
                                                                          child: Text(
                                                                            trans.chatDisabled,
                                                                            style: TextStyle(
                                                                              color: HexColor(AppColor.disabledColor),
                                                                            ),
                                                                          ),
                                                                        ),
                                                                ),
                                                              ],
                                                            ),
                                                          );
                                                        } else {
                                                          return const AppLoadingContainer();
                                                        }
                                                      },
                                                    ),
                                                    for(int i=0; i< (_listPackages?? []).length; i++)
                                                      for(int j=0; j< (_listPackages?[i].gifts?? []).length; j++)
                                                        if(_listPackages?[i].gifts?[j].mimeType == "json")
                                                          Positioned(
                                                            top: 0,
                                                            left: 0,
                                                            child: Lottie.network("${_listPackages?[i].gifts?[j].animationImage}", repeat: false, width: .1, height: .1),
                                                          ),
                                                  ],
                                                );
                                              } else {
                                                return const AppLoadingContainer();
                                              }
                                            },
                                          ),
                                  );
                                } else {
                                  return const AppLoadingContainer();
                                }
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                    if (leaderBoardData != null)
                      if (leaderBoardData?.data?.isNotEmpty == true)
                        Positioned(
                          top: 0,
                          left: 0,
                          right: 0,
                          bottom: leaderBoardData?.isExpandLeaderBoard == true ? 0 : null,
                          child: PhysicalModel(
                            elevation: 0,
                            color: HexColor(AppColor.whiteColor),
                            child: Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 16),
                              child: AppCollapsible(
                                collapsed: !(leaderBoardData?.isExpandLeaderBoard ?? false),
                                child: ListView.builder(
                                  padding: EdgeInsets.zero,
                                  shrinkWrap: !(leaderBoardData?.isExpandLeaderBoard)!,
                                  physics: const BouncingScrollPhysics(),
                                  itemCount: leaderBoardData?.data!.length,
                                  itemBuilder: (context, ind) {
                                    DatumLeaderboard? _itm = leaderBoardData?.data?[ind];
                                    return SafeArea(
                                      top: false,
                                      bottom: false,
                                      child: Row(
                                        children: [
                                          SizedBox(
                                            width: 20,
                                            height: 45,
                                            child: ind < 3
                                                ? Image.asset(
                                                    "lib/assets/images/medals/$ind.png",
                                                    package: sdkConfig.appPackageName,
                                                  )
                                                : Center(
                                                    child: Text(
                                                      "${ind + 1}",
                                                      style: TextStyle(
                                                        color: HexColor(AppColor.blackColor),
                                                        fontSize: 15,
                                                        fontWeight: FontWeight.w700,
                                                      ),
                                                    ),
                                                  ),
                                          ),
                                          const SizedBox(
                                            width: 12,
                                          ),
                                          Expanded(
                                            child: Text(
                                              "${_itm?.name}",
                                              style: TextStyle(
                                                color: HexColor(AppColor.blackColor),
                                                fontSize: 15,
                                                fontWeight: FontWeight.w500,
                                              ),
                                            ),
                                          ),
                                          Text(
                                            "${_itm?.totalGiftPoint}",
                                            style: TextStyle(
                                              color: HexColor(AppColor.blackColor),
                                              fontSize: 15,
                                              fontWeight: FontWeight.w500,
                                            ),
                                          ),
                                        ],
                                      ),
                                    );
                                  },
                                ),
                              ),
                            ),
                          ),
                        ),
                    ChatCollapsibleWidget(
                      isCollapsed: (stateHead.stateStatus == ChatHeaderStateStatus.vote && stateHead.step == 2) ? false : true,
                      child: VoteBodyWidget(
                        isGuestUser: widget.helper.isGuestUser ?? false,
                        onDelete: (onDelete) {
                          if (onDelete) {
                            _cubitHeaderChat.goTo(ChatHeaderStateStatus.initial);
                          }
                        },
                        hostId: widget.helper.hostId!,
                        participantId: widget.helper.participantsId!,
                        selectedIndex: _selectedIndexVote,
                        onChanged: (v) {
                          _selectedIndexVote = v;
                          SchedulerBinding.instance.addPostFrameCallback((_) {
                            setState(() {});
                          });
                        },
                      ),
                    ),
                    ChatCollapsibleWidget(
                      isCollapsed: (stateHead.stateStatus == ChatHeaderStateStatus.winner && stateHead.step == 2) ? false : true,
                      child: WinnerBodyWidget(
                        hostId: "${widget.helper.hostId}",
                        countParts: countParts,
                      ),
                    ),
                    if (_isShowCongratsScreen && giftCongrats != null)
                      IgnorePointer(
                        ignoring: true,
                        child: SafeArea(
                          top: false,
                          child: Container(
                            alignment: Alignment.center,
                            margin: const EdgeInsets.only(bottom: 70),
                            child: GiftAnimatedWidget(
                              gift: giftCongrats!,
                            ),
                          ),
                        ),
                      ),
                  ],
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  /// Widget more vert option
  Widget optionsWidget() {
    return AppContextMenu(
      children: [
        AppContextMenuModel(
          iconPath: 'lib/assets/svg/top_gift.svg',
          text: trans.topGifters,
          onTap: () {
            _cubitHeaderChat.goTo(ChatHeaderStateStatus.initial);
          },
        ),
        AppContextMenuModel(
          iconPath: 'lib/assets/svg/winner.svg',
          text: trans.luckyDraw,
          onTap: () {
            _cubitHeaderChat.goTo(ChatHeaderStateStatus.winner);
          },
        ),
      ],
    );
  }

  @override
  FutureOr<void> afterFirstLayout(BuildContext context) async {
    /// Init socket
    await socketService.initSocketConnection(
      sdkConfig.apiKey ?? "",
      "${widget.helper.hostId}",
      "${widget.helper.participantsId}",
      onError: (errorMessage) {},
      onSuccess: (successMessage) async {
        //
      },
    );

    /// Event subscribe room
    socketService.subScribeRoom(
      hostId: "${widget.state.data?.id}",
      callBack: (dt) {
        socketService.listenReceiveGift(
          callBack: (data) {
            _topGiftsCubit.request(widget.helper.hostId);
            GiftReceiveOnListenModel __gift = GiftReceiveOnListenModel.fromJson(data);
            giftCongrats = __gift.gift!;
            /// If condition to check if sender or receiver
            // if (_gift.participant?.id != widget.helper.participantsId) {
            SchedulerBinding.instance.addPostFrameCallback((_) {
              setState(() {
                _isShowCongratsScreen = true;
              });
            });
            Future.delayed(const Duration(seconds: 3), () {
              SchedulerBinding.instance.addPostFrameCallback((_) {
                setState(() {
                  _isShowCongratsScreen = false;
                });
              });
            });
          },
        );

        /// Listen event create, delete and end vote
        socketService.listenReceiveNewVote(
          callBack: (data) {
            data as Map;
            getVoteCubit.request(widget.helper.hostId);
            if (data['type'] == "create") {
              appEventBus.fire(VoteEventBus(
                voteEventBusState: VoteEventBusState.update,
              ));
            } else if (data['type'] == "delete" || data['type'] == "end") {
              appEventBus.fire(VoteEventBus(
                voteEventBusState: VoteEventBusState.delete,
              ));
            }
            _isFinishedVote = false;
            SchedulerBinding.instance.addPostFrameCallback((_) {
              setState(() {});
            });
          },
        );

        /// Listen event disabled chat
        socketService.listenReceiveDisableChat(
          callBack: (data) {
            data as Map;
            widget.state.data!.isChatEnabled = data['isEnabled'];
            SchedulerBinding.instance.addPostFrameCallback((_) {
              setState(() {});
            });
          },
        );
        socketService.listenerWinnerCreateLuckyDraw(
          callBack: (v) {
            getWinnerCubit.request();
          },
        );
        socketService.listenerHostResetLuckyDraw(
          callBack: (v) {
            appEventBus.fire(ResetLuckyDrawEventBus());
            getWinnerCubit.request();
          },
        );
        socketService.listenerHostCompleteLuckyDraw(
          callBack: (v) {
            getWinnerCubit.request();
          },
        );
        socketService.giftEventSocket(
          callBack: (v) {
            _cubitPacks.request(widget.helper.hostId);
            _selectedGift = null;
          },
        );
        socketService.luckyDrawParticipants(
          callBack: (v) {
            Map<String, dynamic> data = v;
            setState(() {
              countParts = "${data['totalParticipants']}";
            });
          },
        );
      },
    );
  }

  @override
  void dispose() {
    super.dispose();

    /// Event unsubscribe room
    socketService.unSubScribeRoom(
      roomId: "${widget.state.data?.id}",
      callBack: (data) {},
    );

    /// disposed socket
    _messageService.disposedSocket();
  }
}

// showToastBarHaveNewGiftSent(
//   BuildContext context, {
//   double bottom = 350,
//   String? giftPhotoUrl,
//   String backgroundColor = AppColor.blackColor,
// }) {
//   Flushbar(
//     animationDuration: const Duration(seconds: 5),
//     maxWidth: 150,
//     padding: const EdgeInsets.symmetric(vertical: 6, horizontal: 16),
//     flushbarPosition: FlushbarPosition.BOTTOM,
//     borderRadius: BorderRadius.circular(10),
//     margin: EdgeInsets.only(bottom: bottom),
//     dismissDirection: FlushbarDismissDirection.HORIZONTAL,
//     flushbarStyle: FlushbarStyle.GROUNDED,
//     messageText: giftPhotoUrl != null ? IgnorePointer(
//       ignoring: true,
//       child: Center(
//         child: AnimatedWidgetExample(
//           imageUrl: giftPhotoUrl,
//         ),
//       ),
//     ): const SizedBox(),
//     backgroundColor: HexColor(backgroundColor).withOpacity(.0),
//     duration: const Duration(milliseconds: 1200),
//   ).show(context);
// }
