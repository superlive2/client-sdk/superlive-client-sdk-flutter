import 'package:chat_video_sdk/chat_video_sdk.dart';

class SuperliveVideoPaddingWidget extends StatelessWidget {
  final Widget child;
  const SuperliveVideoPaddingWidget({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double paddingTop = appGetAppBarPadding(context).top;
    return Container(
      padding: const EdgeInsets.only(top: 40),
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Colors.black,
            Colors.white,
          ],
        ),
      ),
      child: child,
    );
  }
}
