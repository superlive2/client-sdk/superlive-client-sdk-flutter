import 'package:cached_network_image/cached_network_image.dart';
import 'package:chat_video_sdk/chat_video_sdk.dart';
import 'package:lottie/lottie.dart';

class GiftAnimatedWidget extends StatefulWidget {
  final GiftResponse gift;
  const GiftAnimatedWidget({super.key, required this.gift});

  @override
  State<GiftAnimatedWidget> createState() => _GiftAnimatedWidgetState();
}

class _GiftAnimatedWidgetState extends State<GiftAnimatedWidget> with TickerProviderStateMixin {

  @override
  Widget build(BuildContext context) {
    switch(widget.gift.mimeType) {
      case('json'):
        return AnimatedSize(
          duration: const Duration(milliseconds: 300),
          curve: Curves.fastOutSlowIn,
          child: Lottie.network("${widget.gift.animationImage}", repeat: false),
        );

      default:
        return AnimatedSize(
          curve: Curves.fastOutSlowIn,
          duration: const Duration(milliseconds: 300),
          child: CachedNetworkImage(
            imageUrl: "${widget.gift.animationImage}",
            progressIndicatorBuilder: (context, url, downloadProgress) => const SizedBox(),
            errorWidget: (context, url, error) => const Icon(Icons.error, color: Colors.grey,),
          ),
        );
    }
  }
}
