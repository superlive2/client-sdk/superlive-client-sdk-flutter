import 'package:chat_video_sdk/chat_video_sdk.dart';
import 'package:chat_video_sdk/data/translate/translate.dart';
import 'package:chat_video_sdk/logic/chat/get_vote_cubit.dart';
import 'package:chatme_ui_sdk/util/app_color.dart';
import 'package:chatme_ui_sdk/util/hex_color.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ChatHeaderVoteWidget extends StatelessWidget {
  final GestureTapCallback? onVoteTap;
  const ChatHeaderVoteWidget({super.key, this.onVoteTap});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GetVoteCubit, GetVoteState>(
      builder: (context, state) {
        if(state.stateStatus == SuperLiveStateStatus.success) {
          return GestureDetector(
            onTap: onVoteTap,
            child: SizedBox(
              height: 50,
              child: Row(
                children: [
                  SvgPicture.asset(
                    'lib/assets/svg/comment.svg',
                    width: 25,
                    fit: BoxFit.contain,
                    package: 'chat_video_sdk',
                    color: HexColor(AppColor.primaryColor),
                  ),
                  const SizedBox(width: 10),
                  Expanded(
                    child: Text(
                      '${state.data!.topic}',
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: appTextTheme(context).bodyLarge?.copyWith(
                        color: HexColor(AppColor.primaryColor),
                      ),
                    ),
                  ),
                  const SizedBox(width: 10),
                  AppGestureDetector(
                    onTap: onVoteTap,
                    child: Text(
                      trans.voteNow,
                      style: appTextTheme(context).bodyLarge?.copyWith(
                        color: HexColor(AppColor.primaryColor),
                      ),
                    ),
                  ),
                  const SizedBox(width: 10),
                ],
              ),
            ),
          );
        } else if(state.stateStatus == SuperLiveStateStatus.empty) {
          return Center(
            child: Text(trans.dontHavePoll, style: const TextStyle(color: Colors.black54),),
          );
        } else if(state.stateStatus == SuperLiveStateStatus.failure) {
          return Center(
            child: Text("${state.message}", style: const TextStyle(color: Colors.black54),),
          );
        }
        return const AppLoadingContainer();
      },
    );
  }
}
