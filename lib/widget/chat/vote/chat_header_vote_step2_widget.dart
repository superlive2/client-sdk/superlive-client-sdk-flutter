import 'package:app_packages/app_package.dart';
import 'package:chat_video_sdk/data/translate/translate.dart';
import 'package:chatme_ui_sdk/util/app_color.dart';
import 'package:chatme_ui_sdk/util/hex_color.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ChatHeaderVoteStep2Widget extends StatelessWidget {
  final GestureTapCallback? onClose;

  const ChatHeaderVoteStep2Widget({super.key, this.onClose});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 50,
      child: Row(
        children: [
          SvgPicture.asset(
            'lib/assets/svg/comment.svg',
            width: 30,
            fit: BoxFit.contain,
            package: 'chat_video_sdk',
            color: HexColor(AppColor.primaryColor),
          ),
          const SizedBox(width: 10),
          Expanded(
            child: Text(
              trans.vote,
              style: appTextTheme(context).titleMedium?.copyWith(
                color: HexColor(AppColor.blackColor),
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
          GestureDetector(
            onTap: onClose,
            child: Icon(Icons.close, color: HexColor(AppColor.blackColor),),
          ),
        ],
      ),
    );
  }
}
