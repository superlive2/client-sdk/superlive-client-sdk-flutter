import 'package:app_packages/app_package.dart';
import 'package:chat_video_sdk/data/model/chat/vote_response_model.dart';
import 'package:chat_video_sdk/helper/constant.dart';
import 'package:chat_video_sdk/logic/chat/get_vote_cubit.dart';
import 'package:chatme_ui_sdk/util/app_color.dart';
import 'package:chatme_ui_sdk/util/hex_color.dart';
import 'package:chat_video_sdk/widget/index.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ChatHeaderVoteStep3Widget extends StatelessWidget {
  final GestureTapCallback? onVoteTap;

  const ChatHeaderVoteStep3Widget({super.key, this.onVoteTap});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GetVoteCubit, GetVoteState>(
      builder: (context, state) {
        if(state.stateStatus == SuperLiveStateStatus.success) {
          VoteResponseModel _data = state.data!;
          return SizedBox(
            height: 50,
            child: GestureDetector(
              onTap: onVoteTap,
              child: Row(
                children: [
                  Expanded(
                    child: _data.options!.isEmpty? const SizedBox() :Row(
                      children: [
                        ProgressWidget(
                          process: int.parse("${_data.options![0].percentage}"),
                        ),
                        const SizedBox(width: 8),
                        Expanded(
                          child: Text(
                            '${_data.options![0].name}',
                            overflow: TextOverflow.ellipsis,
                            style: appTextTheme(context).titleMedium?.copyWith(
                              fontSize: 14,
                              color: HexColor(AppColor.blackColor),
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                        const SizedBox(width: 4),
                      ],
                    ),
                  ),
                  Expanded(
                    child: _data.options!.length < 2? const SizedBox() : Row(
                      children: [
                        ProgressWidget(
                          process: int.parse("${_data.options![1].percentage}"),
                          color: const Color(0XFF2F8BEB),
                        ),
                        const SizedBox(width: 8),
                        Expanded(
                          child: Text(
                            '${_data.options![1].name}',
                            overflow: TextOverflow.ellipsis,
                            style: appTextTheme(context).titleMedium?.copyWith(
                              fontSize: 14,
                              color: HexColor(AppColor.blackColor),
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                        const SizedBox(width: 4),
                      ],
                    ),
                  ),
                  Expanded(
                    child: _data.options!.length < 3? const SizedBox() : Row(
                      children: [
                        ProgressWidget(
                          process: int.parse("${_data.options![2].percentage}"),
                          color: const Color(0XFF00CD2D),
                        ),
                        const SizedBox(width: 8),
                        Expanded(
                          child: Text(
                            '${_data.options![2].name}',
                            overflow: TextOverflow.ellipsis,
                            style: appTextTheme(context).titleMedium?.copyWith(
                              fontSize: 14,
                              color: HexColor(AppColor.blackColor),
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                        const SizedBox(width: 4),
                      ],
                    )
                  ),
                ],
              ),
            ),
          );
        }
        return const AppLoadingContainer();
      },
    );
  }
}
