import 'package:chat_video_sdk/chat_video_sdk.dart';
import 'package:chatme_ui_sdk/util/app_color.dart';
import 'package:chatme_ui_sdk/util/hex_color.dart';
import 'package:timer_count_down/timer_count_down.dart';

class FloatingVoteWidget extends StatelessWidget {
  final Function(bool) onFinished;
  final Widget floatingVoteWidget;
  const FloatingVoteWidget({super.key, required this.onFinished, required this.floatingVoteWidget});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        floatingVoteWidget,
        const SizedBox(
          height: 4,
        ),
        Container(
          padding: const EdgeInsets.symmetric(vertical: 2, horizontal: 8),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(30),
            gradient: LinearGradient(
              begin: Alignment.bottomCenter,
              end: Alignment.topCenter,
              colors: [
                HexColor("#E49F30"),
                HexColor("#DD7E3D"),
              ],
            ),
          ),
          child: Countdown(
            seconds: Duration(microseconds: ((getVoteCubit.state.data?.endAt?.toLocal().microsecondsSinceEpoch??0) - DateTime.now().toLocal().microsecondsSinceEpoch).toInt()).inSeconds,
            build: (BuildContext context, double time) {
              final _duration = Duration(seconds: time.toInt());
              return Text(
                formatDurationInHhMmSs(_duration),
                style: TextStyle(
                  color: HexColor(AppColor.whiteColor),
                  fontWeight: FontWeight.w500,
                  fontSize: 9,
                ),
              );
            },
            interval: const Duration(milliseconds: 100),
            onFinished: () {
              onFinished(true);
            },
          ),
        ),
        const SizedBox(
          height: 85,
        ),
      ],
    );
  }
}
