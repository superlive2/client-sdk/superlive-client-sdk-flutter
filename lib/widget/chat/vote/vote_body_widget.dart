import 'dart:async';
import 'package:chat_video_sdk/chat_video_sdk.dart';
import 'package:chat_video_sdk/data/model/chat/vote_response_model.dart';
import 'package:chat_video_sdk/data/translate/translate.dart';
import 'package:chat_video_sdk/helper/eventbus/vote_event_bus.dart';
import 'package:chat_video_sdk/logic/vote/retract_vote_cubit.dart';
import 'package:chat_video_sdk/logic/vote/submit_vote_cubit.dart';
import 'package:chatme_ui_sdk/util/app_color.dart';
import 'package:chatme_ui_sdk/util/hex_color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../logic/chat/get_vote_cubit.dart';

VoteResponseModel? _voteResponseData;
class VoteBodyWidget extends StatefulWidget {
  final String hostId;
  final String participantId;
  final int? selectedIndex;
  final ValueChanged<int> onChanged;
  final Function(bool) onDelete;
  final bool isGuestUser;
  const VoteBodyWidget({super.key, this.selectedIndex, required this.onChanged, required this.hostId, required this.participantId, required this.onDelete, required this.isGuestUser});

  @override
  State<VoteBodyWidget> createState() => _VoteBodyWidgetState();
}

class _VoteBodyWidgetState extends State<VoteBodyWidget> {
  StreamSubscription? loginSubscription;

  @override
  void initState() {
    // TODO: implement initState
    loginSubscription = appEventBus.on<VoteEventBus>().listen((event) {
      if(event.voteEventBusState == VoteEventBusState.delete) {
        widget.onChanged(-1);
        widget.onDelete(true);
      } else if(event.voteEventBusState == VoteEventBusState.update) {
        getVoteCubit.state.data?.votedOption = null;
        widget.onChanged(-1);
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    widget.onChanged(-1);
    loginSubscription?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    late SubmitVoteCubit _submitVoteCubit;
    _submitVoteCubit = context.read<SubmitVoteCubit>();
    return BlocBuilder<GetVoteCubit, GetVoteState>(
      builder: (context, state) {
        if(state.stateStatus == SuperLiveStateStatus.success || _voteResponseData != null) {
          _voteResponseData = state.data!;
          return SizedBox(
            width: double.infinity,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(height: 10),
                Row(
                  children: [
                    Expanded(
                      child: Text(
                        '${_voteResponseData!.topic}',
                        style: appTextTheme(context).titleMedium?.copyWith(
                          color: HexColor(AppColor.blackColor),
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    if(widget.selectedIndex != -1 && widget.selectedIndex != null)
                      _optionsWidget(context, _voteResponseData!.id!),
                  ],
                ),
                const SizedBox(height: 4),
                Text(
                  '${_voteResponseData!.totalVotes}  ${trans.votes}',
                  style: appTextTheme(context).bodySmall?.copyWith(
                    color: HexColor(AppColor.greyColor).withOpacity(0.6),
                    fontWeight: FontWeight.w600,
                  ),
                ),
                const SizedBox(height: 10),
                Expanded(
                  child: BlocBuilder<SubmitVoteCubit, SubmitVoteState>(
                    builder: (context, state) {
                      return AppListViewBuilder(
                        children: List.generate(
                          (_voteResponseData?.options?.length?? 0), (index) {
                          final isActive = index == widget.selectedIndex;
                          final _option = _voteResponseData?.options?[index];
                          if(_option?.id == _voteResponseData?.votedOption) {
                            SchedulerBinding.instance.addPostFrameCallback((_) {
                              widget.onChanged(index);
                            });
                          }
                          return AppGestureDetector(
                            onTap: () {
                              checkRequiredLogin(
                                context: context,
                                isGuestUser: widget.isGuestUser,
                                eventContinue: () {
                                  if(_voteResponseData?.votedOption == null) {
                                    if(getVoteCubit.state.stateStatus == SuperLiveStateStatus.loading || _submitVoteCubit.state.stateStatus == SuperLiveStateStatus.loading || index == widget.selectedIndex) {
                                      return;
                                    } else {
                                      widget.onChanged(index);
                                      getVoteCubit.state.data?.totalVotes = (getVoteCubit.state.data?.totalVotes?? 0) + 1;
                                      _submitVoteCubit.participantId = widget.participantId;
                                      _submitVoteCubit.pollId = "${_voteResponseData?.id}";
                                      _submitVoteCubit.optionId = "${_option?.id}";
                                      _submitVoteCubit.request(widget.hostId);
                                      return;
                                    }
                                  }
                                },
                              );
                            },
                            child: Container(
                              padding: const EdgeInsets.symmetric(
                                horizontal: 10,
                                vertical: 10,
                              ),
                              decoration: BoxDecoration(
                                color: HexColor(isActive ? AppColor.primaryColor : AppColor.whiteColor),
                                borderRadius: BorderRadius.circular(10),
                                border: isActive ? null : Border.all(
                                  color: HexColor(AppColor.greyColor).withOpacity(0.6),
                                  width: 0.3,
                                ),
                              ),
                              child: Row(
                                children: [
                                  isActive ? const Icon(
                                    Icons.check_circle,
                                    color: Colors.white,
                                  ) : Icon(
                                    Icons.radio_button_unchecked_outlined,
                                    color: HexColor(AppColor.greyColor).withOpacity(0.4),
                                  ),
                                  const SizedBox(width: 10),
                                  Expanded(
                                    child: Text(
                                      '${_option?.name}',
                                      style: appTextTheme(context).titleMedium?.copyWith(
                                        color: HexColor(isActive ? AppColor.whiteColor : AppColor.blackColor),
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                  ),
                                  const SizedBox(width: 10),
                                  state.stateStatus == SuperLiveStateStatus.loading && index == widget.selectedIndex? const Center(
                                    child: CupertinoActivityIndicator(),
                                  ): _voteResponseData?.votedOption != null? Text(
                                    '${(_option?.percentage?.length??0) > 5? _option?.percentage?.substring(0, 5): _option?.percentage}%', // keep left dot 2chars.
                                    style: appTextTheme(context).titleMedium?.copyWith(
                                      color: HexColor(isActive ? AppColor.whiteColor : AppColor.primaryColor),
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ): const SizedBox(),
                                ],
                              ),
                            ),
                          );
                        },
                        ),
                      ).separated((context, index) => const SizedBox(height: 15));
                    },
                  ),
                ),
              ],
            ),
          );
        } else if(state.stateStatus == SuperLiveStateStatus.failure) {
          return Center(
            child: Text("${state.message}", style: const TextStyle(color: Colors.black54),),
          );
        }
        return const AppLoadingContainer();
      },
    );
  }

  Widget _optionsWidget(BuildContext context, String pollId) {
    late RetractViteCubit _retractVotCubit;
    _retractVotCubit = context.read<RetractViteCubit>();
    return BlocBuilder<RetractViteCubit, RetractVoteState>(
      builder: (context, state) {
        return AppContextMenu(
          children: [
            AppContextMenuModel(
              iconPath: 'lib/assets/svg/redo.svg',
              text: trans.retractVote,
              onTap: () {
                if(state.stateStatus == SuperLiveStateStatus.loading) return;
                _retractVotCubit.participantId = widget.participantId;
                _retractVotCubit.pollId = pollId;
                _retractVotCubit.request(widget.hostId);
                getVoteCubit.state.data!.totalVotes = (getVoteCubit.state.data?.totalVotes?? 1) - 1;
                getVoteCubit.state.data!.votedOption = null;
                widget.onChanged(-1);
              },
            ),
          ],
        );
      },
    );
  }
}
