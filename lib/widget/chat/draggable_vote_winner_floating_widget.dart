import 'package:chat_video_sdk/logic/chat/get_chat_host_cubit.dart';
import 'package:chat_video_sdk/widget/chat/vote/floating_vote_widget.dart';
import 'package:chatme_ui_sdk/ui/screen/chatme_sdk.dart';
import '../../chat_video_sdk.dart';
import '../../data/model/chat/get_chat_host_model.dart';
import '../../logic/chat/get_vote_cubit.dart';

class DraggableVoteWinnerFloatingWidget extends StatelessWidget {
  final GestureTapCallback tapToGoToVote;
  final GestureTapCallback tapToGoToWinner;
  final Function(bool) callBack;
  final bool isFinishedVote;
  final WinnerState winnerState;
  final GetVoteState getVoteState;
  final GetChatHostState settingState;
  const DraggableVoteWinnerFloatingWidget({super.key, required this.tapToGoToVote, required this.tapToGoToWinner, required this.callBack, required this.isFinishedVote, required this.winnerState, required this.getVoteState, required this.settingState});

  @override
  Widget build(BuildContext context) {
    print("===========::: Change");
    print("==========:: ${initChatMeSdk.showingType}");
    print("=========${initChatMeSdk.showingType == OpenOptionType.emoji || initChatMeSdk.showingType == OpenOptionType.gift}");
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Builder(
          builder: (context) {
            if(winnerState.stateStatus == SuperLiveStateStatus.success || winnerState.stateStatus == SuperLiveStateStatus.empty) {
              String winnerUrl = _getImagePath(type: _winnerKey, settingState: settingState);
              return winnerState.data != null ? GestureDetector(
                onTap: tapToGoToWinner,
                child: winnerUrl != "" ? _imageBuilder(
                  winnerUrl,
                  _winnerWidgetDefault(),
                ): _winnerWidgetDefault(),
              ): const SizedBox();
            }
            return const SizedBox();
          },
        ),
        const SizedBox(height: 10,),
        Builder(
          builder: (context) {
            if(getVoteState.stateStatus == SuperLiveStateStatus.success) {
              String voteUrl = _getImagePath(type: _voteKey, settingState: settingState);
              return ((getVoteCubit.state.data?.endAt?.toLocal().microsecondsSinceEpoch??0) - DateTime.now().toLocal().microsecondsSinceEpoch) > 0 && !isFinishedVote ? GestureDetector(
                onTap: tapToGoToVote,
                child: Column(
                  children: [
                    FloatingVoteWidget(
                      onFinished: callBack,
                      floatingVoteWidget: voteUrl != "" ? _imageBuilder(
                        voteUrl,
                        _voteWidgetDefault(),
                      ): _voteWidgetDefault(),
                    ),
                    SizedBox(height: (initChatMeSdk.showingType == OpenOptionType.emoji || initChatMeSdk.showingType == OpenOptionType.gift)? 200: 0,)
                  ],
                ),
              ): SizedBox(height: (initChatMeSdk.showingType == OpenOptionType.emoji || initChatMeSdk.showingType == OpenOptionType.gift)? 275: 75,);
            }
            return SizedBox(height: (initChatMeSdk.showingType == OpenOptionType.emoji || initChatMeSdk.showingType == OpenOptionType.gift)? 275: 75,);
          },
        ),
      ],
    );
  }
}

const String _voteKey = "voteIcon";
const  String _winnerKey = "luckyDrawIcon";
const String _hostKey = "ClientSDKSettings";

_getImagePath({required String type, required GetChatHostState settingState}) {
  String _imagePath = "";
  for(int i =0; i < (settingState.data?.length?? 0); i++) {
    GetChatHostModel? _chatHost = settingState.data?[i];
    if(_chatHost?.name == _hostKey) {
      for(int j = 0; j < (_chatHost?.properties?.length?? 0); j++) {
        Property? _prop = _chatHost?.properties?[j];
        if(_prop?.key == type) {
          _imagePath = _prop?.value?? "";
        }
      }
    }
  }
  return _imagePath;
}

const double _imageSize = 50;
Widget _imageBuilder(String url, Widget defaultWidget) {
  return Image.network(
    url,
    width: _imageSize,
    height: _imageSize,
    fit: BoxFit.contain,
    errorBuilder: (context, exception, stackTrace) {
      return defaultWidget;
    },
  );
}

Widget _voteWidgetDefault() {
  return Image.asset(
    "lib/assets/images/floating_vote_icon.png",
    package: sdkConfig.appPackageName,
    width: _imageSize,
    height: _imageSize,
  );
}

Widget _winnerWidgetDefault() {
  return Image.asset(
    "lib/assets/images/floating_winner_icon.png",
    package: sdkConfig.appPackageName,
    width: _imageSize,
    height: _imageSize,
  );
}
