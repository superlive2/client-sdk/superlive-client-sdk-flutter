import 'package:app_packages/app_package.dart';
import 'package:chatme_ui_sdk/util/app_color.dart';
import 'package:chatme_ui_sdk/util/hex_color.dart';

class ChatCollapsibleWidget extends StatelessWidget {
  final double? bottom;
  final Widget child;
  final bool isCollapsed;
  const ChatCollapsibleWidget({super.key, this.bottom, required this.child, required this.isCollapsed});

  @override
  Widget build(BuildContext context) {
    if(isCollapsed){
      return const SizedBox.shrink();
    }
    return Positioned(
      top: 0,
      left: 0,
      right: 0,
      bottom:  bottom ?? 0,
      child: PhysicalModel(
        elevation: 0,
        color: HexColor(AppColor.whiteColor),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: AppCollapsible(
            collapsed: isCollapsed,
            child: child,
          ),
        ),
      ),
    );
  }
}
