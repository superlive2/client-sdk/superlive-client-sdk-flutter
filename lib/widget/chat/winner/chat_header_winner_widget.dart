// import 'package:chat_video_sdk/chat_video_sdk.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
//
// class ChatHeaderWinnerWidget extends StatefulWidget {
//   final String hostId;
//   const ChatHeaderWinnerWidget({super.key, required this.hostId});
//
//   @override
//   State<ChatHeaderWinnerWidget> createState() => _ChatHeaderWinnerWidgetState();
// }
//
// class _ChatHeaderWinnerWidgetState extends State<ChatHeaderWinnerWidget> {
//   late WinnerCubit _cubit;
//
//   @override
//   void initState() {
//     _cubit = context.read<WinnerCubit>();
//     super.initState();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return WinnerBody(cubit: _cubit, context: context, hostId: widget.hostId).buildHead();
//   }
// }
