import 'package:chat_video_sdk/chat_video_sdk.dart';
import 'package:chat_video_sdk/widget/chat/winner/winner_body2_widget.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class WinnerBodyWidget extends StatelessWidget {
  final String hostId;
  final String countParts;

  const WinnerBodyWidget({super.key, required this.hostId, required this.countParts});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<WinnerCubit, WinnerState>(
      builder: (context, state) {
        if(state.stateStatus == SuperLiveStateStatus.success) {
          return state.data?.drawType == "schedule" && state.data?.isCompleted == false ? WinnerBody2Widget(
            endDateAsInt: (state.data?.endAt?.toLocal().microsecondsSinceEpoch?? 0).toInt(),
            title: "${state.data?.title}",
            description: "${state.data?.description}",
            type: "${state.data?.type}",
            gifts: state.data?.gifts?? [],
            keywords: state.data?.keywords ?? [],
            countParts: countParts,
          ): WinnerBody(cubit: getWinnerCubit, context: context, hostId: hostId).buildBody();
        }
        return const AppLoadingContainer();
      },
    );
  }
}
