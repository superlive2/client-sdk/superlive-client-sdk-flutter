import 'package:chat_video_sdk/chat_video_sdk.dart';
import 'package:chat_video_sdk/data/translate/translate.dart';
import 'package:chatme_ui_sdk/util/app_color.dart';
import 'package:chatme_ui_sdk/util/hex_color.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '../../../helper/bloc/base_bloc_builder.dart';

class WinnerBody with BaseBlocBuilder<LuckyDrawsResponse> {
  final WinnerCubit cubit;
  final BuildContext context;
  final String hostId;

  WinnerBody({
    required this.cubit,
    required this.context,
    required this.hostId,
  });

  Widget buildBody() {
    return Container(
      margin: const EdgeInsets.only(top: 15),
      child: BlocBuilder<WinnerCubit, WinnerState>(
        builder: (context, state) {
          return widgetBuilder(context, cubit, state, builder: (BuildContext context, LuckyDrawsResponse data) {
            return (data.winners?? []).isNotEmpty == true? ListView.builder(
              padding: EdgeInsets.zero,
              itemCount: data.winners?.length,
              itemBuilder: (context, ind) {
                return _listItemWidget(
                  ind,
                  data.winners![ind],
                );
              },
            ): Center(
              child: Text(trans.noRecord),
            );
          },);
        },
      ),
    );
  }

  String _getWinnerIcon(int index) {
    switch (index) {
      case 0:
        return 'lib/assets/svg/winner_1.svg';
      case 1:
        return 'lib/assets/svg/winner_2.svg';
      case 2:
        return 'lib/assets/svg/winner_3.svg';
      default:
        return 'lib/assets/svg/winner.svg';
    }
  }

  Widget _listItemWidget(int index, WinnerResponse data) {
    return Row(
      children: [
        SvgPicture.asset(
          _getWinnerIcon(index),
          width: 25,
          package: 'chat_video_sdk',
        ),
        const SizedBox(width: 10),
        Expanded(
          child: Container(
            margin: const EdgeInsets.symmetric(vertical: 10),
            padding: const EdgeInsets.only(bottom: 5, top: 5),
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(
                  color: HexColor(AppColor.blackColor).withOpacity(0.6),
                  width: 0.2,
                ),
              ),
            ),
            child: Text(
              data.name,
              style: appTextTheme(context).titleSmall?.copyWith(
                color: HexColor(AppColor.blackColor),
                fontWeight: FontWeight.w600,
              ),
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
          ),
        ),
        const SizedBox(width: 10),
      ],
    );
  }

  @override
  void request() {
    cubit.request(hostId);
  }
}
