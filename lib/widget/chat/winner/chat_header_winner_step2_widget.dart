import 'package:app_packages/app_package.dart';
import 'package:chat_video_sdk/data/translate/translate.dart';
import 'package:chatme_ui_sdk/util/app_color.dart';
import 'package:chatme_ui_sdk/util/hex_color.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '../../../helper/eventbus/reset_lucky_draw_event_bus.dart';

class ChatHeaderWinnerStep2Widget extends StatefulWidget {
  final GestureTapCallback onClose;
  final Function(bool) requestClose;
  const ChatHeaderWinnerStep2Widget({
    super.key,
    required this.onClose,
    required this.requestClose,
  });

  @override
  State<ChatHeaderWinnerStep2Widget> createState() => _ChatHeaderWinnerStep2WidgetState();
}

class _ChatHeaderWinnerStep2WidgetState extends State<ChatHeaderWinnerStep2Widget> {

  @override
  void initState() {
    appEventBus.on<ResetLuckyDrawEventBus>().listen((event) {
      widget.requestClose(true);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 50,
      child: Row(
        children: [
          SvgPicture.asset(
            'lib/assets/svg/winner_head.svg',
            width: 30,
            fit: BoxFit.contain,
            package: 'chat_video_sdk',
          ),
          const SizedBox(width: 10),
          Expanded(
            child: Text(
              trans.luckyDraw,
              style: appTextTheme(context).titleLarge?.copyWith(
                color: HexColor(AppColor.blackColor),
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
          GestureDetector(
            onTap: widget.onClose,
            child: Icon(Icons.close, color: HexColor(AppColor.blackColor),),
          ),
        ],
      ),
    );
  }
}
