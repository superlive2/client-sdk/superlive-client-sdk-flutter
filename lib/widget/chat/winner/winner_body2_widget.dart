import 'package:chat_video_sdk/chat_video_sdk.dart';
import 'package:chatme_ui_sdk/util/app_color.dart';
import 'package:chatme_ui_sdk/util/hex_color.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:timer_count_down/timer_count_down.dart';
import '../../../data/translate/translate.dart';

class WinnerBody2Widget extends StatelessWidget {
  final int endDateAsInt;
  final String title;
  final String description;
  final String type;
  final List<GiftResponse> gifts;
  final List keywords;
  final String countParts;

  const WinnerBody2Widget({
    super.key,
    required this.endDateAsInt,
    required this.title,
    required this.description,
    required this.type,
    required this.keywords,
    required this.gifts,
    required this.countParts,
  });
  @override
  Widget build(BuildContext context) {
    const _labelStyle = TextStyle(
      fontSize: 8,
      fontWeight: FontWeight.w400,
    );

    Widget _timerDisplay(String text) {
      return Expanded(
        child: Center(
          child: Text(
            text,
            style: TextStyle(
              color: HexColor(AppColor.primaryColor),
              fontWeight: FontWeight.w500,
              fontSize: 40,
            ),
          ),
        ),
      );
    }
    Widget _doublePointDisplay(String text) {
      return Text(
        text,
        style: TextStyle(
          color: HexColor(AppColor.primaryColor),
          fontWeight: FontWeight.w500,
          fontSize: 40,
        ),
      );
    }

    return SizedBox(
      height: MediaQuery.of(context).size.height,
      child: ListView(
        padding: EdgeInsets.zero,
        shrinkWrap: true,
        children: [
          const SizedBox(height: 16,),
          Row(
            children: [
              const Expanded(child: SizedBox()),
              SvgPicture.asset("lib/assets/svg/users.svg", package: sdkConfig.appPackageName,),
              const SizedBox(width: 4,),
              Text(countParts, style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w500,
                color: HexColor(AppColor.primaryColor),
              )),
            ],
          ),
          const SizedBox(height: 16,),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SvgPicture.asset("lib/assets/svg/alarm.svg", package: sdkConfig.appPackageName,),
              const SizedBox(width: 4,),
              Text(
                trans.luckyDrawWillStartIn,
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w500,
                  color: HexColor(AppColor.blackColor).withOpacity(.7),
                ),
              ),
            ],
          ),
          Center(
            child: Column(
              children: [
                Countdown(
                  seconds: Duration(microseconds: (endDateAsInt - DateTime.now().toLocal().microsecondsSinceEpoch).toInt()).inSeconds,
                  build: (BuildContext context, double time) {
                    final _duration = Duration(seconds: time.toInt());
                    return Container(
                      alignment: Alignment.center,
                      width: 230,
                      child: Row(
                        children: [
                          _timerDisplay(formatDurationInHhMmSs(_duration).substring(0, 2)),
                          _doublePointDisplay(":"),
                          _timerDisplay(formatDurationInHhMmSs(_duration).substring(3, 5)),
                          _doublePointDisplay(":"),
                          _timerDisplay(formatDurationInHhMmSs(_duration).substring(6, 8)),
                        ],
                      ),
                    );
                  },
                  interval: const Duration(milliseconds: 100),
                  onFinished: () {
                    //
                  },
                ),
                SizedBox(
                  width: 230,
                  child: Row(
                    children: [
                      Expanded(
                        child: Text(trans.hours, style: _labelStyle, textAlign: TextAlign.center,),
                      ),
                      Expanded(
                        child: Text(trans.minutes, style: _labelStyle, textAlign: TextAlign.center,),
                      ),
                      Expanded(
                        child: Text(trans.seconds, style: _labelStyle, textAlign: TextAlign.center,),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(height: 16,),
          if(type != "gift")
            _keyword(context),
          if(type != "gift")
            const SizedBox(height: 16,),
          if(type == "gift")
            _gift(context),
          if(type == "gift")
            const SizedBox(height: 16,),
          Text(title, style: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.w600,
            color: HexColor(AppColor.blackColor),
          ),),
          const SizedBox(height: 8,),
          Text(
            description,
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: HexColor(AppColor.blackColor).withOpacity(.7),
            ),
          ),
          const SizedBox(height: 16,),
        ],
      ),
    );
  }

  Widget _keyword(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: const EdgeInsets.all(16),
      decoration: BoxDecoration(
        color: HexColor(AppColor.primaryColor).withOpacity(.05),
        borderRadius: BorderRadius.circular(8),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Center(
            child: Text(
              trans.chatWithTheKeyWords_,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w600,
                color: HexColor(AppColor.blackColor),
              ),
            ),
          ),
          const SizedBox(height: 10,),
          SizedBox(
            width: MediaQuery.of(context).size.width,
            child: Wrap(
              alignment: WrapAlignment.center,
              children: [
                for(int i=0; i< keywords.length; i++)
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 2),
                    child: Text(
                      keywords[i],
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w700,
                        color: HexColor(AppColor.primaryColor),
                      ),
                    ),
                  ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _gift(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: const EdgeInsets.all(16),
      decoration: BoxDecoration(
        color: HexColor(AppColor.primaryColor).withOpacity(.05),
        borderRadius: BorderRadius.circular(8),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Center(
            child: Text(
              trans.sendGiftBelow_,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w600,
                color: HexColor(AppColor.blackColor),
              ),
            ),
          ),
          const SizedBox(height: 10,),
          SizedBox(
            width: MediaQuery.of(context).size.width,
            child: Wrap (
              spacing: 12,
              runSpacing: 12,
              alignment: WrapAlignment.center,
              children: [
                for(int i=0; i < gifts.length; i++)
                  ClipRRect(
                    borderRadius: BorderRadius.circular(8.0),
                    child: Container(
                      height: (MediaQuery.of(context).size.width-100)/4,
                      width: (MediaQuery.of(context).size.width-100)/4,
                      decoration: BoxDecoration(
                        color: HexColor(AppColor.whiteColor),
                        boxShadow: [
                          BoxShadow(
                            color: HexColor(AppColor.blackColor),
                            spreadRadius: 2,
                            blurRadius: 3,
                            offset: const Offset(0, 2), // changes position of shadow
                          ),
                        ],
                      ),
                      child: Column(
                        children: [
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(8.0),
                                child: Image.network(
                                  gifts[i].image,
                                  width: MediaQuery.of(context).size.height,
                                  height: MediaQuery.of(context).size.height,
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          ),
                          Container(
                            height: 18,
                            width: MediaQuery.of(context).size.width,
                            decoration: BoxDecoration(
                              color: HexColor(AppColor.primaryColor),
                            ),
                            alignment: Alignment.center,
                            child: Text(gifts[i].name, style: TextStyle(
                              color: HexColor(AppColor.whiteColor),
                              fontWeight: FontWeight.w400,
                              fontSize: 9.25,
                            ),),
                          )
                        ],
                      ),
                    ),
                  ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
