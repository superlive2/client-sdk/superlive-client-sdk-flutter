import 'package:cached_network_image/cached_network_image.dart';
import 'package:chat_video_sdk/chat_video_sdk.dart';
import 'package:chat_video_sdk/data/translate/translate.dart';
import 'package:chatme_ui_sdk/util/app_color.dart';
import 'package:chatme_ui_sdk/util/hex_color.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../helper/eventbus/giftpack_update.dart';

class GiftWidget extends StatefulWidget {
  final List<PackResponse>? data;
  final GiftResponse? selectedGift;
  final GestureTapCallback onClear;
  final ValueChanged<GiftResponse> onChoose;
  final Function(String) onSentGift;
  bool isSecondScreen;

  GiftWidget({
    super.key,
    required this.data,
    required this.selectedGift,
    required this.onClear,
    required this.onChoose,
    required this.onSentGift,
    this.isSecondScreen = false,
  });

  @override
  State<GiftWidget> createState() => _GiftWidgetState();
}

class _GiftWidgetState extends State<GiftWidget> with SingleTickerProviderStateMixin{
  late TabController _tabController;
  final TextEditingController _giftAmountCtr = TextEditingController(text: "1");
  @override
  void initState() {
    _tabController = TabController(vsync: this, length: widget.data == null ? 0 : widget.data!.length);
    _tabController.addListener(_handleTabSelection);
    super.initState();
  }

  _handleTabSelection() {
    AppHelperWidget.focusNew(context);
    widget.onClear();
  }
  @override
  Widget build(BuildContext context) {
    if (widget.data == null ? true : (widget.data?? []).isEmpty) {
      return const SizedBox();
    }
    if(widget.isSecondScreen) {
      return BlocProvider(
        create: (context) => SendGiftCubit(),
        child: _child(),
      );
    }
    return _child();
  }

  Widget _child() {
    return DefaultTabController(
      length: (widget.data?? []).length,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if((widget.data?? []).length > 1)
            TabBar(
              isScrollable: true,
              tabs: List.from(widget.data!.map((e) => _giftGroupTabItem(e.image?? '${e.gifts?[0].image}'))),
            ),
          if((widget.data?? []).length > 1)
            const SizedBox(
              height: 8,
            ),
          Expanded(
            child: TabBarView(
              children: List.from((widget.data?? []).map((e) => _gifts(e.gifts?? []))),
            ),
          ),
        ],
      ),
    );
  }

  _gifts(List<GiftResponse>? data) {
    return AppGestureDetector(
      onTap: () {
        widget.onClear();
      },
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        width: MediaQuery.of(context).size.width,
        child: data != null ? Column(
          children: [
            Expanded(
              child: GridView.count(
                crossAxisCount: 4,
                crossAxisSpacing: 8,
                padding: EdgeInsets.zero,
                children: List.generate((data.length), (index) {
                  final item = data[index];
                  final isSelected = widget.selectedGift?.id == item.id;
                  return GestureDetector(
                    onTap: () async {
                      if (isSelected) {
                        widget.onClear();
                      } else {
                        _giftAmountCtr.value = const TextEditingValue(text: "1");
                        widget.onChoose(item);
                      }
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(4),
                        color: isSelected ? HexColor(AppColor.greyColor).withOpacity(0.1) : null,
                      ),
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height,
                      child: AnimatedPadding(
                        duration: const Duration(milliseconds: 200),
                        padding: EdgeInsets.all(isSelected ? 8 : 0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Expanded(
                              child: CachedNetworkImage(
                                imageUrl: data[index].image,
                                progressIndicatorBuilder: (context, url, downloadProgress) => const AppLoadingContainer(),
                                errorWidget: (context, url, error) => const Icon(Icons.error),
                              ),
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            Text(
                              data[index].point,
                              style: TextStyle(
                                color: HexColor(AppColor.primaryColor),
                                fontWeight: FontWeight.w700,
                                fontSize: 12,
                              ),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                            ),
                            const SizedBox(
                              height: 3,
                            ),
                            Text(
                              item.name,
                              style: TextStyle(
                                color: HexColor(AppColor.greyColor),
                                fontWeight: FontWeight.w500,
                                fontSize: 10,
                              ),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                }),
              ),
            ),
            if (widget.selectedGift != null)
              BlocBuilder<SendGiftCubit, SendGiftState>(
                builder: (context, state) {
                  return Row(
                    children: [
                      SizedBox(
                        width: 130,
                        child: Row(
                          children: [
                            Container(
                              height: 40,
                              width: 40,
                              decoration: BoxDecoration(
                                color: HexColor(AppColor.greyColor),
                                shape: BoxShape.circle,
                              ),
                              child: Center(
                                child: IconButton(
                                  icon: Icon(Icons.remove, color: HexColor(AppColor.whiteColor),),
                                  onPressed: () {
                                    if(int.parse(_giftAmountCtr.text) <= 1) {
                                      _giftAmountCtr.value = const TextEditingValue(text: "1");
                                    } else {
                                      int _counter = int.parse(_giftAmountCtr.text);
                                      _counter--;
                                      _giftAmountCtr.value = TextEditingValue(text: "$_counter");
                                    }
                                  },
                                ),
                              ),
                            ),
                            Expanded(
                              child: Center(
                                child: TextFormField(
                                  readOnly: true,
                                  controller: _giftAmountCtr,
                                  textAlign: TextAlign.center,
                                  keyboardType: TextInputType.number,
                                  decoration: const InputDecoration.collapsed(
                                    hintText: "",
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              height: 40,
                              width: 40,
                              decoration: BoxDecoration(
                                color: HexColor(AppColor.primaryColor),
                                shape: BoxShape.circle,
                              ),
                              child: Center(
                                child: IconButton(
                                  icon: Icon(Icons.add, color: HexColor(AppColor.whiteColor),),
                                  onPressed: () {
                                    int _counter = int.parse(_giftAmountCtr.text);
                                    if(_counter < 99) {
                                      _counter++;
                                      _giftAmountCtr.value = TextEditingValue(text: "$_counter");
                                    }
                                  },
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(width: 25,),
                      Expanded(
                        child: GestureDetector(
                          onTap: () async {
                            if(state.stateStatus == SuperLiveStateStatus.loading) return;
                            widget.onSentGift(
                              _giftAmountCtr.text,
                            );
                            _giftAmountCtr.value = const TextEditingValue(text: "1");
                          },
                          child: Container(
                            height: 40,
                            decoration: BoxDecoration(
                              color: HexColor(AppColor.primaryColor),
                              borderRadius: BorderRadius.circular(8),
                            ),
                            child: Center(
                              child: (state.stateStatus == SuperLiveStateStatus.loading)? const AppLoadingContainer(): Text(
                                trans.sendGift,
                                style: TextStyle(fontWeight: FontWeight.w500, color: HexColor(AppColor.whiteColor)),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  );
                },
              ),
            const SizedBox(
              height: 10,
            ),
          ],
        ): const SizedBox(),
      ),
    );
  }

  _giftGroupTabItem(String imageUrl) {
    return SizedBox(
      width: 40,
      child: Padding(
        padding: const EdgeInsets.all(4.0),
        child: CachedNetworkImage(
          imageUrl: imageUrl,
          progressIndicatorBuilder: (context, url, downloadProgress) => const AppLoadingContainer(),
          errorWidget: (context, url, error) => const Icon(Icons.error),
        ),
      ),
    );
  }
}
