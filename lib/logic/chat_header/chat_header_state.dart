part of 'chat_header_cubit.dart';

enum ChatHeaderStateStatus { initial, winner, vote }

class ChatHeaderState {
  final ChatHeaderStateStatus stateStatus;
  final int step;

  const ChatHeaderState({
    this.stateStatus = ChatHeaderStateStatus.initial,
    this.step = 1,
  });

  ChatHeaderState copyWith({
    ChatHeaderStateStatus? stateStatus,
    bool? isList,
    int? step,
  }) {
    return ChatHeaderState(
      stateStatus: stateStatus ?? this.stateStatus,
      step: step ?? this.step,
    );
  }
}
