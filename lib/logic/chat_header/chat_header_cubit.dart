import 'package:bloc/bloc.dart';

part 'chat_header_state.dart';

class ChatHeaderCubit extends Cubit<ChatHeaderState> {
  ChatHeaderCubit() : super(const ChatHeaderState());

  void goTo(ChatHeaderStateStatus stateStatus, {int step = 1}){
    emit(state.copyWith(stateStatus: stateStatus, step: step));
  }
  void goToStep(int step){
    emit(state.copyWith(step: step));
  }
}
