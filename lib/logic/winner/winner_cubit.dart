import 'package:chat_video_sdk/chat_video_sdk.dart';
import 'package:chat_video_sdk/data/socket_service/socket_service.dart';
import 'package:flutter/foundation.dart';

/// winner_cubit
typedef WinnerState = AppBaseBlocState<LuckyDrawsResponse>;

class WinnerCubit extends AppBaseBlocCubit<LuckyDrawsResponse> {
  // final SocketService _socketService;
  final AppServices _service = AppServices();
  String hostId = '';

  // WinnerCubit(this._socketService) : super() {
  //   _socketService.listenerWinnerCreateLuckyDraw(
  //     callBack: (v) {
  //       print("====================::::1");
  //       request();
  //     },
  //   );
  //   _socketService.listenerHostResetLuckyDraw(
  //     callBack: (v) {
  //       print("====================::::2");
  //       request();
  //     },
  //   );
  // }

  @override
  BaseResponseEither<LuckyDrawsResponse> responseData([String? id]) async {
    final response = await _service.getWinner(hostId);
    return response;
  }
}
