import '../../chat_video_sdk.dart';

typedef SendGiftState = AppBaseBlocState<DefaultRespond>;

class SendGiftCubit extends AppBaseBlocCubit<DefaultRespond> {
  SendGiftCubit() : super();

  final AppServices appService = AppServices();
  String participantId = '';
  String hostId = '';
  String giftAmount = '1';

  @override
  BaseResponseEither<DefaultRespond> responseData([String? id]) async {
    final data = await appService.sendGift(hostId: hostId, participantId: participantId, giftId: id?? '', giftAmount: giftAmount,);
    return data;
  }
}
