import '../../chat_video_sdk.dart';

typedef GiftsPacksState = AppBaseListBlocState<PackResponse>;

class GiftsPacksCubit extends AppBaseListBlocCubit<PackResponse> {
  GiftsPacksCubit() : super();

  final AppServices appService = AppServices();

  @override
  BaseResponseEither<List<PackResponse>> responseData([String? id]) async {
    var data = await appService.getGiftsPacks(hostId: id!);
    return data;
  }
}
