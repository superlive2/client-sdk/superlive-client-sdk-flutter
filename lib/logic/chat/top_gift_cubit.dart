import '../../chat_video_sdk.dart';
import '../../data/model/home/leaderboard_model.dart';

typedef TopGiftsState = AppBaseBlocState<LeaderBoardModel>;

class TopGiftsCubit extends AppBaseBlocCubit<LeaderBoardModel> {
  TopGiftsCubit() : super();

  final AppServices appService = AppServices();

  @override
  BaseResponseEither<LeaderBoardModel> responseData([String? id]) async {
    final data = await appService.getTopGifts(hostId: id!);
    return data;
  }
}
