// /// request_chat_cubit
// import 'package:flutter/foundation.dart';
// import '../../chat_video_sdk.dart';
//
// typedef RequestChatState = AppBaseBlocState<RequestJoinChatResponse>;
//
// class RequestChatCubit extends AppBaseBlocCubit<RequestJoinChatResponse> {
//   RequestChatCubit() : super();
//
//   final AppServices appService = AppServices();
//   var participantId = '';
//
//   /// just initial current view and total view for display on live steam.
//   String currentView = '0';
//   String totalView = '0';
//
//   @override
//   BaseResponseEither<RequestJoinChatResponse> responseData([String? id]) async {
//     final data = await appService.requestJoinChat(hostId: id!, participantId: participantId);
//     return data;
//   }
// }