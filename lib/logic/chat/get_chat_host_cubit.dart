import '../../chat_video_sdk.dart';
import '../../data/model/chat/get_chat_host_model.dart';

typedef GetChatHostState = AppBaseListBlocState<GetChatHostModel>;

class GetChatHostCubit extends AppBaseListBlocCubit<GetChatHostModel> {
  GetChatHostCubit() : super();

  final AppServices appService = AppServices();

  @override
  BaseResponseEither<List<GetChatHostModel>> responseData([String? id]) async {
    final data = await appService.getChatHost();
    return data;
  }
}
