import 'package:chat_video_sdk/data/model/chat/vote_response_model.dart';
import '../../chat_video_sdk.dart';

typedef GetVoteState = AppBaseBlocState<VoteResponseModel>;

class GetVoteCubit extends AppBaseBlocCubit<VoteResponseModel> {
  GetVoteCubit() : super();

  final AppServices appService = AppServices();
  String participantId = '';

  @override
  BaseResponseEither<VoteResponseModel> responseData([String? id]) async {
    final data = await appService.getVote(hostId: id, participantId: participantId);
    return data;
  }
}
