import '../../chat_video_sdk.dart';

typedef RequestJoinHostState = AppBaseBlocState<RequestJoinChatResponse>;

class RequestJoinHostCubit extends AppBaseBlocCubit<RequestJoinChatResponse> {
  RequestJoinHostCubit() : super();

  final AppServices appService = AppServices();
  var participantId = '';

  @override
  BaseResponseEither<RequestJoinChatResponse> responseData([String? id]) async {
    final data = await appService.requestJoinHost(hostId: id!, participantId: participantId);
    return data;
  }
}
