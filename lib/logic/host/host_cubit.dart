import 'package:flutter/foundation.dart';

import '../../chat_video_sdk.dart';

/// host_cubit

typedef HostState = AppBaseBlocState<HostResponse>;

class HostCubit extends AppBaseBlocCubit<HostResponse> {
  HostCubit() : super();
  final AppServices appService = AppServices();

  @override
  BaseResponseEither<HostResponse> responseData([String? id])async {
    final data = await appService.getHost(id: id!);
    if (kDebugMode) {
      // print(data);
    }
    return data;
  }

  
}