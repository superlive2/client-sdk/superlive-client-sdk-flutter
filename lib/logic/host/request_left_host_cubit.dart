import 'package:flutter/foundation.dart';
import '../../chat_video_sdk.dart';

typedef RequestLeftHostState = AppBaseBlocState<DefaultRespond>;

class RequestLeftHostCubit extends AppBaseBlocCubit<DefaultRespond> {
  RequestLeftHostCubit() : super();

  final AppServices appService = AppServices();
  var participantId = '';

  @override
  BaseResponseEither<DefaultRespond> responseData([String? id]) async {
    final data = await appService.requestLeftHost(hostId: id!, participantId: participantId);
    if (kDebugMode) {
      print(data);
    }
    return data;
  }
}
