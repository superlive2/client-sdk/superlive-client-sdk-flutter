import 'package:flutter/foundation.dart';
import '../../chat_video_sdk.dart';
import '../../data/model/host/view_count_model.dart';

typedef ViewCountState = AppBaseBlocState<ViewCountModel>;

class ViewCountCubit extends AppBaseBlocCubit<ViewCountModel> {
  ViewCountCubit() : super();
  final AppServices appService = AppServices();
  @override
  BaseResponseEither<ViewCountModel> responseData([String? id]) async {
    final data = await appService.getViewCount(hostId: id!);
    if (kDebugMode) {
      print(data);
    }
    return data;
  }
}
