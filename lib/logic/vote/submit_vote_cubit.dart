import '../../chat_video_sdk.dart';

typedef SubmitVoteState = AppBaseBlocState<DefaultRespond>;

class SubmitVoteCubit extends AppBaseBlocCubit<DefaultRespond> {
  SubmitVoteCubit() : super();

  final AppServices appService = AppServices();
  String participantId = '';
  String pollId = '';
  String optionId = '';

  @override
  BaseResponseEither<DefaultRespond> responseData([String? id]) async {
    final data = await appService.submitVote(hostId: id!, participantId: participantId, pollId: pollId, optionId: optionId);
    return data;
  }
}