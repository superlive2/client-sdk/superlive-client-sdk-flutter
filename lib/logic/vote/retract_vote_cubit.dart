import '../../chat_video_sdk.dart';

typedef RetractVoteState = AppBaseBlocState<DefaultRespond>;

class RetractViteCubit extends AppBaseBlocCubit<DefaultRespond> {
  RetractViteCubit() : super();
  final AppServices appService = AppServices();
  String participantId = '';
  String pollId = '';

  @override
  BaseResponseEither<DefaultRespond> responseData([String? id]) async {
    final data = await appService.retractVote(hostId: id!, participantId: participantId, pollId: pollId);
    return data;
  }
}