// import 'dart:async';
// import 'dart:io';
// import 'dart:math';
//
// import 'package:event_bus/event_bus.dart';
// import 'package:http/http.dart' as http;
// import 'package:path_provider/path_provider.dart';
//
// import '../data/model/host/play_option_response.dart';
//
// class InternetSpeedHelper {
//   static int speed = 0;
//   static Timer? timer;
//   static EventBus dynamicVideoUrlEvent = EventBus();
//   static int tempTime = 0;
//
//   static Future<int> getDownloadSpeed() async {
//     var url = 'http://ipv4.download.thinkbroadband.com/1MB.zip'; // URL of a file to download
//     var stopwatch = Stopwatch()..start();
//
//     var response = await http.get(Uri.parse(url));
//
//     if (response.statusCode == 200) {
//       var dir = await getTemporaryDirectory();
//       var file = File('${dir.path}/temp.zip');
//       await file.writeAsBytes(response.bodyBytes);
//
//       var downloadTime = stopwatch.elapsedMilliseconds;
//       var fileSizeInMB = response.bodyBytes.length / 1024 / 1024;
//       var downloadSpeed = fileSizeInMB / (downloadTime / 1000);
//       speed = (downloadSpeed * 8).round();
//       return speed; // Returns download speed in Mbps as an integer
//     } else {
//       throw Exception('Error downloading file');
//     }
//   }
//
//   static void startCheckingInternetSpeed(List<String> videoUrls) {
//     List<String> urls = [];
//     //todo get link of video from state
//     // List<PlayOptionResponse> videos = state.data!.playOptions!.ws ?? [];
//     // for (var i in videos) {
//     //   urls.addAll(i.urls);
//     // }
//     videoQualityByInternetSpeed(urls);
//     timer = Timer.periodic(const Duration(seconds: 10), (Timer t) async {
//       getDownloadSpeed();
//       videoQualityByInternetSpeed(urls);
//     });
//   }
//
//   static void videoQualityByInternetSpeed(List<String> videoUrls) {
//     //test url
//
//     var urlA = 'https://flv.art8848.cn/v3/live/ba8b691d387cff8ed82a44d1789f62cd_low';
//     var urlB = 'https://flv.art8848.cn/v3/live/ba8b691d387cff8ed82a44d1789f62cd_medium';
//     var urlC = 'https://flv.art8848.cn/v3/live/ba8b691d387cff8ed82a44d1789f62cd_high';
//
//     //testing with random number 0, 1, 2
//     var speed = Random().nextInt(3);
//     if (tempTime == speed) {
//       print('new url stay the same');
//       return;
//     } // no need update url when speed stay the same
//     tempTime = speed;
//     if (speed == 0) {
//       dynamicVideoUrlEvent.fire(urlA); // Returns Full HD for speeds >= 20 Mbps
//     } else if (speed == 1) {
//       dynamicVideoUrlEvent.fire(urlB); // Returns HD for speeds >= 10 Mbps
//     } else if (speed == 2) {
//       dynamicVideoUrlEvent.fire(urlC); // Returns SD for speeds >=5 Mbps
//     } else {
//       dynamicVideoUrlEvent.fire(urlB);
//     }
//   }
//
//   static void stopCheckingInternetSpeed() {
//     if (timer != null) {
//       timer!.cancel();
//     }
//   }
// }
