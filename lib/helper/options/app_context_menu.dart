import 'package:chat_video_sdk/chat_video_sdk.dart';
import 'package:chatme_ui_sdk/util/app_color.dart';
import 'package:chatme_ui_sdk/util/hex_color.dart';
import 'package:custom_pop_up_menu/custom_pop_up_menu.dart';
import 'package:flutter_svg/flutter_svg.dart';

class AppContextMenuModel {
  final String iconPath;
  final String text;
  final GestureTapCallback? onTap;

  const AppContextMenuModel({
    required this.iconPath,
    required this.text,
    this.onTap,
  });
}

class AppContextMenu extends StatefulWidget {
  final List<AppContextMenuModel> children;
  final bool isColorWhite;

  const AppContextMenu({
    Key? key,
    required this.children,
    this.isColorWhite = false,
  }) : super(key: key);

  @override
  State<AppContextMenu> createState() => _AppContextMenuState();
}

class _AppContextMenuState extends State<AppContextMenu> {
  late CustomPopupMenuController contextMenuController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    contextMenuController = CustomPopupMenuController();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    contextMenuController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return CustomPopupMenu(
      controller: contextMenuController,
      arrowColor: Colors.white,
      arrowSize: 30,
      menuBuilder: () => _showMoreContextMenu(),
      barrierColor: Colors.transparent,
      pressType: PressType.longPress,
      verticalMargin: -20,
      horizontalMargin: 5,
      child: AppGestureDetector(
        onTap: () {
          contextMenuController.toggleMenu();
        },
        child: Icon(
          Icons.more_vert_outlined,
          size: 20,
          color: HexColor(AppColor.blackColor),
        ),
      ),
    );
  }

  Widget _showMoreContextMenu() {
    return Container(
      width: 170,
      padding: const EdgeInsets.all(16),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: const Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: List.from(
          widget.children.map(
            (e) => InkWell(
              onTap: () {
                contextMenuController.hideMenu();
                if (e.onTap != null) {
                  e.onTap!();
                }
              },
              child: Row(
                children: [
                  SvgPicture.asset(
                    e.iconPath,
                    package: 'chat_video_sdk',
                  ),
                  const SizedBox(width: 8),
                  Text(
                    e.text,
                    style: appTextTheme(context).bodyLarge?.copyWith(
                          color: Colors.black,
                        ),
                  )
                ],
              ),
            ),
          ),
        ).separator((index) => const SizedBox(height: 10)),
      ),
    );
  }
}
