import 'package:chat_video_sdk/chat_video_sdk.dart';

abstract class BaseListBlocBuilder<T extends SerializableApp> {
  @required
  Widget listItemWidget(int index, T data);

  @required
  void request();

  List<Widget> listItemBuilder(
    List<T> data,
    double marginTop,
  ) {
    return List.generate(
      data.length,
      (index) => Container(
        margin: EdgeInsets.only(top: index == 0 ? marginTop : 0),
        child: listItemWidget(index, data[index]),
      ),
    );
  }

  Widget listBuilder(
    BuildContext context,
    AppBaseListBlocCubit<T> cubit,
    AppBaseListBlocState<T> state, {
    double marginTop = 0.0,
    bool hasRefreshIndicator = true,
    bool hasOverLoading = true,
    IndexedWidgetBuilder? separatorBuilder,
  }) {
    final build = AppLoadingOverWidget(
      isShowLoading: cubit.isLoadingOver && hasOverLoading,
      child: Builder(
        builder: (context) {
          if (cubit.isLoading) {
            return const AppLoadingContainer();
          }
          List<Widget> listWidget = [];
          if (!cubit.isDataEmpty) {
            final builder = listItemBuilder(state.data!, marginTop);
            listWidget.addAll(builder);
          } else {
            return errorPage(
              status: state.stateStatus,
              title: state.message,
              onTap: () {
                request();
              },
            );
          }
          listWidget.add(SizedBox(height: MediaQuery.of(context).padding.bottom + 15));
          return AppListViewBuilder(
            children: listWidget,
          ).separated(separatorBuilder ?? (context, index) => const SizedBox(height: 15));
        },
      ),
    );
    return hasRefreshIndicator
        ? RefreshIndicator(
            onRefresh: () async {
              request();
            },
            child: build,
          )
        : build;
  }

  Widget widgetBuilder(
    BuildContext context,
    AppBaseListBlocCubit<T> cubit,
    AppBaseListBlocState<T> state, {
    required Widget Function(BuildContext context, List<T> data) builder,
    bool isPage = true,
    bool isWithScaffold = false,
  }) {
    final _builder = Builder(
      builder: (context) {
        if (cubit.isLoading) {
          if (isPage) {
            return const AppLoadingContainer();
          } else {
            return Column(
              children: const [
                SizedBox(height: 15),
                AppLoadingContainer(),
              ],
            );
          }
        }
        if (!cubit.isDataEmpty) {
          return builder(context, state.data!);
        } else {
          if (isPage) {
            return errorPage(
              status: state.stateStatus,
              title: state.message,
              onTap: () {
                request();
              },
            );
          } else {
            return Column(
              children: [
                const SizedBox(height: 7),
                errorPage(
                  status: state.stateStatus,
                  message: state.message,
                  onTap: () {
                    request();
                  },
                ),
              ],
            );
          }
        }
      },
    );
    return isWithScaffold && !cubit.isSuccess
        ? Scaffold(
            appBar: AppBar(
              elevation: 0.0,
            ),
            body: _builder,
          )
        : _builder;
  }

  Widget errorPage({
    required SuperLiveStateStatus status,
    String? title,
    String? message,
    required GestureTapCallback onTap,
  });
}
