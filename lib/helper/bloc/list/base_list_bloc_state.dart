import 'package:chat_video_sdk/chat_video_sdk.dart';

class AppBaseListBlocState<T> {
  final SuperLiveStateStatus stateStatus;
  final String? message;
  final List<T>? data;
  final String? url;
  final String? id;
  final bool isShowBarNoInternet;

  AppBaseListBlocState({
    this.stateStatus = SuperLiveStateStatus.none,
    this.message,
    this.data,
    this.url,
    this.id,
    this.isShowBarNoInternet = false,
  });

  AppBaseListBlocState<T> copyWith({
    bool? nextUrl,
    SuperLiveStateStatus? stateStatus,
    String? message,
    List<T>? data,
    String? url,
    String? id,
    bool? isShowBarNoInternet,
  }) {
    return AppBaseListBlocState<T>(
      stateStatus: stateStatus ?? this.stateStatus,
      message: message ?? this.message,
      data: data ?? this.data,
      url: url ?? this.url,
      id: id ?? this.id,
      isShowBarNoInternet: isShowBarNoInternet ?? this.isShowBarNoInternet,
    );
  }
}
