import 'dart:async';

import 'package:chat_video_sdk/chat_video_sdk.dart';
import 'package:chat_video_sdk/data/translate/translate.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

export 'base_list_bloc_state.dart';
export 'base_list_bloc_builder.dart';

abstract class AppBaseListBlocCubit<T extends SerializableApp> extends Cubit<AppBaseListBlocState<T>> {
  final Connectivity _connectivity = AppConnectivity.instance;
  late StreamSubscription<AppEventBusConnectivity> _eventBusConnectivity;

  AppBaseListBlocCubit() : super(AppBaseListBlocState<T>()) {
    if (!kDebugMode) {
      _eventBusConnectivity = appEventBus.on<AppEventBusConnectivity>().listen(_onConnectionChanged);
    }
  }

  bool get isDataEmpty => state.data == null ? true : state.data!.isEmpty;

  bool get isLoading => (state.stateStatus == SuperLiveStateStatus.loading || state.stateStatus == SuperLiveStateStatus.none) && isDataEmpty;

  bool get isLoadingOver => state.stateStatus == SuperLiveStateStatus.loading && !isDataEmpty;

  bool get isShowBarNoInternet => state.isShowBarNoInternet && !isDataEmpty;

  bool get isSuccess => state.stateStatus == SuperLiveStateStatus.success;

  @required
  BaseResponseEither<List<T>?> responseData([String? id]);

  void _onConnectionChanged(AppEventBusConnectivity event) {
    if (!event.isConnected) {
      emit(state.copyWith(isShowBarNoInternet: true));
    } else {
      emit(state.copyWith(isShowBarNoInternet: false));
    }
  }

  void request([String? id]) async {
    if (state.stateStatus == SuperLiveStateStatus.loading) return;
    final _checkConnectivity = await checkConnectivity();
    if (!_checkConnectivity) {
      return;
    }
    emit(
      state.copyWith(
        stateStatus: SuperLiveStateStatus.loading,
      ),
    );
    final response = await responseData(id);
    response.fold((l) {
      emit(
        state.copyWith(
          stateStatus: SuperLiveStateStatus.failure,
          message: l.toString(),
        ),
      );
    }, (r) {
      if (r == null) {
        emit(
          state.copyWith(
            stateStatus: SuperLiveStateStatus.failure,
          ),
        );
      } else if(r.isEmpty) {
        print("=============+$id");
        print("==================::21212empty");
        emit(
          state.copyWith(
            stateStatus: SuperLiveStateStatus.empty,
            data: r,
          ),
        );
      } else {
        emit(
          state.copyWith(
            stateStatus: SuperLiveStateStatus.success,
            data: r,
          ),
        );
      }
    });
  }

  Future<bool> checkConnectivity() async {
    if (!kDebugMode) {
      final connectivityResult = await _connectivity.checkConnectivity();
      if (connectivityResult == ConnectivityResult.none) {
        emit(state.copyWith(stateStatus: SuperLiveStateStatus.noInternet, message: trans.noInternetConnection));
        return false;
      }
    }
    return true;
  }

  void reset() {
    emit(AppBaseListBlocState<T>());
  }

  @override
  Future<void> close() {
    if (!kDebugMode) {
      _eventBusConnectivity.cancel();
    }
    return super.close();
  }
}
