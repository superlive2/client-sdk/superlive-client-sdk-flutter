import 'package:chat_video_sdk/chat_video_sdk.dart';
import 'package:chat_video_sdk/data/translate/translate.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

export 'base_bloc_state.dart';
export 'list/base_list_bloc_cubit.dart';

abstract class AppBaseBlocCubit<T extends SerializableApp> extends Cubit<AppBaseBlocState<T>> {
  final Connectivity _connectivity = AppConnectivity.instance;

  AppBaseBlocCubit() : super(AppBaseBlocState<T>());

  bool get isLoading =>
      state.stateStatus == SuperLiveStateStatus.loading ||
      state.stateStatus == SuperLiveStateStatus.initial ||
      state.stateStatus == SuperLiveStateStatus.none;

  bool get isError => state.stateStatus == SuperLiveStateStatus.failure || state.stateStatus == SuperLiveStateStatus.noInternet;

  bool get isSuccess => state.stateStatus == SuperLiveStateStatus.success;

  @required
  BaseResponseEither<T> responseData([String? id]);

  void request([String? id]) async {
    if (state.stateStatus == SuperLiveStateStatus.loading) return;
    final _checkConnectivity = await checkConnectivity();
    if (!_checkConnectivity) {
      return;
    }
    emit(
      state.copyWith(
        stateStatus: SuperLiveStateStatus.loading,
      ),
    );
    final response = await responseData(id);
    response.fold((l) {
      if (l.contains('<<errorCode>>')) {
        emit(
          state.copyWith(
            stateStatus: SuperLiveStateStatus.failure,
            message: l.toString().split("<<errorCode>>")[0],
            error: l.toString().split("<<errorCode>>")[1],
          ),
        );
      } else {
        emit(
          state.copyWith(
            stateStatus: SuperLiveStateStatus.failure,
            message: l,
            error: l,
          ),
        );
      }
    }, (r) {
      emit(
        state.copyWith(
          stateStatus: SuperLiveStateStatus.success,
          data: r,
        ),
      );
    });
  }

  Future<bool> checkConnectivity() async {
    if (!kDebugMode) {
      final connectivityResult = await _connectivity.checkConnectivity();
      if (connectivityResult == ConnectivityResult.none) {
        emit(state.copyWith(stateStatus: SuperLiveStateStatus.noInternet, message: trans.noInternetConnection));
        return false;
      }
    }
    return true;
  }

  void reset() {
    emit(AppBaseBlocState<T>());
  }
}
