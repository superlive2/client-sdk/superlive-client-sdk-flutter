import 'package:chat_video_sdk/chat_video_sdk.dart';

abstract class BaseBlocBuilder<T extends SerializableApp> {
  @required
  void request();

  Widget widgetBuilder(
    BuildContext context,
    AppBaseBlocCubit<T> cubit,
    AppBaseBlocState<T> state, {
    required Widget Function(BuildContext context, T data) builder,
    Widget Function(BuildContext context)? emptyBuilder,
    bool isPage = true,
    bool isWithScaffold = false,
  }) {
    final _builder = Builder(builder: (context) {
      if (cubit.isLoading) {
        if (isPage) {
          return const AppLoadingContainer();
        } else {
          return Column(
            children: const [
              SizedBox(height: 15),
              AppLoadingContainer(),
            ],
          );
        }
      }
      if (cubit.isSuccess) {
        return builder(context, state.data!);
      } else {
        if (emptyBuilder != null && state.stateStatus == SuperLiveStateStatus.empty) {
          return emptyBuilder(context);
        } else {
          if (isPage) {
            return Text("${state.message}");
            // return AppErrorWidget(
            //   status: state.stateStatus,
            //   title: state.message,
            //   onTap: () {
            //     request();
            //   },
            // );
          } else {
            return Column(
              children: [
                const SizedBox(height: 7),
                Text("${state.message}"),

                // AppErrorBottomWidget(
                //   status: state.stateStatus,
                //   message: state.message,
                //   onTap: () {
                //     request();
                //   },
                // ),
              ],
            );
          }
        }
      }
    });
    return isWithScaffold && !cubit.isSuccess
        ? Scaffold(
            appBar: AppBar(
              elevation: 0.0,
            ),
            body: _builder,
          )
        : _builder;
  }
}
