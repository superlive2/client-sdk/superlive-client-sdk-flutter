import 'package:chat_video_sdk/chat_video_sdk.dart';
class AppBaseBlocState<T> {
  final SuperLiveStateStatus stateStatus;
  final String? message;
  final String? error;
  final T? data;
  final String? url;
  final String? id;

  AppBaseBlocState({
    this.stateStatus = SuperLiveStateStatus.none,
    this.message,
    this.data,
    this.url,
    this.id,
    this.error,
  });

  AppBaseBlocState<T> copyWith({
    bool? nextUrl,
    SuperLiveStateStatus? stateStatus,
    String? message,
    T? data,
    String? url,
    String? id,
    String? error,
  }) {
    return AppBaseBlocState<T>(
      stateStatus: stateStatus ?? this.stateStatus,
      message: message ?? this.message,
      data: data ?? this.data,
      url: url ?? this.url,
      id: id ?? this.id,
      error: error ?? this.error,
    );
  }
}
