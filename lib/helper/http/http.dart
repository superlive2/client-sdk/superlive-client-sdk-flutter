import 'dart:async';
import 'dart:io';
import 'package:chat_video_sdk/chat_video_sdk.dart';
import 'package:chat_video_sdk/data/translate/translate.dart';

class _AppHttpModel {
  final String url;
  final int? timeOut;
  final Map<String, dynamic>? fields;
  final Map<String, List<dynamic>>? fieldsList;
  final Map<String, File>? files;
  final Map<String, String>? headers;
  final Map<String, List<File>>? filesList;
  final CancelToken? cancelToken;
  final ProgressCallback? onSendProgress;
  final ProgressCallback? onReceiveProgress;

  _AppHttpModel({
    this.url = '',
    this.timeOut,
    this.fields,
    this.fieldsList,
    this.files,
    this.filesList,
    this.headers,
    this.cancelToken,
    this.onSendProgress,
    this.onReceiveProgress,
  });
}

class Http {
  final String url;
  final int? timeOut;
  final Map<String, dynamic>? fields;
  final Map<String, List<dynamic>>? fieldsList;
  final Map<String, File>? files;
  final Map<String, String>? headers;
  final Map<String, List<File>>? filesList;
  final CancelToken? cancelToken;
  final ProgressCallback? onSendProgress;
  final ProgressCallback? onReceiveProgress;

  final bool addBaseUrl;
  final bool addDefaultHeader;
  final bool addToken;
  bool _isRepeat = false;

  Http({
    this.url = '',
    this.timeOut,
    this.fields,
    this.fieldsList,
    this.files,
    this.filesList,
    this.headers,
    this.cancelToken,
    this.onSendProgress,
    this.onReceiveProgress,
    this.addBaseUrl = true,
    this.addDefaultHeader = true,
    this.addToken = true,
  });

  Future<Map<String, dynamic>> get({String? newToken}) async {
    if (url == '') {
      return Future.error(trans.somethingUnexpectedWentWrong);
    }
    var _httpModel = _init(newToken: newToken);

    return AppPKHttp.requestMethod(
      url: _httpModel.url,
      fields: _httpModel.fields,
      headers: _httpModel.headers,
      timeOut: _httpModel.timeOut,
      cancelToken: _httpModel.cancelToken,
      onReceiveProgress: _httpModel.onReceiveProgress,
      onSendProgress: _httpModel.onSendProgress,
      type: AppHttpType.get,
    ).then((value) {
      return value;
    }).onError((error, stackTrace) {
      return _checkWhenError(error, AppHttpType.get);
    });
  }

  Future<Map<String, dynamic>> post({String? newToken}) async {
    if (url == '') {
      return Future.error(trans.somethingUnexpectedWentWrong);
    }
    var _httpModel = _init(newToken: newToken);
    return AppPKHttp.requestMethod(
      url: _httpModel.url,
      fields: _httpModel.fields,
      fieldsList: _httpModel.fieldsList,
      files: _httpModel.files,
      filesList: _httpModel.filesList,
      headers: _httpModel.headers,
      timeOut: _httpModel.timeOut,
      cancelToken: _httpModel.cancelToken,
      onReceiveProgress: _httpModel.onReceiveProgress,
      onSendProgress: _httpModel.onSendProgress,
      type: AppHttpType.post,
    ).then((value) {
      return value;
    }).onError((error, stackTrace) {
      return _checkWhenError(error, AppHttpType.post);
    });
  }

  _AppHttpModel _init({String? newToken}) {
    var _headers = <String, String>{};
    var _url = '';
    Map<String, dynamic>? _fields = {};
    Map<String, List<dynamic>>? _fieldsList = {};
    Map<String, List<File>>? _filesList = {};
    if (addDefaultHeader) {
      _headers.addAll({});
    }
    if (headers != null) {
      _headers.addAll(headers!);
    }
    if (addBaseUrl) {
      _url = sdkConfig.baseUrl;
    }
    _url = _url + url;
    if (addToken) {
      _headers['Authorization'] = sdkConfig.apiKey ?? "";
    }
    if (fields != null) {
      _fields.addAll(fields!);
    }
    if (fieldsList != null) {
      _fieldsList.addAll(fieldsList!);
    }
    if (filesList != null) {
      _filesList.addAll(filesList!);
    }
    if (newToken != null) {
      _headers['Authorization'] = "Bearer $newToken";
    }
    return _AppHttpModel(
      url: _url,
      fields: _fields,
      files: files,
      fieldsList: _fieldsList,
      filesList: _filesList,
      headers: _headers,
      timeOut: timeOut,
      cancelToken: cancelToken,
      onReceiveProgress: onReceiveProgress,
      onSendProgress: onSendProgress,
    );
  }

  Future<Map<String, dynamic>> _checkWhenError(dynamic error, AppHttpType httpType) async {
    if (error is AppHttpError) {
      return Future.error("${error.message}<<errorCode>>${error.errorCode}");
    } else {
      return Future.error(trans.somethingUnexpectedWentWrong);
    }
  }
}

class AppForeLogout {
  final bool isLogout;

  AppForeLogout(this.isLogout);
}
