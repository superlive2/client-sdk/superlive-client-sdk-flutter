import 'package:chat_video_sdk/chat_video_sdk.dart';

import '../widget/required_sigin/required_sigin_page.dart';

enum SuperLiveStateStatus { none, initial, loading, failure, empty, success, noInternet }

enum VideoDisplayOption { standard, fill, cover }

class ChannelName {
  static Map<int, String> name = {
    1: '香港',
    2: "澳门",
    3: '台湾',
    4: '新澳门',
  };
}

class NewVoteEvenListenerType {
  static const String create = "create";
  static const String delete = "delete";
  static const String end = "end";
}

class AppEventBusConnectivity {
  final bool isConnected;
  const AppEventBusConnectivity({this.isConnected = true});
}

class ChatVideoPlayerConfig {
  String? apiKey;
  bool isProduction;
  String language;
  VideoDisplayOption videoDisplayOption;

  ChatVideoPlayerConfig({
    this.apiKey,
    this.isProduction = false,
    this.language = 'en',
    this.videoDisplayOption = VideoDisplayOption.standard,
  });

  final String _baseUrlDev = 'http://merch.sp.tv/api/client-sdk/';
  // final String _baseUrlDev = 'http://192.168.160.166:5000/api/client-sdk/';
  final String _baseUrlProduction = 'https://merchant.super-live.tv/api/client-sdk/';
  String get baseUrl => isProduction ? _baseUrlProduction : _baseUrlDev;

  final String _socketBaseUrlDev = 'ws://merch.sp.tv/';
  // final String _socketBaseUrlDev = 'ws://192.168.160.166:5000/';
  final String _socketBaseUrlProduction = 'wss://merchant.super-live.tv/';
  String get socketBaseUrl => isProduction ? _socketBaseUrlProduction : _socketBaseUrlDev;

  final String _socketPath = "/api/socket.io";
  // final String _socketPath = "/socket.io";
  final String _appPackageName = "chat_video_sdk";
  String get socketPath => _socketPath;
  String get appPackageName => _appPackageName;

  ChatVideoPlayerConfig initClass({
    required String apiKey,
    required bool isProduction,
    required String language,
    required VideoDisplayOption videoDisplayOption,
  }) {
    return ChatVideoPlayerConfig(
        apiKey: apiKey, isProduction: isProduction, language: language, videoDisplayOption: videoDisplayOption);
  }

  static void init(
      {required String apiKey,
      bool isProduction = false,
      required String language,
      required VideoDisplayOption videoDisplayOption}) {
    sdkConfig = sdkConfig.initClass(
      apiKey: apiKey,
      isProduction: isProduction,
      language: language,
      videoDisplayOption: videoDisplayOption,
    );
  }
}

class AppMiniPlay {
  final bool isMax;

  AppMiniPlay({this.isMax = false});
}

class ChatVideoPlayerHelperEventBus {
  final String hostId;
  final String? title;
  final String? description;
  final bool isGuestUser;

  const ChatVideoPlayerHelperEventBus({required this.hostId, this.title, this.description, this.isGuestUser = false});
}

class PlayMiniPlayer {
  final List<String> urls;
  final String? title, description;

  const PlayMiniPlayer({
    required this.urls,
    this.title,
    this.description,
  });
}

class CloseMiniPlayer {}

EdgeInsets appGetAppBarPadding(BuildContext context) {
  return MediaQuery.of(context).padding;
}

String formatDurationInHhMmSs(Duration duration) {
  final HH = (duration.inHours).toString().padLeft(2, '0');
  final mm = (duration.inMinutes % 60).toString().padLeft(2, '0');
  final ss = (duration.inSeconds % 60).toString().padLeft(2, '0');

  return '$HH:$mm:$ss';
}

void checkRequiredLogin(
    {required BuildContext context, required bool isGuestUser, required Function() eventContinue}) async {
  if (isGuestUser == true) {
    /// Request login
    var _resp = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const RequiredSigInPage()),
    );
    if (_resp.toString() == "goToLogIn") {
      appEventBus.fire(
        const GuestModeEventBus(
          isRequiredLogin: true,
        ),
      );
    }
  } else {
    /// Continue something
    eventContinue();
  }
}

BuildContext? globalContext;
bool currentVersionIsAlreadySubmit = false;
