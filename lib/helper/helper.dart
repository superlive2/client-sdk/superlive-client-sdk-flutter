import 'package:flutter/material.dart';

export 'storage_util.dart';
export 'app_connectivity.dart';
export 'bloc/base_bloc_cubit.dart';
export 'constant.dart';
export 'types/app_type.dart';
export 'http/http.dart';
export 'video/mini_fijk_player.dart';
export 'options/app_context_menu.dart';

extension ListExtension on List {
  List<Widget> separator(Widget Function(int index) separatorBuilder) {
    var result = List<Widget>.empty(growable: true);
    for (int i = 0; i < length; i++) {
      result.add(this[i]);
      if (i != length - 1) {
        result.add(separatorBuilder(i));
      }
    }
    return result;
  }
}