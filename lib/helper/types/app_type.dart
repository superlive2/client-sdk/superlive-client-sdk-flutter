import 'package:chat_video_sdk/chat_video_sdk.dart';
import 'package:dartz/dartz.dart';

import '../../data/translate/translate.dart';

typedef BaseResponseEither<Type> = Future<Either<String, Type>>;
typedef BaseJson = Map<String, dynamic>;

class AppBaseResponse<T extends SerializableApp> {
  int statusCode;
  String message;
  T? data;
  List<T>? dataList;

  AppBaseResponse({
    required this.statusCode,
    required this.message,
    required this.data,
    required this.dataList,
  });

  factory AppBaseResponse.fromJson(Map<String, dynamic> json, Function(Map<String, dynamic>) create) {
    try {
      final List<T>? itemList = json["data"] == null || json["data"] is Map ? null : List<T>.from(json["data"].map((x) => create(x)));
      final T? itemMap = json["data"] == null || json["data"] is List ? null : create(json["data"]);
      return AppBaseResponse<T>(
        statusCode: json["statusCode"].toString().toAppInt(),
        message: checkKeyMap(json, "message") ? json["message"].toString().toAppString()! : '',
        data: itemMap,
        dataList: itemList,
      );
    } catch (e) {
      throw 'somethingUnexpectedWentWrong';
    }
  }

  Map<String, dynamic> toJson() => {
        "statusCode": this.statusCode,
        "message": this.message,
        "data": this.data?.toJson(),
        "dataList": this.dataList?.map((x) => x.toJson()).toList(),
      };
}

abstract class SerializableApp {
  Map<String, dynamic> toJson();
}

class AppBaseResponsePaginateDataPaginate {
  AppBaseResponsePaginateDataPaginate({
    required this.total,
    required this.page,
    required this.hasNextUrl,
  });

  final int total;
  final int page;
  final bool hasNextUrl;

  factory AppBaseResponsePaginateDataPaginate.fromJson(Map<String, dynamic> json) {
    try {
      return AppBaseResponsePaginateDataPaginate(
        total: json["total"].toString().toAppInt(),
        page: json["page"].toString().toAppInt(),
        hasNextUrl: json["total"].toString().toAppInt() < json["page"].toString().toAppInt(),
      );
    } catch (e) {
      throw trans.somethingUnexpectedWentWrong;
    }
  }

  Map<String, dynamic> toJson() => {
        "total": total,
        "next_page_url": hasNextUrl,
      };
}

class AppBaseResponsePaginateData<T> {
  List<T>? items;
  T? itemMap;

  AppBaseResponsePaginateData({
    required this.items,
    required this.itemMap,
  });

  factory AppBaseResponsePaginateData.fromJson(List<dynamic> json, List<T>? t, T? tMap) {
    try {
      return AppBaseResponsePaginateData(
        items: t ?? [],
        itemMap: tMap,
      );
    } catch (e) {
      throw trans.somethingUnexpectedWentWrong;
    }
  }

  Map<String, dynamic> toJson() => {};
}

class AppBaseResponsePaginate<T extends SerializablePaginateApp> {
  int statusCode;
  String message;
  AppBaseResponsePaginateData<T> data;
  AppBaseResponsePaginateDataPaginate extra;

  AppBaseResponsePaginate({
    required this.statusCode,
    required this.message,
    required this.data,
    required this.extra,
  });

  factory AppBaseResponsePaginate.fromJson(Map<String, dynamic> json, Function(Map<String, dynamic>) create) {
    try {
      final _json = json["data"];
      final List<T>? itemList = _json != null && _json is List ? List<T>.from(_json.map((x) => create(x))) : null;
      final T? itemMap = _json != null && _json is Map ? create(_json as Map<String, dynamic>) : null;
      return AppBaseResponsePaginate<T>(
        statusCode: json["statusCode"].toString().toAppInt(),
        message: json["message"].toString().toAppString()!,
        data: AppBaseResponsePaginateData<T>.fromJson(json["data"], itemList, itemMap),
        extra: AppBaseResponsePaginateDataPaginate.fromJson(json["extra"],),
      );
    } catch (e) {
      return AppBaseResponsePaginate<T>(
        statusCode: 200,
        message: "",
        data: AppBaseResponsePaginateData<T>(
          items: [],
          itemMap: null,
        ),
        extra: AppBaseResponsePaginateDataPaginate(
          total: 0,
          hasNextUrl: false,
          page: 0,
        ),
      );
    }
  }

  Map<String, dynamic> toJson() => {
        "statusCode": this.statusCode,
        "message": this.message,
        "data": this.data.toJson(),
        "extra": this.extra.toJson(),
      };
}

abstract class SerializablePaginateApp {
  Map<String, dynamic> toJson();
}
