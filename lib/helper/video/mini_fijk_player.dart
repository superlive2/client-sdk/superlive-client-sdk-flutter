import 'dart:async';
import 'package:after_layout/after_layout.dart';
import 'package:chat_video_sdk/chat_video_sdk.dart';
import 'package:chatme_ui_sdk/helper/helper.dart';
import 'package:chatme_ui_sdk/services/message_service.dart';
import 'package:chatme_ui_sdk/ui/screen/chatme_sdk.dart';
import 'package:chatme_ui_sdk/util/app_color.dart';
import 'package:chatme_ui_sdk/util/app_constant.dart';
import 'package:chatme_ui_sdk/util/custom_font_weight.dart';
import 'package:chatme_ui_sdk/util/hex_color.dart';
import 'package:event_bus/event_bus.dart';
import 'package:fijkplayer/eventbus/play_video_event_bus.dart';
import 'package:fijkplayer/fijkplayer.dart';
import 'package:fijkplayer/helper/internet_download_speed.dart';
import 'package:fijkplayer/model/error_message.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:miniplayer/miniplayer.dart';
import '../../data/translate/translate.dart';
import '../../logic/host/request_join_host_cubit.dart';
import '../../widget/animations/animations_style_widget.dart';
import '../../widget/chat/gift.dart';

class SuperliveVideoFijkMiniHelper {
  final MiniplayerController _miniPlayerController = MiniplayerController();

  MiniplayerController get miniPlayerController => _miniPlayerController;

  final EventBus _eventBusMiniHelper = EventBus();
  EventBus get eventBusMiniHelper => _eventBusMiniHelper;

  void pay({
    required List<String> urls,
    String? title,
    String? description,
  }) {
    _eventBusMiniHelper.fire(PlayMiniPlayer(urls: urls, title: title, description: description));
  }

  Widget builder({
    required Widget child,
    required HostState state,
    required ChatVideoPlayerHelper chatHelper,
    Widget? relatedChild,
    double? bottom,
    bool? isActive,
    GestureTapCallback? onClose,
    ValueChanged<bool>? onMinimize,
    ValueChanged<String?>? onMore,
  }) {
    return _MiniPlayerWidget(
      helper: this,
      relatedChild: GestureDetector(
        onHorizontalDragUpdate: (e) {},
        onVerticalDragUpdate: (e) {},
        child: relatedChild,
      ),
      bottom: bottom,
      isActive: isActive ?? true,
      onClose: onClose,
      onMinimize: onMinimize,
      onMore: onMore,
      state: state,
      chatHelper: chatHelper,
      child: child,
    );
  }
}

class _MiniPlayerWidget extends StatefulWidget {
  final SuperliveVideoFijkMiniHelper helper;
  final Widget child;
  final Widget? relatedChild;
  final bool isActive;
  final double? bottom;
  final GestureTapCallback? onClose;
  final ValueChanged<bool>? onMinimize;
  final ValueChanged<String?>? onMore;
  final HostState state;
  final ChatVideoPlayerHelper chatHelper;

  const _MiniPlayerWidget({
    Key? key,
    required this.child,
    this.relatedChild,
    required this.helper,
    this.bottom,
    required this.isActive,
    this.onClose,
    this.onMinimize,
    this.onMore,
    required this.state,
    required this.chatHelper,
  }) : super(key: key);

  @override
  State<_MiniPlayerWidget> createState() => _MiniPlayerWidgetState();
}

class _MiniPlayerWidgetState extends State<_MiniPlayerWidget> {
  double playerMinHeight = 80.0;
  bool _isMax = false;
  PlayMiniPlayer? _selectedVideo;
  bool chatPlay = false;
  late StreamSubscription<AppMiniPlay> _eventBus;
  late StreamSubscription<PlayMiniPlayer> _eventBusPlay;
  late StreamSubscription<PlayMiniPlayer> _eventBusPlay2;
  late StreamSubscription<ChatVideoPlayerHelperEventBus> _eventBusPlayChat;

  void eventAppMiniPlay(AppMiniPlay event) {
    _isMax = event.isMax;
    setState(() {});
    if (widget.onMinimize != null) {
      widget.onMinimize!(!_isMax);
    }
  }

  void eventPlayMiniPlayer(PlayMiniPlayer event) {
    if (widget.isActive) {
      _selectedVideo = event;
      setState(() {});
      Future.delayed(const Duration(milliseconds: 100), () {
        widget.helper.miniPlayerController.animateToHeight(state: PanelState.MAX);
      });
    }
  }

  @override
  void initState() {
    _eventBus = widget.helper.eventBusMiniHelper.on<AppMiniPlay>().listen((event) {
      eventAppMiniPlay(event);
    });
    _eventBusPlay = playerEventBus.on<PlayMiniPlayer>().listen((event) {
      eventPlayMiniPlayer(event);
    });
    _eventBusPlay2 = widget.helper.eventBusMiniHelper.on<PlayMiniPlayer>().listen((event) {
      eventPlayMiniPlayer(event);
    });
    _eventBusPlayChat = widget.helper.eventBusMiniHelper.on<ChatVideoPlayerHelperEventBus>().listen((event) {
      chatPlay = true;
      setState(() {});
    });
    super.initState();
  }

  @override
  void dispose() {
    _eventBus.cancel();
    _eventBusPlay.cancel();
    _eventBusPlay2.cancel();
    _eventBusPlayChat.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final _height = MediaQuery.of(context).size.width * (9 / 16);
    final _bottom = MediaQuery.of(context).padding.bottom;
    final _playerMinHeight = playerMinHeight + (widget.bottom == null ? _bottom : 0);
    return Stack(
      children: [
        widget.child,
        if (_selectedVideo != null || chatPlay)
          Positioned.fill(
            bottom: widget.bottom == null ? 0 : (!_isMax ? widget.bottom : 0),
            child: _PlayerWidget(
              player: _selectedVideo,
              playerMinHeight: _playerMinHeight,
              isMax: _isMax,
              height: _height,
              helper: widget.helper,
              mainWidget: widget,
              bottom: _bottom,
              onClose: () {
                _selectedVideo = null;
                chatPlay = false;
                requestJoinChat = true;
                fijkPlayerVolume = 1.0;
                isfijkPlayerVolume = true;
                InternetSpeedHelper.stopCheckingInternetSpeed();
                setState(() {});
              },
              state: widget.state,
              chatHelper: widget.chatHelper,
            ),
          ),
      ],
    );
  }
}

class _PlayerWidget extends StatefulWidget {
  final double height;
  final double bottom;
  final double playerMinHeight;
  final bool isMax;
  final SuperliveVideoFijkMiniHelper helper;
  final _MiniPlayerWidget mainWidget;
  final PlayMiniPlayer? player;
  final GestureTapCallback? onClose;
  final HostState state;
  final ChatVideoPlayerHelper chatHelper;

  const _PlayerWidget({
    Key? key,
    required this.playerMinHeight,
    required this.isMax,
    required this.height,
    required this.helper,
    required this.mainWidget,
    required this.bottom,
    required this.player,
    this.onClose,
    required this.state,
    required this.chatHelper,
  }) : super(key: key);

  @override
  State<_PlayerWidget> createState() => _PlayerWidgetState();
}

class _PlayerWidgetState extends State<_PlayerWidget> with AfterLayoutMixin {
  late var miniPlayerHeightNotifier = ValueNotifier<double>(widget.playerMinHeight);

  final player = FijkPlayer();
  late StreamSubscription<PlayMiniPlayer> _eventBusPlay;
  late StreamSubscription<PlayMiniPlayer> _eventBusPlay2;
  late StreamSubscription<CloseMiniPlayer> _eventBusClosePlayer;
  late StreamSubscription<CloseMiniPlayer> _eventBusClosePlayer2;
  GiftResponse? _selectedGift;
  List<PackResponse>? _listPackages;
  bool _isShowCongratsScreen = false;
  final MessageService _messageService = MessageService();
  String? _currentView;
  List<String> _urls = [];
  final GlobalKey _miniPlayerKey = GlobalKey();
  String numberOfGift = "1";

  void play([List<String>? urls]) {
    if (widget.mainWidget.isActive) {
      player.pause();
      if (urls != null) {
        _urls = urls;
        playVideo(urls);
      }
    }
  }

  Future<void> playVideo(List<String> urls) async {
    player.setOption(FijkOption.playerCategory, "mediacodec-all-videos", 1);
    player.setDataSource(urls.length == 1? urls[0]: urls[1], autoPlay: true);
  }

  @override
  void initState() {
    if (widget.player != null) {
      playVideo(widget.player!.urls);
    }
    _eventBusPlay = playerEventBus.on<PlayMiniPlayer>().listen((event) {
      play(event.urls);
    });
    _eventBusPlay2 = widget.helper.eventBusMiniHelper.on<PlayMiniPlayer>().listen((event) {
      play(event.urls);
    });
    _eventBusClosePlayer = widget.helper.eventBusMiniHelper.on<CloseMiniPlayer>().listen((event) {
      close();
    });
    _eventBusClosePlayer2 = playerEventBus.on<CloseMiniPlayer>().listen((event) {
      close();
    });
    super.initState();
  }

  @override
  void dispose() {
    try {
      player.release();
      _eventBusPlay.cancel();
      _eventBusPlay2.cancel();
      _eventBusClosePlayer.cancel();
      _eventBusClosePlayer2.cancel();
      _messageService.disposedSocket();
      widget.chatHelper.socketService.unSubScribeRoom(
        roomId: "${widget.chatHelper.hostId}",
        callBack: (data) {
          //
        },
      );
      widget.chatHelper.socketService.disposed();
    } catch (_) {}
    super.dispose();
  }

  void willDispose() {
    try {
      player.release();
      if (widget.onClose != null) {
        widget.onClose!();
      }
      if (widget.mainWidget.onClose != null) {
        widget.mainWidget.onClose!();
      }
      player.release();
      _eventBusPlay.cancel();
      _eventBusPlay2.cancel();
    } catch (_) {}
  }

  @override
  Widget build(BuildContext context) {
    return MiniplayerWillPopScope(
      onWillPop: () async {
        willDispose();
        return true;
      },
      child: Stack(
        children: [
          // Dark background behind miniplayer
          Positioned.fill(
            child: IgnorePointer(
              child: ValueListenableBuilder<double>(
                valueListenable: miniPlayerHeightNotifier,
                builder: (context, height, _) {
                  final maxHeight = MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;
                  final opacity = (height - widget.playerMinHeight) / (maxHeight - widget.playerMinHeight);
                  return AppBar(
                    elevation: 0,
                    backgroundColor: Colors.black.withOpacity(opacity),
                    toolbarHeight: 0,
                    surfaceTintColor: Colors.transparent,
                    systemOverlayStyle: opacity > 0.6 ? SystemUiOverlayStyle.light : SystemUiOverlayStyle.dark,
                  );
                },
              ),
            ),
          ),
          Miniplayer(
            key: _miniPlayerKey,
            onDismiss: () {},
            onDismissed: () => willDispose(),
            tapToCollapse: false,
            controller: widget.helper.miniPlayerController,
            minHeight: widget.playerMinHeight,
            maxHeight: MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top,
            valueNotifier: miniPlayerHeightNotifier,
            builder: (height, percentage) {
              bool isMin = percentage <= 0.4;

              if (isMin) {
                if (widget.isMax) {
                  widget.helper.eventBusMiniHelper.fire(AppMiniPlay(isMax: false));
                }
              } else {
                if (!widget.isMax) {
                  widget.helper.eventBusMiniHelper.fire(AppMiniPlay(isMax: true));
                }
              }
              Widget _video = Stack(
                children: [
                  Positioned.fill(
                    child: AppGestureDetector(
                      onTap: !isMin
                          ? null
                          : () {
                              widget.helper.miniPlayerController.animateToHeight(state: PanelState.MAX);
                            },
                      child: IgnorePointer(
                        ignoring: isMin,
                        child: BlocBuilder<RequestJoinHostCubit, RequestJoinHostState>(
                          builder: (context, requestChatState) {
                            return BlocBuilder<GiftsPacksCubit, GiftsPacksState>(
                              builder: (context, giftsPacksState) {
                                if ((giftsPacksState.stateStatus == SuperLiveStateStatus.success ||
                                        giftsPacksState.stateStatus == SuperLiveStateStatus.empty) &&
                                    requestChatState.stateStatus == SuperLiveStateStatus.success) {
                                  _listPackages = giftsPacksState.data ?? [];
                                  return BlocListener<SendGiftCubit, SendGiftState>(
                                    listener: (context, state) {
                                      if (state.stateStatus == SuperLiveStateStatus.success && _selectedGift != null) {
                                        setState(() {
                                          _isShowCongratsScreen = true;
                                        });
                                        Future.delayed(const Duration(seconds: 4), () {
                                          setState(() {
                                            _isShowCongratsScreen = false;
                                          });
                                        });
                                        _messageService.sendMessage(
                                          "🎁${_selectedGift!.name} - ${_selectedGift!.point}" + trans.pts+ (numberOfGift == "1"? "": " x$numberOfGift"),
                                          messageStyle: {
                                            "color": AppColor.primaryColor,
                                            "fontSize": "12",
                                            "fontWeight": CustomFontWeight.w900,
                                          },
                                          callBack: (e) {
                                            _selectedGift = null;
                                            setState(() {});
                                            chatEventBus.fire(const SentGiftEvent());
                                          },
                                          onError: (e, v) {
                                            //
                                          },
                                        );
                                      } else if (state.stateStatus == SuperLiveStateStatus.failure) {
                                        // showToastBarHaveNewGiftSent(
                                        //   context,
                                        //   title: trans.sendFail,
                                        //   message: '${state.message}',
                                        //   bottom: 10,
                                        //   backgroundColor: AppColor.errorColor,
                                        // );
                                      }
                                    },
                                    child: FijkView2(
                                      urls: _urls,
                                      broadcastSoonTextMessage: trans.broadcastSoon,
                                      fjikPlayerErrorMessage: FijkPlayerErrorMessage(
                                        errorTitle: trans.errorTitle,
                                        errorMessage: trans.errorMessage,
                                        retryTextButton: trans.retryTextButton,
                                      ),
                                      liveStreamText: trans.liveStream,
                                      currentViewLiveStream: _currentView ?? "0",
                                      isShowLive: false,
                                      fsRightWidget: widget.state.data?.chatRoomId == null
                                          ? const SizedBox()
                                          : Stack(
                                              children: [
                                                widget.state.data!.isChatEnabled
                                                    ? ChatMeUiSdk(
                                                        setTimeoutEachMessage: setTimeoutPerMessage,
                                                        isBanned: requestChatState.data!.isBanned,
                                                        isDisplayFullScreen: true,
                                                        roomId: "${widget.state.data?.chatRoomId}",
                                                        isImplementGift: _listPackages!.isNotEmpty ? true : false,
                                                        accessToken: requestChatState.data!.chatToken,
                                                        onSwitchInputType: (isOnSwitchType) {},
                                                        isGuestUser: widget.chatHelper.isGuestUser ?? false,
                                                        isRequiredLogin: (isRequiredLogin) {
                                                          if (isRequiredLogin) {
                                                            /// Required login to send message
                                                            checkRequiredLogin(
                                                              context: context,
                                                              isGuestUser: widget.chatHelper.isGuestUser ?? false,
                                                              eventContinue: () {},
                                                            );
                                                          }
                                                        },
                                                        gifts: GiftWidget(
                                                          isSecondScreen: true,
                                                          data: _listPackages!,
                                                          onClear: () {
                                                            _selectedGift = null;
                                                            setState(() {});
                                                          },
                                                          onChoose: (v) {
                                                            _selectedGift = v;
                                                            setState(() {});
                                                          },
                                                          selectedGift: _selectedGift,
                                                          onSentGift: (giftAmount) {
                                                            checkRequiredLogin(
                                                              context: context,
                                                              isGuestUser: widget.chatHelper.isGuestUser ?? false,
                                                              eventContinue: () {
                                                                numberOfGift = giftAmount;
                                                                cubitSendGift.giftAmount = giftAmount;
                                                                cubitSendGift.request(_selectedGift!.id);
                                                              },
                                                            );
                                                          },
                                                        ),
                                                        onSuccess: (succeedMessage) {
                                                          debugPrint("On success");
                                                          debugPrint(succeedMessage.toString());
                                                        },
                                                        onError: (errorMessage) {
                                                          debugPrint("On error");
                                                          debugPrint(errorMessage.toString());
                                                        },
                                                        callBackSocket: (messageService) {
                                                          // messageService = messageService;
                                                        },
                                                        congrats: const SizedBox(),
                                                        messageService: _messageService,
                                                        language: sdkConfig.language,
                                                      )
                                                    : Center(
                                                        child: Text(
                                                          trans.chatDisabled,
                                                          style: TextStyle(
                                                            color: HexColor(AppColor.disabledColor),
                                                          ),
                                                        ),
                                                      ),
                                                if (_isShowCongratsScreen && giftCongrats != null)
                                                  IgnorePointer(
                                                    ignoring: true,
                                                    child: SafeArea(
                                                      top: false,
                                                      child: Container(
                                                        alignment: Alignment.center,
                                                        margin: const EdgeInsets.only(bottom: 70),
                                                        child: GiftAnimatedWidget(
                                                          gift: giftCongrats!,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                              ],
                                            ),
                                      player: player,
                                      fit: FijkFit.cover,
                                      fsFit: sdkConfig.videoDisplayOption == VideoDisplayOption.standard
                                          ? FijkFit.contain
                                          : sdkConfig.videoDisplayOption == VideoDisplayOption.fill
                                              ? FijkFit.fill
                                              : FijkFit.cover,
                                      color: HexColor(AppColor.blackColor),
                                    ),
                                  );
                                }
                                return const AppLoadingContainer();
                              },
                            );
                          },
                        ),
                      ),
                    ),
                  ),
                  if (widget.isMax)
                    Positioned(
                      top: 0,
                      left: 0,
                      child: AppGestureDetector(
                        paddingAll: 5,
                        onTap: () {
                          Future.delayed(const Duration(milliseconds: 100), () {
                            widget.helper.miniPlayerController.animateToHeight(state: PanelState.MIN);
                          });
                        },
                        child: Container(
                          margin: const EdgeInsets.only(left: 15),
                          child: const Icon(
                            Icons.keyboard_arrow_down_sharp,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                ],
              );
              return Container(
                padding: EdgeInsets.only(bottom: isMin ? (widget.mainWidget.bottom == null ? widget.bottom : 0) : 0),
                color: appBackgroundDialogColor(context),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: clampDouble(
                            height - MediaQuery.of(context).padding.bottom,
                            widget.playerMinHeight - MediaQuery.of(context).padding.bottom,
                            MediaQuery.of(context).size.width / 1.77,
                          ),
                          child: FittedBox(
                            fit: BoxFit.fitHeight,
                            child: SizedBox(
                              width: MediaQuery.of(context).size.width,
                              height: (MediaQuery.of(context).size.width / 1.77) - MediaQuery.of(context).viewInsets.bottom,
                              child: _video,
                              // child: Container(
                              //   color: Colors.black,
                              //   alignment: Alignment.center,
                              //   child: Text(
                              //     clampDouble(
                              //       height - MediaQuery.of(context).padding.bottom,
                              //       widget.playerMinHeight,
                              //       MediaQuery.of(context).size.width / 1.77,
                              //     ).toStringAsFixed(2),
                              //     style: const TextStyle(
                              //       color: Colors.white,
                              //       fontWeight: FontWeight.bold,
                              //       fontSize: 48,
                              //     ),
                              //   ),
                              // ),
                            ),
                          ),
                        ),
                        if (!isMin)
                          Expanded(
                            child: widget.mainWidget.relatedChild ?? SizedBox(height: MediaQuery.of(context).viewPadding.bottom),
                          ),
                      ],
                    ),
                    // isMin
                    //     ? Align(
                    //         alignment: Alignment.topCenter,
                    //         child: SizedBox(
                    //           height: widget.height,
                    //           child: AspectRatio(aspectRatio: 16 / 9, child: _video),
                    //         ),
                    //       )
                    //     : ,
                    if (isMin)
                      Expanded(
                        child: Container(
                          margin: const EdgeInsets.only(left: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              Expanded(
                                child: VideoMiniWidget(
                                  title: widget.player?.title ?? trans.playing,
                                  description: widget.player?.description,
                                ),
                              ),
                              const SizedBox(width: 10),
                              AppGestureDetector(
                                paddingAll: 5,
                                onTap: () {
                                  close();
                                },
                                child: Container(
                                  margin: const EdgeInsets.only(right: 16),
                                  child: Icon(
                                    Icons.close,
                                    color: appIconColor(context),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                  ],
                ),
              );
            },
          ),
        ],
      ),
    );
  }

  void close() {
    player.pause();
    if (widget.onClose != null) {
      widget.onClose!();
    }
    if (widget.mainWidget.onClose != null) {
      widget.mainWidget.onClose!();
    }
    Future.delayed(const Duration(milliseconds: 100), () {
      widget.helper.miniPlayerController.animateToHeight(state: PanelState.MIN);
      player.pause();
    });
  }

  @override
  FutureOr<void> afterFirstLayout(BuildContext context) async {
    await widget.chatHelper.socketService.initSocketConnection(
      sdkConfig.apiKey ?? "",
      "${widget.chatHelper.hostId}",
      "${widget.chatHelper.participantsId}",
      onError: (errorMessage) {
        debugPrint(errorMessage);
      },
      onSuccess: (successMessage) async {
        debugPrint(successMessage);
      },
    );
    await widget.chatHelper.socketService.subScribeRoom(
      hostId: "${widget.chatHelper.hostId}",
      callBack: (data) {},
    );
    widget.chatHelper.socketService.listenHostStart(
      callBack: (data) {
        fijkPlayerEventBus.fire(
          PlayVideoEventBus(
            url: _urls[1],
            state: PlayVideoEventBusState.startLive,
          ),
        );
      },
    );
    widget.chatHelper.socketService.listenHostStop(
      callBack: (data) {
        fijkPlayerEventBus.fire(
          PlayVideoEventBus(
            url: _urls[1],
            state: PlayVideoEventBusState.stopLive,
          ),
        );
      },
    );
  }
}
