enum VoteEventBusState {update, delete}

class VoteEventBus {
  VoteEventBusState voteEventBusState;

  VoteEventBus({required this.voteEventBusState});
}
