class GuestModeEventBus {
  final bool isRequiredLogin;
  final dynamic data;

  const GuestModeEventBus({required this.isRequiredLogin, this.data});
}
