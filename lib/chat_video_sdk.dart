library chat_video_sdk;

import 'package:chat_video_sdk/chat_video_sdk.dart';
import 'package:chat_video_sdk/data/socket_service/socket_service.dart';
import 'package:chat_video_sdk/logic/chat/get_chat_host_cubit.dart';
import 'package:chat_video_sdk/logic/chat/get_vote_cubit.dart';
import 'package:chat_video_sdk/logic/chat/top_gift_cubit.dart';
import 'package:chat_video_sdk/logic/host/request_join_host_cubit.dart';
import 'package:chat_video_sdk/logic/host/request_left_host_cubit.dart';
import 'package:chat_video_sdk/logic/host/view_count_cubit.dart';
import 'package:event_bus/event_bus.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'logic/vote/retract_vote_cubit.dart';
import 'logic/vote/submit_vote_cubit.dart';

export 'package:app_packages/app_package.dart';

export 'data/index.dart';
export 'data/model/index.dart';
export 'helper/helper.dart';
export 'logic/index.dart';
export 'widget/chat_video_mini_player_widget.dart';
export 'widget/chat_widget.dart';
export 'widget/index.dart';
export 'widget/video_mini_widget.dart';
export 'widget/video_padding_widget.dart';

EventBus playerEventBus = EventBus();

ChatVideoPlayerConfig sdkConfig = ChatVideoPlayerConfig();

class ChatVideoPlayerHelper {
  final videoMiniHelper = SuperliveVideoFijkMiniHelper();
  final SocketService _socketService = SocketService();
  SocketService get socketService => _socketService;
  final EventBus _eventBus = EventBus();
  EventBus get eventBus => _eventBus;
  String? _participantsId;
  String? get participantsId => _participantsId;
  String? _hostId;
  bool? _isGuestUser;
  String? get hostId => _hostId;
  bool? get isGuestUser => _isGuestUser;

  void play({
    required String hostId,
    required String participantsId,
    String? title,
    String? description,
    bool isGuestUser = false,
  }) {
    _participantsId = participantsId;
    _hostId = hostId;
    _isGuestUser = isGuestUser;
    videoMiniHelper.eventBusMiniHelper.fire(
      ChatVideoPlayerHelperEventBus(hostId: hostId, title: title, description: description, isGuestUser: isGuestUser),
    );
  }

  void close() {
    videoMiniHelper.eventBusMiniHelper.fire(CloseMiniPlayer());
    playerEventBus.fire(CloseMiniPlayer());
  }

  Widget builder({
    required Widget child,
    Widget? chatHeaderWidget,
  }) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<HostCubit>(
          create: (context) => HostCubit(),
        ),
        BlocProvider<GiftsPacksCubit>(
          create: (context) => GiftsPacksCubit(),
        ),
        BlocProvider<SendGiftCubit>(
          create: (context) => SendGiftCubit(),
        ),
        BlocProvider<TopGiftsCubit>(
          create: (context) => TopGiftsCubit(),
        ),
        BlocProvider<RequestJoinHostCubit>(
          create: (context) => RequestJoinHostCubit(),
        ),
        BlocProvider<RequestLeftHostCubit>(
          create: (context) => RequestLeftHostCubit(),
        ),
        BlocProvider<ViewCountCubit>(
          create: (context) => ViewCountCubit(),
        ),
        BlocProvider<ChatHeaderCubit>(
          create: (context) => ChatHeaderCubit(),
        ),
        BlocProvider<GetChatHostCubit>(
          create: (context) => GetChatHostCubit(),
        ),
        BlocProvider<GetVoteCubit>(
          create: (context) => GetVoteCubit(),
        ),
        BlocProvider<WinnerCubit>(
          create: (context) => WinnerCubit(),
        ),
        BlocProvider<SubmitVoteCubit>(
          create: (context) => SubmitVoteCubit(),
        ),
        BlocProvider<RetractViteCubit>(
          create: (context) => RetractViteCubit(),
        ),
      ],
      child: ChatVideoMiniPlayerWidget(
        helper: this,
        chatHeaderWidget: chatHeaderWidget,
        child: child,
      ),
    );
  }
}
