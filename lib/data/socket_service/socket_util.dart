import 'dart:async';
import 'package:chat_video_sdk/chat_video_sdk.dart';
import 'package:socket_io_client/socket_io_client.dart';

class SocketUtil {
  static Future<Socket> initConnection(String token, String hostId, String participantId) async {
    final completer = Completer<Socket>();
    Socket socket = io(
      sdkConfig.socketBaseUrl,
      OptionBuilder()
        .setTransports(['websocket'])
        .disableAutoConnect()
        .setExtraHeaders({
          'authorization': token,
          'hostid': hostId,
          'participantid': participantId,
        })
        .setPath(sdkConfig.socketPath)
        .build(),
    );
    try {
      socket.connect();
      completer.complete(socket);
    } catch (e) {
      completer.completeError(socket);
    }
    return completer.future;
  }
}
