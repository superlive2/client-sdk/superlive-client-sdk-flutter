import 'package:chat_video_sdk/chat_video_sdk.dart';
import 'package:chat_video_sdk/data/socket_service/socket_util.dart';
import 'package:flutter/foundation.dart';
import 'package:socket_io_client/socket_io_client.dart';

class SocketService {
  late Socket _socket;
  static const String _subScribeNameSpace = "subscribe";
  static const String _unSubScribeNameSpace = "unsubscribe";
  static const String _eventListenerNameSpace = "sendGift";
  static const String _eventListenerParticipantJoinNameSpace = "participantJoin";
  static const String _eventListenerParticipantLeaveNameSpace = "participantLeave";
  static const String _eventListenerCreateLuckyDraw = "hostCreateLuckyDraw";
  static const String _eventListenerHostResetLuckyDraw = "hostResetLuckyDraw";
  static const String _eventListenerHostCompleteLuckyDraw = "hostCompleteLuckyDraw";
  static const String _eventListenerNewVote = "voteEventSocket";
  static const String _eventListenerDisabledChat = "chatStatus";
  static const String _eventListenerHostStart = "hostStartLiveStream";
  static const String _eventListenerHostStop = "hostStopLiveStream";
  static const String _eventListenerGiftEventSocket = "giftEventSocket";
  static const String _eventListenerLuckyDrawParticipants = "luckyDrawParticipants";

  initSocketConnection(
    String token,
    String hostId,
    String participantId, {
    required Function(dynamic) onError,
    required Function(dynamic) onSuccess,
  }) async {
    _socket = await SocketUtil.initConnection(token, hostId, participantId);
    _socket.onError((data) => onError(data.toString()));
    _socket.onConnect((data) {
      debugPrint("===============::::::::ON CONNECTED");
      onSuccess(data.toString());
    });
    _socket.onDisconnect((data) {
      debugPrint("===============::::::::ON DISCONNECTED");
    });
  }

  void disConnect() {
    if (_socket.active) {
      if (_socket.connected) {
        _socket.disconnect();
      }
    }
  }

  void reConnect() {
    if (_socket.active) {
      if (_socket.disconnected) {
        _socket.connect();
      }
    }
  }

  void disposed() {
    if (_socket.active) {
      if (_socket.connected) {
        _socket.dispose();
      }
    }
  }

  Future<dynamic> subScribeRoom({
    required String hostId,
    required Function(dynamic) callBack,
  }) async {
    try {
      var request = {
        "roomId": hostId,
      };
      _socket.emitWithAck(
        _subScribeNameSpace,
        request,
        ack: (data) {
          data as Map;
          if (data['status'] == "success") {
            callBack(data);
          } else {
            debugPrint("$data");
          }
        },
      );
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  Future<dynamic> unSubScribeRoom({
    required String roomId,
    required Function(dynamic) callBack,
  }) async {
    try {
      var request = {
        "roomId": roomId,
      };
      _socket.emitWithAck(
        _unSubScribeNameSpace,
        request,
        ack: (data) {
          data as Map;
          if (data['status'] == "success") {
            callBack(data);
          } else {
            debugPrint("$data");
          }
        },
      );
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  Future<dynamic> listenReceiveGift({required Function(dynamic) callBack}) async {
    try {
      _socket.on(_eventListenerNameSpace, (data) {
        callBack(data);
      });
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  Future<dynamic> listenerHostResetLuckyDraw({required ValueChanged<bool> callBack}) async {
    try {
      _socket.on(_eventListenerHostResetLuckyDraw, (data) {
        callBack(true);
      });
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  Future<dynamic> listenerHostCompleteLuckyDraw({required ValueChanged<bool> callBack}) async {
    try {
      _socket.on(_eventListenerHostCompleteLuckyDraw, (data) {
        callBack(true);
      });
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  Future<dynamic> giftEventSocket({required ValueChanged<bool> callBack}) async {
    try {
      _socket.on(_eventListenerGiftEventSocket, (data) {
        callBack(true);
      });
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  Future<dynamic> listenerWinnerCreateLuckyDraw({required ValueChanged<bool> callBack}) async {
    try {
      _socket.on(_eventListenerCreateLuckyDraw, (data) {
        callBack(true);
      });
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  Future<dynamic> listenReceiveParticipantJoin({required Function(dynamic) callBack}) async {
    try {
      _socket.on(_eventListenerParticipantJoinNameSpace, (data) {
        callBack(data);
      });
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  Future<dynamic> listenReceiveParticipantLeave({required Function(dynamic) callBack}) async {
    try {
      _socket.on(_eventListenerParticipantLeaveNameSpace, (data) {
        callBack(data);
      });
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  Future<dynamic> listenReceiveNewVote({required Function(dynamic) callBack}) async {
    try {
      _socket.on(_eventListenerNewVote, (data) {
        callBack(data);
      });
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  Future<dynamic> listenReceiveDisableChat({required Function(dynamic) callBack}) async {
    try {
      _socket.on(_eventListenerDisabledChat, (data) {
        callBack(data);
      });
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  Future<dynamic> listenHostStart({required Function(dynamic) callBack}) async {
    try {
      _socket.on(_eventListenerHostStart, (data) {
        callBack(data);
      });
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  Future<dynamic> listenHostStop({required Function(dynamic) callBack}) async {
    try {
      _socket.on(_eventListenerHostStop, (data) {
        callBack(data);
      });
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  Future<dynamic> luckyDrawParticipants({required Function(dynamic) callBack}) async {
    try {
      _socket.on(_eventListenerLuckyDrawParticipants, (data) {
        callBack(data);
      });
    } catch (e) {
      debugPrint(e.toString());
    }
  }
}
