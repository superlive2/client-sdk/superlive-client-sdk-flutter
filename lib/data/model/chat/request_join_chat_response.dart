import 'package:chat_video_sdk/chat_video_sdk.dart';

/// request_join_chat_response

class RequestJoinChatResponse extends SerializableApp {

  final String chatToken;
  final bool isBanned;

  RequestJoinChatResponse({
    required this.chatToken,
    required this.isBanned,
  });

//coverFileName: json['coverFileName'].toString().toAppString()!,
// fromMap
  factory RequestJoinChatResponse.fromMap(Map<String, dynamic> json) {
    try {
      return RequestJoinChatResponse(
        chatToken: json['chatToken'].toString().toAppString()!,
        isBanned: json['isChatBanned']?? false,
      );
    } catch (e) {
      rethrow;
    }
  }

// toMap
  @override
  Map<String, dynamic> toJson() {
    return {
      'chatToken': chatToken,
      'isChatBanned': isBanned,
    };
  }
}