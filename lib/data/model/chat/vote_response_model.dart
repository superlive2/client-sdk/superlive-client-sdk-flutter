import 'dart:convert';
import 'package:chat_video_sdk/chat_video_sdk.dart';

class VoteResponseModel extends SerializableApp {
  String? id;
  String? topic;
  String? host;
  String? votedOption;
  int? status;
  DateTime? createdAt;
  DateTime? endAt;
  int? totalVotes;
  List<Option>? options;

  VoteResponseModel({
    this.id,
    this.topic,
    this.host,
    this.status,
    this.createdAt,
    this.endAt,
    this.totalVotes,
    this.options,
    this.votedOption,
  });

  factory VoteResponseModel.fromRawJson(String str) => VoteResponseModel.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory VoteResponseModel.fromJson(Map<String, dynamic> json) => VoteResponseModel(
    id: json["_id"],
    topic: json["topic"],
    host: json["host"],
    status: json["status"],
    createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
    endAt: json["endAt"] == null ? null : DateTime.parse(json["endAt"]),
    totalVotes: json["totalVotes"],
    votedOption: json["votedOption"] == ""? null: json["votedOption"],
    options: json["options"] == null ? [] : List<Option>.from(json["options"]?.map((x) => Option.fromJson(x))),
  );

  @override
  Map<String, dynamic> toJson() => {
    "_id": id,
    "topic": topic,
    "host": host,
    "status": status,
    "createdAt": createdAt?.toIso8601String(),
    "totalVotes": totalVotes,
    "votedOption": votedOption,
    "options": options == null ? [] : List<dynamic>.from(options!.map((x) => x.toJson())),
  };
}

class Option {
  String? id;
  String? name;
  DateTime? createdAt;
  String? numVotes;
  String? percentage;

  Option({
    this.id,
    this.name,
    this.createdAt,
    this.numVotes,
    this.percentage,
  });

  factory Option.fromRawJson(String str) => Option.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Option.fromJson(Map<String, dynamic> json) => Option(
    id: json["_id"],
    name: json["name"],
    createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
    numVotes: json["numVotes"].toString(),
    percentage: "${json["percentage"]?? ""}",
  );

  Map<String, dynamic> toJson() => {
    "_id": id,
    "name": name,
    "createdAt": createdAt?.toIso8601String(),
    "numVotes": numVotes,
    "percentage": percentage,
  };
}
