/// gift_response
import 'package:chat_video_sdk/chat_video_sdk.dart';
import 'package:chat_video_sdk/widget/animations/animations_style_widget.dart';
import 'package:flutter/foundation.dart';

class GiftResponse {
  final String id;
  final String name;
  final String image;
  final String point;
  final String giftPack;
  final String merchant;
  final String? mimeType;
  final String? animationImage;

  GiftResponse({
    required this.id,
    required this.name,
    required this.image,
    required this.point,
    required this.giftPack,
    required this.merchant,
    this.animationImage,
    this.mimeType,
  });

factory GiftResponse.fromMap(Map<String, dynamic> json) {
  try {
    return GiftResponse(
      id: json['_id'].toString().toAppString()!,
      name: json['name'].toString().toAppString()!,
      image: json['image'].toString().toAppString()!,
      point: json['point'].toString().toAppString()!,
      giftPack: json['giftPack'].toString().toAppString()!,
      merchant: json['merchant'].toString().toAppString()!,
      animationImage: json['animation']?? json['image'].toString(),
      mimeType: json['mimetype']?? 'image/gif',
    );
  } catch (e) {
    if (kDebugMode) {
      print(e);
    }
    rethrow;
  }
}

// toMap
Map<String, dynamic> toMap() => {
      '_id': id,
      'name': name,
      'image': image,
      'point': point,
      'giftPack': giftPack,
      'merchant': merchant,
      'animation': animationImage,
      'mimeType': mimeType,
    };
}