import 'dart:convert';

import 'package:chat_video_sdk/chat_video_sdk.dart';

class GiftReceiveOnListenModel extends SerializableApp{
  Host? host;
  Host? participant;
  GiftResponse? gift;

  GiftReceiveOnListenModel({
    this.host,
    this.participant,
    this.gift,
  });

  factory GiftReceiveOnListenModel.fromRawJson(String str) => GiftReceiveOnListenModel.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory GiftReceiveOnListenModel.fromJson(Map<String, dynamic> json) => GiftReceiveOnListenModel(
    host: json["host"] == null ? null : Host.fromJson(json["host"]),
    participant: json["participant"] == null ? null : Host.fromJson(json["participant"]),
    gift: json["gift"] == null ? null : GiftResponse.fromMap(json["gift"]),
  );

  @override
  Map<String, dynamic> toJson() => {
    "host": host?.toJson(),
    "participant": participant?.toJson(),
    "gift": gift?.toMap(),
  };
}

// class Gift {
//   String? id;
//   String? name;
//   String? image;
//   int? point;
//
//   Gift({
//     this.id,
//     this.name,
//     this.image,
//     this.point,
//   });
//
//   factory Gift.fromRawJson(String str) => Gift.fromJson(json.decode(str));
//
//   String toRawJson() => json.encode(toJson());
//
//   factory Gift.fromJson(Map<String, dynamic> json) => Gift(
//     id: json["_id"],
//     name: json["name"],
//     image: json["image"],
//     point: json["point"],
//   );
//
//   Map<String, dynamic> toJson() => {
//     "_id": id,
//     "name": name,
//     "image": image,
//     "point": point,
//   };
// }

class Host {
  String? id;
  String? name;

  Host({
    this.id,
    this.name,
  });

  factory Host.fromRawJson(String str) => Host.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Host.fromJson(Map<String, dynamic> json) => Host(
    id: json["_id"],
    name: json["name"],
  );

  Map<String, dynamic> toJson() => {
    "_id": id,
    "name": name,
  };
}
