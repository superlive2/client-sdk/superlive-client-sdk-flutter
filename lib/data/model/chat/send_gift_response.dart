import 'package:chat_video_sdk/chat_video_sdk.dart';
import 'package:flutter/foundation.dart';

/// send_gift_response
class SendGiftResponse extends SerializableApp{

  final String id;

  SendGiftResponse({
    required this.id,
  });

//coverFileName: json['coverFileName'].toString().toAppString()!,
// fromMap
factory SendGiftResponse.fromMap(Map<String, dynamic> json) {
  try {
    return SendGiftResponse(
      id: json['_id'].toString().toAppString()!,
    );
  } catch (e) {
    if (kDebugMode) {
      print(e);
    }
    rethrow;
  }
}

// toMap
@override
  Map<String, dynamic> toJson() => {
      '_id': id,
    };
}