
import 'dart:convert';
import 'package:chat_video_sdk/chat_video_sdk.dart';

class GetChatHostModel extends SerializableApp {
  String? id;
  String? name;
  bool? status;
  bool? deleted;
  DateTime? createdAt;
  DateTime? updatedAt;
  int? v;
  List<Property>? properties;

  GetChatHostModel({
    this.id,
    this.name,
    this.status,
    this.deleted,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.properties,
  });

  factory GetChatHostModel.fromRawJson(String str) => GetChatHostModel.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory GetChatHostModel.fromJson(Map<String, dynamic> json) => GetChatHostModel(
    id: json["_id"],
    name: json["name"],
    status: json["status"],
    deleted: json["deleted"],
    createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
    updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
    v: json["__v"],
    properties: json["properties"] == null ? [] : List<Property>.from(json["properties"]!.map((x) => Property.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "_id": id,
    "name": name,
    "status": status,
    "deleted": deleted,
    "createdAt": createdAt?.toIso8601String(),
    "updatedAt": updatedAt?.toIso8601String(),
    "__v": v,
    "properties": properties == null ? [] : List<dynamic>.from(properties!.map((x) => x.toJson())),
  };
}

class Property {
  String? key;
  String? value;

  Property({
    this.key,
    this.value,
  });

  factory Property.fromRawJson(String str) => Property.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Property.fromJson(Map<String, dynamic> json) => Property(
    key: json["key"],
    value: json["value"],
  );

  Map<String, dynamic> toJson() => {
    "key": key,
    "value": value,
  };
}
