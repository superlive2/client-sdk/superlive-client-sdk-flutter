import 'package:chat_video_sdk/chat_video_sdk.dart';
import 'package:flutter/foundation.dart';

/// pack_response

class PackResponse extends SerializableApp{

  final String id;
  final String name;
  final String merchant;
  final String? image;
  final List<GiftResponse>? gifts;

  PackResponse({
    required this.id,
    required this.name,
    this.image,
    required this.merchant,
    required this.gifts,
  });

factory PackResponse.fromMap(Map<String, dynamic> json) {
  try {
    return PackResponse(
      id: json['_id'].toString().toAppString()!,
      name: json['name'].toString().toAppString()!,
      image: json['image'],
      merchant: json['merchant'].toString().toAppString()!,
      gifts: json['gifts'] == null ? null : List<GiftResponse>.from(json['gifts'].map((x) => GiftResponse.fromMap(x))),
    );
  } catch (e) {
    if (kDebugMode) {
      print(e);
    }
    rethrow;
  }
}

// toMap
@override
  Map<String, dynamic> toJson() => {
      '_id': id,
      'name': name,
      'image': image,
      'merchant': merchant,
      'gifts': gifts,
    };
}