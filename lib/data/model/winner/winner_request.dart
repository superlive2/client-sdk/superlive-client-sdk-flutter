/// winner_request
class WinnerRequest {

  final String id;

  WinnerRequest({
    required this.id,
  });

  Map<String, dynamic> toMap() =>
      {
        "id": id,
      };
}