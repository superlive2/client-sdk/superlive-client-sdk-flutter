import 'package:chat_video_sdk/chat_video_sdk.dart';
import 'package:flutter/foundation.dart';

/// winner_response
class WinnerResponse extends SerializableApp {
  final String id;
  final String name;
  final String description;
  final double giftPoint;
  final String createdAt;
  final String updatedAt;

  WinnerResponse({
    required this.id,
    required this.name,
    required this.description,
    required this.giftPoint,
    required this.createdAt,
    required this.updatedAt,
  });

//coverFileName: json['coverFileName'].toString().toAppString()!,
// fromMap
  factory WinnerResponse.fromMap(Map<String, dynamic> json) {
    try {
      return WinnerResponse(
        id: json['_id'].toString().toAppString()!,
        name: json['name'].toString().toAppString()!,
        description: json['description'].toString().toAppString()!,
        giftPoint: json['giftPoint'].toString().toAppDouble(),
        createdAt: json['createdAt'].toString().toAppString()!,
        updatedAt: json['updatedAt'].toString().toAppString()!,
      );
    } catch(e){
      if (kDebugMode) {
        // print(e);
      }
      rethrow;
    }
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      '_id': id,
      'name': name,
      'description': description,
      'giftPoint': giftPoint,
      'createdAt': createdAt,
      'updatedAt': updatedAt,
    };
  }
}
