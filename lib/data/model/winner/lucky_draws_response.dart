import 'package:chat_video_sdk/chat_video_sdk.dart';
import 'package:flutter/foundation.dart';

/// lucky_draws_response
class LuckyDrawsResponse extends SerializableApp {
  final String id;
  final String drawType;
  final bool isCompleted;
  final List<WinnerResponse>? winners;
  final String host;
  final String? title;
  final String? description;
  DateTime? endAt;
  DateTime? completedAt;
  String? type;
  List? keywords;
  List<GiftResponse>? gifts;
  LuckyDrawsResponse({required this.id, required this.winners, required this.host, required this.drawType, required this.isCompleted, this.endAt, this.completedAt, this.title, this.description, this.type, this.keywords, this.gifts});

  factory LuckyDrawsResponse.fromMap(Map<String, dynamic> json) {
    try {
      return  LuckyDrawsResponse(
        id: json['_id'].toString().toAppString()!,
        drawType: json['drawType'].toString().toAppString()!,
        winners: json['winners'] != null ? List<WinnerResponse>.from(json['winners'].map((x) => WinnerResponse.fromMap(x))) : [],
        host: json['host'].toString().toAppString()!,
        isCompleted: json['isCompleted']?? false,
        title: json['name']?? "",
        type: json['type']?? "",
        keywords: json['keywords']?? [],
        gifts: json['gifts'] != null ? List<GiftResponse>.from(json['gifts'].map((x) => GiftResponse.fromMap(x))) : [],
        description: json['description']?? "",
        endAt: json["endAt"] == null ? null : DateTime.parse(json["endAt"]),
        completedAt: json["completedAt"] == null ? null : DateTime.parse(json["completedAt"]),
      );
    } catch(e){
      rethrow;
    }
  }

  @override
  Map<String, dynamic> toJson() => {
    '_id': id,
    'drawType': drawType,
    'winners': winners != null ? List<dynamic>.from(winners!.map((x) => x.toJson())) : null,
    'host': host,
    'isCompleted': isCompleted,
    "name": title,
    "description": description,
  };
}
