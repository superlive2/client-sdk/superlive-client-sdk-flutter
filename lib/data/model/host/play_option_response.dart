/// play_option_response
import 'package:chat_video_sdk/chat_video_sdk.dart';

class PlayOptionResponse {

  final String name;
  final List<String> urls;

  PlayOptionResponse({
    required this.name,
    required this.urls,
  });

//coverFileName: json['coverFileName'].toString().toAppString()!,
// fromMap
factory PlayOptionResponse.fromMap(Map<String, dynamic> json) {
  try {
    return PlayOptionResponse(
      name: json["name"].toString().toAppString()!,
      urls: List<String>.from(json["urls"].map((x) => x.toString().toAppString()!)),
    );
  } catch (e) {
    rethrow;
  }
}

// toMap
Map<String, dynamic> toMap() => {
  "name": name,
  "urls": List<dynamic>.from(urls.map((x) => x)),
};
}