import 'dart:convert';
import 'package:chat_video_sdk/chat_video_sdk.dart';

class ViewCountModel extends SerializableApp {
  int? totalView;
  int? currentView;

  ViewCountModel({
    this.totalView,
    this.currentView,
  });

  factory ViewCountModel.fromRawJson(String str) => ViewCountModel.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ViewCountModel.fromJson(Map<String, dynamic> json) => ViewCountModel(
    totalView: json["totalView"],
    currentView: json["currentView"],
  );

  Map<String, dynamic> toJson() => {
    "totalView": totalView,
    "currentView": currentView,
  };
}
