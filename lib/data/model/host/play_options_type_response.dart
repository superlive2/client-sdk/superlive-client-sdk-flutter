import 'package:chat_video_sdk/chat_video_sdk.dart';

/// push_options_response

class PlayOptionsTypeResponse {

  final List<PlayOptionResponse>? llhls;
  final List<PlayOptionResponse>? ws;


  PlayOptionsTypeResponse({
    required this.llhls,
    required this.ws,
  });

//coverFileName: json['coverFileName'].toString().toAppString()!,
// fromMap
factory PlayOptionsTypeResponse.fromMap(Map<String, dynamic> json) {
  try {
    return PlayOptionsTypeResponse(
      llhls: List<PlayOptionResponse>.from(json["llhls"].map((x) => PlayOptionResponse.fromMap(x))),
      ws: List<PlayOptionResponse>.from(json["ws"].map((x) => PlayOptionResponse.fromMap(x))),
    );
  } catch (e) {
    rethrow;
  }
}

// toMap
Map<String, dynamic> toMap() => {
  "llhls": List<dynamic>.from(llhls!.map((x) => x.toMap())),
  "ws": List<dynamic>.from(ws!.map((x) => x.toMap())),
};
}