import 'package:chat_video_sdk/chat_video_sdk.dart';

/// push_options_response

class PushOptionsResponse {

  final String type;
  final String url;


  PushOptionsResponse({
    required this.type,
    required this.url,
  });

//coverFileName: json['coverFileName'].toString().toAppString()!,
// fromMap
factory PushOptionsResponse.fromMap(Map<String, dynamic> json) {
  try {
    return PushOptionsResponse(
      type: json["type"].toString().toAppString()!,
      url: json["url"].toString().toAppString()!,
    );
  } catch (e) {
    rethrow;
  }
}

// toMap
Map<String, dynamic> toMap() => {
  "type": type,
  "url": url,
};
}