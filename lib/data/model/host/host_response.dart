import 'package:chat_video_sdk/chat_video_sdk.dart';
/// host_response

class HostResponse extends SerializableApp{
  bool isChatEnabled;
  final String id;
  final String name;
  final String description;
  final String merchant;
  final String app;
  final String vhost;
  final String stream;
  final String token;
  final bool isStreaming;
  final List<String> giftPacks;
  final List<String> stickerPacks;
  final int receivedGiftPoint;
  final bool isEnabled;
  final bool deleted;
  final String createdAt;
  final String updatedAt;
  final String v;
  final String chatRoomId;
  final String participant;
  final List<PushOptionsResponse> pushOptions;
  final PlayOptionsTypeResponse? playOptions;
  final List<String> thumbnailUrls;

  HostResponse({
    required this.isChatEnabled,
    required this.id,
    required this.name,
    required this.description,
    required this.merchant,
    required this.app,
    required this.vhost,
    required this.stream,
    required this.token,
    required this.isStreaming,
    required this.giftPacks,
    required this.stickerPacks,
    required this.receivedGiftPoint,
    required this.isEnabled,
    required this.deleted,
    required this.createdAt,
    required this.updatedAt,
    required this.v,
    required this.chatRoomId,
    required this.participant,
    required this.pushOptions,
    required this.playOptions,
    required this.thumbnailUrls,
  });

//coverFileName: json['coverFileName'].toString().toAppString()!,
// fromMap
factory HostResponse.fromMap(Map<String, dynamic> json) {
  try {
    return HostResponse(
      isChatEnabled: json["isChatEnabled"] == true,
      id: json["_id"].toString().toAppString()!,
      name: json["name"].toString().toAppString()!,
      description: json["description"].toString().toAppString()!,
      merchant: json["merchant"].toString().toAppString()!,
      app: json["app"].toString().toAppString()!,
      vhost: json["vhost"].toString().toAppString()!,
      stream: json["stream"].toString().toAppString()!,
      token: json["token"].toString().toAppString()!,
      isStreaming: json["isStreaming"] == true,
      giftPacks: List<String>.from(json["giftPacks"].map((x) => x.toString().toAppString()!)),
      stickerPacks: List<String>.from(json["stickerPacks"].map((x) => x.toString().toAppString()!)),
      receivedGiftPoint: json["receivedGiftPoint"].toString().toAppInt(),
      isEnabled: json["isEnabled"] == true,
      deleted: json["deleted"] == true,
      createdAt: json["createdAt"].toString().toAppString()!,
      updatedAt: json["updatedAt"].toString().toAppString()!,
      v: json["__v"].toString().toAppString()!,
      chatRoomId: json["chatRoomId"].toString().toAppString()!,
      participant: json["participant"].toString().toAppString()!,
      pushOptions: List<PushOptionsResponse>.from(json["pushOptions"].map((x) => PushOptionsResponse.fromMap(x))),
      playOptions: PlayOptionsTypeResponse.fromMap(json["playOptions"]),
      thumbnailUrls: List<String>.from(json["thumbnailUrls"].map((x) => x.toString().toAppString()!)),
    );
  } catch (e) {
    rethrow;
  }
}

// toMap
@override
  Map<String, dynamic> toJson() => {
  "isChatEnabled": isChatEnabled,
  "_id": id,
  "name": name,
  "description": description,
  "merchant": merchant,
  "app": app,
  "vhost": vhost,
  "stream": stream,
  "token": token,
  "isStreaming": isStreaming,
  "giftPacks": List<dynamic>.from(giftPacks.map((x) => x)),
  "stickerPacks": List<dynamic>.from(stickerPacks.map((x) => x)),
  "receivedGiftPoint": receivedGiftPoint,
  "isEnabled": isEnabled,
  "deleted": deleted,
  "createdAt": createdAt,
  "updatedAt": updatedAt,
  "__v": v,
  "chatRoomId": chatRoomId,
  "participant": participant,
  "pushOptions": List<dynamic>.from(pushOptions.map((x) => x.toMap())),
  "playOptions": playOptions!.toMap(),
  "thumbnailUrls": List<dynamic>.from(thumbnailUrls.map((x) => x)),
};
}