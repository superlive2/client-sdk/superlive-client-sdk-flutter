class GiftAlertResponseModel {
  String streamId;
  Sender sender;
  Receiver receiver;
  Gift gift;

  GiftAlertResponseModel({
    required this.streamId,
    required this.sender,
    required this.receiver,
    required this.gift,
  });

  factory GiftAlertResponseModel.fromJson(Map<String, dynamic> json) => GiftAlertResponseModel(
    streamId: json["streamId"],
    sender: Sender.fromJson(json["sender"]),
    receiver: Receiver.fromJson(json["receiver"]),
    gift: Gift.fromJson(json["gift"]),
  );

  Map<String, dynamic> toJson() => {
    "streamId": streamId,
    "sender": sender.toJson(),
    "receiver": receiver.toJson(),
    "gift": gift.toJson(),
  };
}

class Gift {
  String name;
  String image;
  int point;

  Gift({
    required this.name,
    required this.image,
    required this.point,
  });

  factory Gift.fromJson(Map<String, dynamic> json) => Gift(
    name: json["name"],
    image: json["image"],
    point: json["point"],
  );

  Map<String, dynamic> toJson() => {
    "name": name,
    "image": image,
    "point": point,
  };
}

class Receiver {
  String id;
  String username;

  Receiver({
    required this.id,
    required this.username,
  });

  factory Receiver.fromJson(Map<String, dynamic> json) => Receiver(
    id: json["_id"],
    username: json["username"],
  );

  Map<String, dynamic> toJson() => {
    "_id": id,
    "username": username,
  };
}

class Sender {
  String id;
  String name;
  String username;

  Sender({
    required this.id,
    required this.name,
    required this.username,
  });

  factory Sender.fromJson(Map<String, dynamic> json) => Sender(
    id: json["_id"],
    name: json["name"],
    username: json["username"],
  );

  Map<String, dynamic> toJson() => {
    "_id": id,
    "name": name,
    "username": username,
  };
}
