import 'dart:convert';
import 'package:chat_video_sdk/chat_video_sdk.dart';

class LeaderBoardModel extends SerializableApp {
  int? statusCode;
  List<DatumLeaderboard>? data;
  bool isExpandLeaderBoard;

  LeaderBoardModel({
    this.statusCode,
    this.data,
    this.isExpandLeaderBoard = false,
  });

  factory LeaderBoardModel.fromRawJson(String str) => LeaderBoardModel.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory LeaderBoardModel.fromJson(Map<String, dynamic> json) => LeaderBoardModel(
    statusCode: json["statusCode"],
    data: json["data"] == null ? [] : List<DatumLeaderboard>.from(json["data"]!.map((x) => DatumLeaderboard.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "statusCode": statusCode,
    "data": data == null ? [] : List<dynamic>.from(data!.map((x) => x.toJson())),
  };
}

class DatumLeaderboard {
  String? id;
  String? name;
  int? totalGiftPoint;

  DatumLeaderboard({
    this.id,
    this.name,
    this.totalGiftPoint,
  });

  factory DatumLeaderboard.fromRawJson(String str) => DatumLeaderboard.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory DatumLeaderboard.fromJson(Map<String, dynamic> json) => DatumLeaderboard(
    id: json["_id"],
    name: json["name"],
    totalGiftPoint: json["totalGiftPoint"],
  );

  Map<String, dynamic> toJson() => {
    "_id": id,
    "name": name,
    "totalGiftPoint": totalGiftPoint,
  };
}
