class GiftResponseModel {
  int statusCode;
  List<Datum> data;
  String selectedItem;

  GiftResponseModel({
    required this.statusCode,
    required this.data,
    required this.selectedItem,
  });

  factory GiftResponseModel.fromJson(Map<String, dynamic> json) => GiftResponseModel(
    statusCode: json["statusCode"],
    data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    selectedItem: "",
  );

  Map<String, dynamic> toJson() => {
    "statusCode": statusCode,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
    "selectedItem": selectedItem,
  };
}

class Datum {
  String id;
  String name;
  String image;
  int point;
  bool deleted;
  DateTime createdAt;
  DateTime updatedAt;
  int v;

  Datum({
    required this.id,
    required this.name,
    required this.image,
    required this.point,
    required this.deleted,
    required this.createdAt,
    required this.updatedAt,
    required this.v,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    id: json["_id"],
    name: json["name"],
    image: json["image"],
    point: json["point"],
    deleted: json["deleted"],
    createdAt: DateTime.parse(json["createdAt"]),
    updatedAt: DateTime.parse(json["updatedAt"]),
    v: json["__v"],
  );

  Map<String, dynamic> toJson() => {
    "_id": id,
    "name": name,
    "image": image,
    "point": point,
    "deleted": deleted,
    "createdAt": createdAt.toIso8601String(),
    "updatedAt": updatedAt.toIso8601String(),
    "__v": v,
  };
}
