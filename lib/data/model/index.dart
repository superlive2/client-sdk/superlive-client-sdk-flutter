import 'package:chat_video_sdk/chat_video_sdk.dart';

export 'host/play_options_type_response.dart';
export 'host/play_option_response.dart';
export 'host/push_options_response.dart';
export 'host/host_response.dart';
export 'chat/request_join_chat_response.dart';
export 'chat/gift_response.dart';
export 'chat/pack_response.dart';
export 'chat/send_gift_response.dart';
export 'winner/winner_request.dart';
export 'winner/winner_response.dart';
export 'winner/lucky_draws_response.dart';

class DefaultRespond extends SerializableApp{
  @override
  Map<String, dynamic> toJson() {
    // TODO: implement toJson
    throw UnimplementedError();
  }
}