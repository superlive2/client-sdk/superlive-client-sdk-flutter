import 'package:chat_video_sdk/chat_video_sdk.dart';
import 'package:chat_video_sdk/data/translate/translate/en_translate.dart';
import 'package:chat_video_sdk/data/translate/translate/lookup_translate.dart';
import 'package:chat_video_sdk/data/translate/translate/zh_translate.dart';

Map<String, LookupTranslateChatVideoSdk> _lookupMessagesMap = {
  'en': EnTranslate(),
  'zh': ZhTranslate(),
};

/// Sets the default [locale]. By default it is `en`.
///
/// Example
/// ```
/// setLocaleMessages('fr', FrMessages());
/// setDefaultLocale('fr');
/// ```
void setDefaultLocale(String locale) {
  assert(_lookupMessagesMap.containsKey(locale), '[locale] must be a registered locale');
  sdkConfig.language = locale;
}

/// Sets a [locale] with the provided [lookupMessages] to be available when
/// using the [format] function.
///
/// Example:
/// ```dart
/// setLocaleMessages('fr', FrMessages());
/// ```
///
/// If you want to define locale message implement [LookupMessages] interface
/// with the desired messages
///
void setLocaleMessages(String locale, LookupTranslateChatVideoSdk lookupMessages) {
  _lookupMessagesMap[locale] = lookupMessages;
}

LookupTranslateChatVideoSdk get trans {
  if (_lookupMessagesMap[sdkConfig.language] == null) {
    print("Locale [${sdkConfig.language}] has not been added, using [${sdkConfig.language}] as fallback. To add a locale use [setLocaleMessages]");
  }
  return _lookupMessagesMap[sdkConfig.language] ?? EnTranslate();
}
