/// [LookupTranslateChatVideoSdk] template for any language
abstract class LookupTranslateChatVideoSdk {
  String get date;
  String get somethingWentWrong;
  String get leaderBoard;
  String get noInternetConnection;
  String get somethingUnexpectedWentWrong;
  String get playing;
  String get sendGift;
  String get pts;
  String get send;
  String get sendFail;
  String get broadcastSoon;
  String get liveStream;
  String get topGift;
  String get winner;
  String get luckyDraw;
  String get vote;
  String get votes;
  String get voteNow;
  String get waitingForResult;
  String get noLuckyDraw;
  String get retractVote;
  String get topGifters;
  String get chatDisabled;
  String get dontHavePoll;
  String get noRecord;
  String get errorTitle;
  String get errorMessage;
  String get retryTextButton;
  String get hours;
  String get minutes;
  String get seconds;
  String get luckyDrawWillStartIn;
  String get sendGiftBelow_;
  String get chatWithTheKeyWords_;

  String get signInToContinue;
  String get getAccessToMemberOnly;
  String get signIn;
}
