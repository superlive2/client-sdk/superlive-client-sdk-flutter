import 'lookup_translate.dart';

/// English language
class EnTranslate implements LookupTranslateChatVideoSdk {
  @override
  String get date => 'Date';

  @override
  String get somethingWentWrong => 'Something went wrong';

  @override
  String get leaderBoard => "Leaderboard";

  @override
  String get noInternetConnection => "No internet connection";

  @override
  String get playing => "Playing...";

  @override
  String get pts => "Pts";

  @override
  String get sendGift => "Send Gift";

  @override
  String get somethingUnexpectedWentWrong => "Something unexpected went wrong.";

  @override
  String get send => "Send";

  @override
  String get sendFail => "Sent fail";

  @override
  String get broadcastSoon => "Broadcast soon!";

  @override
  String get liveStream => "Live Stream";

  @override
  String get topGift => "Top Gift";

  @override
  String get vote => "Vote";

  @override
  String get winner => "Winner";

  @override
  String get luckyDraw => "Lucky Draw";

  @override
  String get voteNow => "Vote now";

  @override
  String get votes => "Votes";

  @override
  String get waitingForResult => "Waiting for result";

  @override
  String get noLuckyDraw => "No lucky draw";

  @override
  String get retractVote => "Retract Vote";

  @override
  String get topGifters => "Top Gifters";

  @override
  String get chatDisabled => "Chat's disabled";

  @override
  String get dontHavePoll => "Don't have vote";

  @override
  String get noRecord => "No record";

  @override
  String get errorTitle => "Network Unavailable";

  @override
  String get errorMessage => "Please check your connection.";

  @override
  String get retryTextButton => "Reload";

  @override
  String get hours => "HOURS";

  @override
  String get minutes => "MINUTES";

  @override
  String get seconds => "SECONDS";

  @override
  String get luckyDrawWillStartIn => "Lucky draw will start in";

  @override
  String get sendGiftBelow_ => "Send gift below to participate in the lucky draw!!";

  @override
  String get chatWithTheKeyWords_ => "Chat with the keywords below to participate in the lucky draw!!";

  @override
  String get getAccessToMemberOnly => "Get access to member only";

  @override
  String get signIn => "Sign In";

  @override
  String get signInToContinue => "Sign in to continue";
}
