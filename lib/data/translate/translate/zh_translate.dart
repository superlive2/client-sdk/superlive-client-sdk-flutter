import 'lookup_translate.dart';

/// Chinese language
class ZhTranslate implements LookupTranslateChatVideoSdk {
  @override
  String get date => '日期';

  @override
  String get somethingWentWrong => '出了些问题';

  @override
  String get leaderBoard => "排行榜";

  @override
  String get noInternetConnection => "无网络";

  @override
  String get playing => "播放...";

  @override
  String get pts => "分";

  @override
  String get sendGift => "送礼物";

  @override
  String get somethingUnexpectedWentWrong => "出了些问题";

  @override
  String get send => "发送";

  @override
  String get sendFail => "发送失败";

  @override
  String get broadcastSoon => "直播未开始";

  @override
  String get liveStream => "六合直播";

  @override
  String get topGift => "高级礼物";

  @override
  String get vote => "投票";

  @override
  String get winner => "获奖者";

  @override
  String get luckyDraw => "抽奖";

  @override
  String get voteNow => "立即投票";

  @override
  String get votes => "投票";

  @override
  String get waitingForResult => "等待结果";

  @override
  String get noLuckyDraw => "无抽奖活动";

  @override
  String get retractVote => "撤回投票";

  @override
  String get topGifters => "榜一";

  @override
  String get chatDisabled => "聊天功能已禁用";

  @override
  String get dontHavePoll => "无投票权";

  @override
  String get noRecord => "没有记录";

  @override
  String get errorTitle => "没有网络";

  @override
  String get errorMessage => "请检查您的网络连接";

  @override
  String get retryTextButton => "重新加载";

  @override
  String get hours => "小时";

  @override
  String get minutes => "分钟";

  @override
  String get seconds => "秒";

  @override
  String get luckyDrawWillStartIn => "抽奖倒计时";

  @override
  String get sendGiftBelow_ => "用以下的关键字聊天即可参与抽奖！！";

  @override
  String get chatWithTheKeyWords_ => "送以下的礼物即可参与抽奖！！";

  @override
  String get getAccessToMemberOnly => "仅限会员访问";

  @override
  String get signIn => "登录";

  @override
  String get signInToContinue => "登录以继续";
}
