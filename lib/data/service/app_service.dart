import 'package:chat_video_sdk/chat_video_sdk.dart';
import 'package:chat_video_sdk/data/model/chat/get_chat_host_model.dart';
import 'package:chat_video_sdk/data/model/chat/vote_response_model.dart';
import 'package:chat_video_sdk/data/model/host/view_count_model.dart';
import 'package:dartz/dartz.dart';
import '../model/home/leaderboard_model.dart';

class AppServices {
  BaseResponseEither<HostResponse> getHost({required String id}) async {
    try {
      final value = await Http(
        url: 'hosts/$id',
      ).get();
      var response = AppBaseResponse<HostResponse>.fromJson(value, (data) => HostResponse.fromMap(data));
      return right(response.data!);
    } catch (e) {
      return left(e.toString());
    }
  }
  BaseResponseEither<List<PackResponse>> getGiftsPacks({
    required String hostId,
  }) async {
    try {
      final value = await Http(
        url: 'gifts/packs?hostId=$hostId',
      ).get();
      var response = AppBaseResponse<PackResponse>.fromJson(value, (data) => PackResponse.fromMap(data));
      return right(response.dataList!);
    } catch (e) {
      return left(e.toString());
    }
  }

  BaseResponseEither<DefaultRespond> sendGift({
    required String hostId,
    required String participantId,
    required String giftId,
    required String giftAmount,
  }) async {
    try {
      final value = await Http(
        url: 'participants/send-gift',
        fields: {
          'hostId' : hostId,
          'participantId' : participantId,
          'giftId' : giftId,
          'ignoreValidate': false,
          'numOfGift': int.parse(giftAmount),
        },
      ).post();
      return right(DefaultRespond());
    } catch (e) {
      return left(e.toString());
    }
  }

  BaseResponseEither<LeaderBoardModel> getTopGifts({
    required String hostId,
  }) async {
    try {
      final value = await Http(
        url: 'hosts/$hostId/top-gifters',
      ).get();
      LeaderBoardModel response = LeaderBoardModel.fromJson(value);
      return right(response);
    } catch (e) {
      return left(e.toString());
    }
  }
  BaseResponseEither<RequestJoinChatResponse> requestJoinHost({
    required String hostId,
    required String participantId,
  }) async {
    try {
      var val = await Http(
        url: 'participants/join-host',
        fields: {
          "hostId": hostId,
          "participantId": participantId,
        },
      ).post();
      var response = AppBaseResponse<RequestJoinChatResponse>.fromJson(val, (data) => RequestJoinChatResponse.fromMap(data));
      return right(response.data!);
    } catch (e) {
      return left(e.toString());
    }
  }
  BaseResponseEither<DefaultRespond> requestLeftHost({
    required String hostId,
    required String participantId,
  }) async {
    try {
      var val = await Http(
        url: 'participants/leave-host',
        fields: {
          "hostId": hostId,
          "participantId": participantId,
        },
      ).post();
      return right(DefaultRespond());
    } catch (e) {
      return left(e.toString());
    }
  }
  BaseResponseEither<ViewCountModel> getViewCount({
    required String hostId,
  }) async {
    try {
      final value = await Http(
        url: 'hosts/$hostId/view-count',
      ).get();
      ViewCountModel response = ViewCountModel.fromJson(value);
      return right(response);
    } catch (e) {
      return left(e.toString());
    }
  }
  BaseResponseEither<List<GetChatHostModel>> getChatHost() async {
    try {
      final value = await Http(
        url: 'settings',
      ).get();
      var response = AppBaseResponse<GetChatHostModel>.fromJson(value, (data) => GetChatHostModel.fromJson(data));
      return right(response.dataList!);
    } catch (e) {
      return left(e.toString());
    }
  }
  BaseResponseEither<VoteResponseModel> getVote({
    required String? hostId,
    required String? participantId,
  }) async {
    try {
      final value = await Http(
        url: 'votes/current',
        fields: {
          "hostId": hostId,
          "participantId": participantId,
        },
      ).get();
      VoteResponseModel? response;
      if(value['data'] != {} && value['data'] != null) {
        response = VoteResponseModel.fromJson(value['data']);
      }
      return right(response!);
    } catch (e) {
      return left(e.toString());
    }
  }
  BaseResponseEither<DefaultRespond> submitVote({
    required String hostId,
    required String participantId,
    required String pollId,
    required String optionId,
  }) async {
    try {
      final value = await Http(
        url: 'participants/vote',
        fields: {
          'hostId' : hostId,
          'participantId' : participantId,
          "voteId": pollId,
          "optionId": optionId,
        },
      ).post();
      return right(DefaultRespond());
    } catch (e) {
      return left(e.toString());
    }
  }

  BaseResponseEither<DefaultRespond> retractVote({
    required String hostId,
    required String participantId,
    required String pollId,
  }) async {
    try {
      final value = await Http(
        url: 'participants/unvote',
        fields: {
          'hostId' : hostId,
          'participantId' : participantId,
          "voteId": pollId,
        },
      ).post();
      return right(DefaultRespond());
    } catch (e) {
      return left(e.toString());
    }
  }

  BaseResponseEither<LuckyDrawsResponse> getWinner(String hostId) async {
    try {
      final value = await Http(
        url: 'lucky-draws/current',
        fields: {
          'hostId': hostId,
        },
      ).get();
      final response = AppBaseResponse<LuckyDrawsResponse>.fromJson(value, (data) => LuckyDrawsResponse.fromMap(data));
      return right(response.data!);
    } catch (e) {
      return left(e.toString());
    }
  }
}
